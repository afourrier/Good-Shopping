package database;

import android.app.Activity;

import com.fourrier.goodshopping.R;

/**
 * Created by Alexis on 05/05/16.
 */
public enum TypeListe {
    LISTE_NORMALE, LISTE_COMPOSE, LISTE_RECETTE, PARAMETRES, REFRESH_FIREBASE, NOTE;

    public static TypeListe stringToEnum(String str){
        switch (str){
            case "LISTE_COMPOSE":
                return LISTE_COMPOSE;
            case "LISTE_RECETTE":
                return LISTE_RECETTE;
            case "PARAMETRES":
                return PARAMETRES;
            case "REFRESH_FIREBASE":
                return REFRESH_FIREBASE;
            case "NOTE":
                return NOTE;
            default:
                return LISTE_NORMALE;
        }
    }

    public static String toString(Activity activity, TypeListe typeListe){
        switch (typeListe){
            case LISTE_NORMALE:
                return activity.getString(R.string.normal);
            case LISTE_RECETTE:
                return activity.getString(R.string.recette);
            case LISTE_COMPOSE:
                return "Compose";
            default:
                return "";
        }
    }

    public static int getNum(TypeListe typeListe){
        switch (typeListe){
            case LISTE_NORMALE:
                return 0;
            case LISTE_RECETTE:
                return 1;
            case LISTE_COMPOSE:
                return 2;
            default:
                return 3;
        }
    }

    public static TypeListe listToNum(int num){
        switch (num){
            case 0:
                return LISTE_NORMALE;
            case 1:
                return LISTE_RECETTE;
            case 2:
                return LISTE_COMPOSE;
            default:
                return PARAMETRES;
        }
    }

}
