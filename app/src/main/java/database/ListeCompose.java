package database;

import java.util.ArrayList;

import adapters.Item;
import adapters.TypeItemMenu;

/**
 * Created by Alexis on 04/02/2017.
 */

public class ListeCompose implements Item{
    private int id;
    private String nom;
    private ArrayList<GSListe> listes;
    private boolean selected;
    private boolean visible;

    public ListeCompose(int id, String nom, ArrayList<GSListe> listes) {
        this.id = id;
        this.nom = nom;
        this.listes = listes;
        selected = false;
        visible = false;
    }

    public ListeCompose(String nom, ArrayList<GSListe> listes) {
        this.nom = nom;
        this.listes = listes;
        selected = false;
        visible = false;
    }

    public int getId() {
        return id;
    }

    @Override
    public String getFirebaseId() {
        return "";
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public ArrayList<GSListe> getListes() {
        return listes;
    }

    public void setListes(ArrayList<GSListe> listes) {
        this.listes = listes;
    }

    @Override
    public boolean isSelected() {
        return selected;
    }

    @Override
    public TypeItemMenu getTypeItemMenu() {
        return TypeItemMenu.ITEM_LISTE;
    }

    @Override
    public TypeListe getTypeListe() {
        return TypeListe.LISTE_COMPOSE;
    }

    @Override
    public String getTitle() {
        return nom;
    }

    @Override
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    public GSListe toList(){
        GSListe liste = listes.get(0);
        for (int i = 1; i < listes.size(); i++) {
            for (int j = 0; j < listes.get(i).getListCategories().size(); j++) {
                liste.addCategorie(listes.get(i).getCategorie(j));
            }
        }
        return liste;
    }
}
