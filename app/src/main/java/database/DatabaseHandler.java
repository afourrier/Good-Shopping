package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Alexis on 24/11/15.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 5;

    // Database Name
    private static final String DATABASE_NAME = "GoodShopping";

    // Tables name
    private static final String TABLE_LISTE        = "liste";
    private static final String TABLE_CATEGORIE    = "categorie";
    private static final String TABLE_ITEM         = "item";
    private static final String TABLE_DICO         = "dico";
    private static final String TABLE_LIST_COMPOSE = "list_compose";
    private static final String TABLE_CARTE        = "carte_fidelite";

    //Liste des colones
    //Commun
    private static final String KEY_ID            = "id";
    private static final String KEY_NOM           = "nom";
    private static final String KEY_POSITION      = "position";
    private static final String KEY_TYPE          = "type";
    private static final String KEY_ID_CATEGORIE  = "id_categorie";
    //Table Liste
    private static final String KEY_NBP           = "npb";  //Nombre de personnes
    private static final String KEY_NBPD          = "nbpd"; //Nombre de personnes demmande
    //Table GSCategory
    private static final String KEY_COLOR         = "couleur";
    private static final String KEY_ID_LISTE      = "id_liste";
    //Table Item
    private static final String KEY_SELECT        = "selected";
    private static final String KEY_LONGUEUR      = "longueur";
    private static final String KEY_LARGEUR       = "largeur";
    private static final String KEY_HAUTEUR       = "hauteur";
    private static final String KEY_QUANTITE      = "quantite";
    private static final String KEY_NBFA          = "nbfa";//Nombre de fois achete
    private static final String KEY_TYPE_LONGUEUR = "type_longueur";
    private static final String KEY_TYPE_LARGEUR  = "type_largeur";
    private static final String KEY_TYPE_HAUTEUR  = "type_hauteur";
    private static final String KEY_IMAGE         = "image";
    //Table Dico
    private static final String KEY_KEY           = "key";
    private static final String KEY_VALUE         = "value";
    //Table carte de fidelite
    private static final String KEY_NUM           = "numero";
    private static final String KEY_MAGASIN       = "magasin";
    private static final String KEY_PRENOM        = "prenom";

    private static final String KEY_FIREBASE_ID   = "firebase_id";

    private int FLAG = 0;

    private Context m_context;

    private SQLiteDatabase db;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        m_context = context;
    }


    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LISTE_TABLE = "CREATE TABLE " + TABLE_LISTE + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_NOM + " TEXT,"
                + KEY_TYPE + " TEXT,"
                + KEY_NBP + " TEXT,"
                + KEY_NBPD + " TEXT,"
                + KEY_FIREBASE_ID + " TEXT,"
                + KEY_POSITION + " TEXT)";

        String CREATE_CATEGORIE_TABLE = "CREATE TABLE " + TABLE_CATEGORIE + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_NOM + " TEXT,"
                + KEY_COLOR + " TEXT,"
                + KEY_ID_LISTE + " INTEGER,"
                + KEY_POSITION + " INTEGER,"
                + KEY_FIREBASE_ID + " TEXT)";

        String CREATE_ITEM_TABLE = "CREATE TABLE " + TABLE_ITEM + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_NOM + " TEXT,"
                + KEY_ID_CATEGORIE + " INTEGER,"
                + KEY_SELECT + " TEXT,"
                + KEY_LONGUEUR + " TEXT,"
                + KEY_LARGEUR + " TEXT,"
                + KEY_HAUTEUR + " TEXT,"
                + KEY_QUANTITE + " TEXT,"
                + KEY_NBFA + " TEXT,"
                + KEY_IMAGE + " TEXT,"
                + KEY_POSITION + " INTEGER,"
                + KEY_TYPE + " TEXT,"
                + KEY_TYPE_LONGUEUR + " TEXT,"
                + KEY_TYPE_LARGEUR + " TEXT,"
                + KEY_TYPE_HAUTEUR + " TEXT,"
                + KEY_FIREBASE_ID + " TEXT)";


        String CREATE_DICO = "CREATE TABLE " + TABLE_DICO + "("
                + KEY_KEY + " TEXT PRIMARY KEY,"
                + KEY_VALUE + " TEXT)";

        String CREATE_LISTE_COMPOSE_TABLE = "CREATE TABLE " + TABLE_LIST_COMPOSE + "("
                + KEY_ID + " INTEGER,"
                + KEY_ID_LISTE + " INTEGER,"
                + KEY_ID_CATEGORIE + " INTEGER,"
                + KEY_SELECT + " TEXT,"
                + KEY_POSITION + " TEXT,"
                + "PRIMARY KEY ("+KEY_ID+","+ KEY_ID_LISTE+","+ KEY_ID_CATEGORIE+"))";

        String CREATE_CARTE_FID = "CREATE TABLE " + TABLE_CARTE + "("
                + KEY_ID + " INTEGER,"
                + KEY_NUM + " TEXT,"
                + KEY_MAGASIN + " TEXT,"
                + KEY_NOM + " TEXT,"
                + KEY_PRENOM + " TEXT,"
                + KEY_FIREBASE_ID + " TEXT,"
                + KEY_POSITION + " TEXT)";

        db.execSQL(CREATE_LISTE_TABLE);
        db.execSQL(CREATE_CATEGORIE_TABLE);
        db.execSQL(CREATE_ITEM_TABLE);
        db.execSQL(CREATE_DICO);
        db.execSQL(CREATE_LISTE_COMPOSE_TABLE);
        db.execSQL(CREATE_CARTE_FID);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*if (oldVersion == 6 && newVersion == 7) {

            String upgradeQuery = "ALTER TABLE " + TABLE_CHORE + " ADD COLUMN " + KEY_ID_LIST + " TEXT";

            String CREATE_LIST_OF_LIST = "CREATE TABLE " + TABLE_LIST_LIST + "("
                    + KEY_ID + " TEXT PRIMARY KEY,"
                    + KEY_LIBELLE + " TEXT,"
                    + KEY_POSITION + " TEXT,"
                    + KEY_COLOR + " TEXT)";

            String CREATE_LIST_OF_ITEM = "CREATE TABLE " + TABLE_LIST_ITEM + "("
                    + KEY_ID + " TEXT PRIMARY KEY,"
                    + KEY_LIBELLE + " TEXT,"
                    + KEY_SELECT + " TEXT,"
                    + KEY_ID_LIST + " TEXT,"
                    + KEY_POSITION + " TEXT)";

            db.execSQL(upgradeQuery);
            db.execSQL(CREATE_LIST_OF_LIST);
            db.execSQL(CREATE_LIST_OF_ITEM);
        }*/

        // Drop older table if existed
        /*db.execSQL("DROP TABLE IF EXISTS " + TABLE_LISTE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEM);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIST_COMPOSE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DICO);

        // Create tables again
        onCreate(db);*/

        if (oldVersion == 4 && newVersion >= 5) {
            this.db = db;
            db.execSQL("ALTER TABLE " + TABLE_LISTE + " ADD COLUMN " + KEY_FIREBASE_ID + " TEXT");
            db.execSQL("ALTER TABLE " + TABLE_LISTE + " ADD COLUMN " + KEY_POSITION + " TEXT");

            db.execSQL("ALTER TABLE " + TABLE_CATEGORIE + " ADD COLUMN " + KEY_FIREBASE_ID + " TEXT");

            db.execSQL("ALTER TABLE " + TABLE_ITEM  + " ADD COLUMN " + KEY_FIREBASE_ID + " TEXT");

            db.execSQL("ALTER TABLE " + TABLE_CARTE + " ADD COLUMN " + KEY_FIREBASE_ID + " TEXT");
            db.execSQL("ALTER TABLE " + TABLE_CARTE + " ADD COLUMN " + KEY_POSITION + " TEXT");

            addPositionToLists();
        }

    }

    // Adding new chore

    //--------------------------
    // --- TABLE LISTE ---
    //--------------------------
    // -Add
    public int addListe(GSListe liste) {
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        ContentValues values = remplireContentValues(liste, true);
        // Inserting Row
        db.insert(TABLE_LISTE, null, values);

        Cursor cursor = db.rawQuery("SELECT "+KEY_ID+" FROM " + TABLE_LISTE,null);
        if (cursor != null)
            cursor.moveToLast();
        int id = cursor.getInt(0);
        closeDB(); // Closing database connection
        return id;
    }

    public int addListeCategoriesItems(GSListe liste){
        int idListe = addListe(liste);
        GSCategory category;
        for(int i = 0; i< liste.getListCategories().size(); i++){
            category = liste.getCategorie(i); //On recupere la category
            category.setIdListe(idListe);      //On lui donne l'id de la liste
            addCategorieItems(category);
        }
        return idListe;
    }

    public void addListesCategoriesItems(LinkedList<GSListe> listes){
        for(int i=0; i<listes.size();i++) {
            int idListe = addListe(listes.get(i));
            GSCategory category;
            for (int j = 0; j < listes.get(i).getListCategories().size(); j++) {
                category = listes.get(i).getCategorie(j); //On recupere la category
                category.setIdListe(idListe);      //On lui donne l'id de la liste
                addCategorieItems(category);
            }
        }
    }

    // -Update
    public int updateListe(GSListe liste) {
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        ContentValues values = remplireContentValues(liste, false);
        // updating row
        int res = db.update(TABLE_LISTE, values, KEY_ID + " = ?",
                new String[] { String.valueOf(liste.getId()) });
        closeDB(); // Closing database connection

        return res;
    }

    // -Delete
    public void deleteListe(GSListe liste) {
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        //On supprime toutes les categories de la liste
        for(int i = 0; i<liste.getListCategories().size(); i++){
            deleteCategorie(liste.getCategorie(i));
        }
        db.delete(TABLE_LISTE, KEY_ID + " = ?",
                new String[]{String.valueOf(liste.getId())});
        closeDB(); // Closing database connection
    }

    public boolean isListPresent(String firebaseId){
        //        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_LISTE + " WHERE " + KEY_FIREBASE_ID + " = '" + firebaseId + "'";

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            closeDB(); // Closing database connection
            return true;
        }
        closeDB(); // Closing database connection
        return false;
    }

    // -Get

    /**
     *
     * @param id de la liste a charger
     * @return une liste avec l'ensemble des categories et des items qui la compose
     */
    public GSListe getListe(int id){
//        FLAG++;
//        SQLiteDatabase db = this.getReadableDatabase();
        openDB();

        Cursor cursor = db.query(TABLE_LISTE, new String[]{KEY_ID,
                        KEY_NOM, KEY_TYPE, KEY_NBP, KEY_NBPD, KEY_FIREBASE_ID, KEY_POSITION}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        GSListe liste = new GSListe(
                cursor.getInt(0),//id
                cursor.getString(1),//Nom
                TypeListe.stringToEnum(cursor.getString(2)),//Type
                cursor.getInt(3),//Nombre de personnes
                cursor.getInt(4),//Nombre de personnes demande
                cursor.getString(5),//Firebase id
                cursor.getInt(6));//Position
        liste.setListeCategorie((LinkedList)getAllCategoriesFromListeWithItems(id));
        closeDB(); // Closing database connection
        return liste;

    }

    /**
     *
     * @param id firebase de la liste a charger
     * @return une liste avec l'ensemble des categories et des items qui la compose
     */
    public GSListe getListeWithFirebaseId(String id){
//        FLAG++;
//        SQLiteDatabase db = this.getReadableDatabase();
        openDB();

        Cursor cursor = db.query(TABLE_LISTE, new String[]{KEY_ID,
                        KEY_NOM, KEY_TYPE, KEY_NBP, KEY_NBPD, KEY_FIREBASE_ID, KEY_POSITION}, KEY_FIREBASE_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        else {
            return null;
        }
        GSListe liste = new GSListe(
                cursor.getInt(0),//id
                cursor.getString(1),//Nom
                TypeListe.stringToEnum(cursor.getString(2)),//Type
                cursor.getInt(3),//Nombre de personnes
                cursor.getInt(4),//Nombre de personnes demande
                cursor.getString(5),//Firebase id
                cursor.getInt(6));//Position
        liste.setListeCategorie((LinkedList)getAllCategoriesFromListeWithItems(cursor.getInt(0)));
        closeDB(); // Closing database connection
        return liste;

    }

    /**
     *
     * @return une liste avec l'ensemble des categories et des items qui la compose
     */
    public GSListe getPremiereListeNormale(){
//        FLAG++;
//        SQLiteDatabase db = this.getReadableDatabase();
        openDB();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_LISTE
                + " WHERE " + KEY_TYPE +" = '"+ TypeListe.LISTE_NORMALE.toString()+"'", null);
        if (cursor != null)
            cursor.moveToFirst();
        int id = cursor.getInt(0);

        closeDB(); // Closing database connection
        return getListe(id);

    }

    /**
     *
     * @return l'ensemble des listes sans les categories ni les items
     */
    public List<GSListe> getAllListe(){
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        List<GSListe> list = new LinkedList<GSListe>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_LISTE + " ORDER BY "+KEY_POSITION;
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                GSListe liste = new GSListe(
                        cursor.getInt(0),//id
                        cursor.getString(1),//Nom
                        TypeListe.stringToEnum(cursor.getString(2)),//Type
                        cursor.getInt(3),//Nombre de personnes
                        cursor.getInt(4),//Nombre de personnes demande
                        cursor.getString(5),//Firebase id
                        cursor.getInt(6));//Position
                list.add(liste);
            } while (cursor.moveToNext());
        }
        closeDB(); // Closing database connection

        return list;

    }

    public int getListSize(){
        String countQuery = "SELECT  * FROM " + TABLE_LISTE;
//        SQLiteDatabase db = this.getReadableDatabase();
        openDB();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        closeDB();
        return cnt;
    }

    public String getFirstListFrebaseId(){
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        List<GSListe> list = new LinkedList<GSListe>();
        // Select All Query
        String selectQuery = "SELECT "+KEY_FIREBASE_ID+" FROM " + TABLE_LISTE + " WHERE "+KEY_POSITION+"=0";

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            return cursor.getString(0);
        }
        closeDB(); // Closing database connection

        return "";
    }

    //--------------------------
    // --- TABLE CATEGORIE ---
    //--------------------------
    // -Add
    public int addCategorie(GSCategory category) {
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        // Inserting Row
        ContentValues values = remplireContentValues(category, true);
        // Inserting Row
        db.insert(TABLE_CATEGORIE, null, values);
        Cursor cursor = db.rawQuery("SELECT " + KEY_ID + " FROM " + TABLE_CATEGORIE, null);
        if (cursor != null)
            cursor.moveToLast();
        int id = cursor.getInt(0);
        closeDB(); // Closing database connection
        return id;
    }

    public void addCategorieItems(GSCategory category){
        int idCategorie = addCategorie(category);
        GSItem item;
        LinkedList<GSItem> liste = category.getListItems();
        for(int i=0; i<liste.size();i++){
            item = liste.get(i); //On recupere l'item
            item.setIdCategorie(idCategorie);   //On lui donne l'id de la category
            addItem(item);

        }
    }

    // -Update
    public int updateCategorie(GSCategory category) {
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        ContentValues values = remplireContentValues(category, false);

        // updating row
        int res = db.update(TABLE_CATEGORIE, values, KEY_ID + " = ?",
                new String[] { String.valueOf(category.getId()) });
        closeDB(); // Closing database connection
        return res;
    }

    // -Delete
    public void deleteCategorie(GSCategory category) {
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        db.delete(TABLE_CATEGORIE, KEY_ID + " = ?",
                new String[]{String.valueOf(category.getId())});
        //On commence par supprimer tous les items
        for(int i = 0; i< category.getListItems().size(); i++){
            deleteItem(category.getListItems().get(i));
        }
        closeDB(); // Closing database connection
    }

    public boolean isCategoryPresent(String firebaseId){
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_CATEGORIE + " WHERE " + KEY_FIREBASE_ID + " = '" + firebaseId + "'";

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            closeDB(); // Closing database connection
            return true;
        }
        closeDB(); // Closing database connection
        return false;
    }

    // -Get
    /**
     *
     * @return l'ensemble des categories avec les items
     */
    public List<GSCategory> getAllCategoriesFromListeWithItems(int idListe){
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        List<GSCategory> list = new LinkedList<GSCategory>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_CATEGORIE + " WHERE "
                + KEY_ID_LISTE + "=" + idListe
                + " ORDER BY " + KEY_POSITION;

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                int id = Integer.parseInt(cursor.getString(0));
                GSCategory category = new GSCategory(
                        id,//id
                        cursor.getString(1),//Nom
                        cursor.getString(2),//Couleur
                        idListe,//id liste
                        Integer.parseInt(cursor.getString(4)),//position
                        cursor.getString(5));//Firebase id
                category.setListItems((LinkedList) getAllItemsFromCategorie(id,
                        cursor.getString(2), cursor.getString(5),cursor.getString(1)));
                list.add(category);
            } while (cursor.moveToNext());
        }
        closeDB(); // Closing database connection

        return list;
    }

    /**
     *
     * @return la categorie avec ses items
     */
    public List<GSCategory> getCategoriesWithItems(int idListe, int idcategorie){
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        List<GSCategory> list = new LinkedList<GSCategory>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CATEGORIE + " WHERE "
                + KEY_ID + "=" + idcategorie
                + " ORDER BY " + KEY_POSITION;

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                GSCategory category = new GSCategory(
                        id,//id
                        cursor.getString(1),//Nom
                        cursor.getString(2),//Couleur
                        idListe,//id liste
                        cursor.getInt(4),//position
                        cursor.getString(5));//Firebase id
                category.setListItems((LinkedList) getAllItemsFromCategorie(id,
                        cursor.getString(2), cursor.getString(5),cursor.getString(1)));
                list.add(category);
            } while (cursor.moveToNext());
        }
        closeDB(); // Closing database connection

        return list;
    }

    public GSCategory getCategorieWithFirebaseId(String firebaseId, int idListe){
        //        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        String selectQuery = "SELECT  * FROM " + TABLE_CATEGORIE + " WHERE "
                + KEY_FIREBASE_ID + "='" + firebaseId+"'";

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            GSCategory category =new GSCategory(
                    cursor.getInt(0),//id
                    cursor.getString(1),//Nom
                    cursor.getString(2),//Couleur
                    idListe,//id liste
                    cursor.getInt(4),//position
                    cursor.getString(5));//Firebase id;
            category.setListItems((LinkedList) getAllItemsFromCategorie(cursor.getInt(0),
                    cursor.getString(2), cursor.getString(5), cursor.getString(1)));
            closeDB(); // Closing database connection
            return category;
        }
        closeDB(); // Closing database connection
        return null;
    }

    /**
     *
     * @return l'ensemble des categories sans les items
     */
    public List<GSCategory> getAllCategoriesFromListeWithoutItems(int idListe){
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        List<GSCategory> list = new LinkedList<GSCategory>();
        // Select All Query
        String selectQuery = "SELECT " + KEY_ID + " FROM " + TABLE_CATEGORIE + " WHERE "
                + KEY_ID_LISTE + "=" + idListe
                + " ORDER BY " + KEY_POSITION;

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                GSCategory category = new GSCategory(
                        cursor.getInt(0),//id
                        cursor.getString(1),//Nom
                        cursor.getString(2),//Couleur
                        idListe,//id liste
                        cursor.getInt(4),//position
                        cursor.getString(5));//Firebase id
                list.add(category);
            } while (cursor.moveToNext());
        }
        closeDB(); // Closing database connection

        return list;
    }

    public int getIdCategoryForFirebaseId(String fId){
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        String selectQuery = "SELECT  * FROM " + TABLE_CATEGORIE + " WHERE "
                + KEY_FIREBASE_ID + "='" + fId+"'";

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            closeDB(); // Closing database connection
            return cursor.getInt(0);
        }
        closeDB(); // Closing database connection
        return 0;
    }

    //--------------------------
    // --- TABLE ITEM ---
    //--------------------------
    // -Add
    public int addItem(GSItem item) {
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        // Inserting Row

        ContentValues values = remplireContentValues(item, true);
        // Inserting Row
//        db.insert(TABLE_ITEM, null, values);
//        Cursor cursor = db.rawQuery("SELECT "+KEY_ID+" FROM " + TABLE_ITEM,null);
//        if (cursor != null)
//            cursor.moveToLast();
//        int id = cursor.getInt(0);
        int id = (int) db.insert(TABLE_ITEM, null, values);
        closeDB(); // Closing database connection

        return id;
    }

    // -Update
    public int updateItem(GSItem item) {
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        ContentValues values = remplireContentValues(item, false);

        // updating row
        int res = db.update(TABLE_ITEM, values, KEY_ID + " = ?",
                new String[] { String.valueOf(item.getId()) });
        closeDB(); // Closing database connection

        return res;
    }

    // -Delete
    public void deleteItem(GSItem item) {
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        db.delete(TABLE_ITEM, KEY_ID + " = ?",
                new String[]{String.valueOf(item.getId())});
        closeDB(); // Closing database connection
    }

    // -Get
    /**
     *
     * @return l'ensemble des categories sans les items
     */
    public List<GSItem> getAllItemsFromCategorie(int idCategorie, String color,
                                                 String idFirebaseCate, String categoryName){
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        List<GSItem> list = new LinkedList<GSItem>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_ITEM + " WHERE "
                + KEY_ID_CATEGORIE + "=" + idCategorie
                + " ORDER BY " + KEY_POSITION;

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                GSItem item = new GSItem(
                        cursor.getInt(0),//id
                        cursor.getString(1),//Nom
                        cursor.getInt(2),//id categorie
                        Boolean.parseBoolean(cursor.getString(3)),//selected
                        cursor.getFloat(4),//longueur
                        cursor.getFloat(5),//largeur
                        cursor.getFloat(6),//hauteur
                        cursor.getFloat(7),//quantite
                        cursor.getInt(8),//nb fois achete
                        cursor.getString(9),//image
                        cursor.getInt(10),//position
                        cursor.getInt(11),//type
                        cursor.getInt(12),//type longueur
                        cursor.getInt(13),//type largeur
                        cursor.getInt(14),//type hauteur
                        cursor.getString(15),
                        idFirebaseCate);//Firebase id
                        item.setColor(color);
                        item.setNomCategorie(categoryName);
                list.add(item);
            } while (cursor.moveToNext());
        }
        closeDB(); // Closing database connection

        return list;
    }

    //--------------------------
    // --- TABLE DICO ---
    //--------------------------
    // -Add
    public void addDico(String key, String value) {
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();

        ContentValues values = new ContentValues();
        values.put(KEY_KEY, key);
        values.put(KEY_VALUE, value);

        // Inserting Row
        db.insert(TABLE_DICO, null, values);
        closeDB(); // Closing database connection
    }

    // -Update
    public int updateDico(String key, String valeur) {
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        ContentValues values = new ContentValues();
        values.put(KEY_KEY, key);
        values.put(KEY_VALUE, valeur);

        // updating row
        int res = db.update(TABLE_DICO, values, KEY_KEY + " = ?",
        new String[] {key});
        closeDB(); // Closing database connection

        return res;
    }

    // -Delete
    public void deleteDico(String key) {
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        db.delete(TABLE_DICO, KEY_KEY + " = ?",
                new String[]{key});
        closeDB(); // Closing database connection
    }

    // -Get
    public String getDico(String key) {

        String selectQuery = "SELECT "+KEY_VALUE+" FROM " + TABLE_DICO+ " WHERE "
                + KEY_KEY + "='" + key+"'";

//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null)
            cursor.moveToFirst();

        closeDB(); // Closing database connection
        return cursor.getString(0);
    }

    //--------------------------
    // --- TABLE LISTE COMPOSE ---
    //--------------------------
    // -Add
    public int addListeCompose(ListeCompose liste) {
        ArrayList<GSListe> listes = liste.getListes();
        //creation dune liste normale
        int id = addListe(new GSListe(0, liste.getNom(),TypeListe.LISTE_COMPOSE,0,0,"",0));

//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        //Ajout des autres listes
        for (int i = 0; i < listes.size(); i++) {

            ContentValues values = remplireContentValues(listes.get(i).getId(), false, id);
            db.insert(TABLE_LIST_COMPOSE, null, values);
        }
        closeDB(); // Closing database connection
        return id;
    }

    // -Update
    public int updateListeCompose(GSListe liste) {
        return updateListe(liste);
    }

    public void updateListeCompose(ArrayList<GSListe> listeOfList, GSListe liste){
        ArrayList<Integer> idListExistante = getIdFromListeCompose(liste.getId());
        ArrayList<Integer> idListDifference = new ArrayList<>();
        ArrayList<Integer> idListDifference2 = new ArrayList<>();
        for (GSListe l: listeOfList) {
            idListDifference.add(l.getId());
        }
        idListDifference2 = (ArrayList<Integer>) idListDifference.clone();
        idListDifference.removeAll(idListExistante);
        idListExistante.removeAll(idListDifference2);
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        //Pour tous les anciennes liste présente
        for (int i:idListDifference) {
            if(idListExistante.contains(i)){
                db.delete(TABLE_LIST_COMPOSE, KEY_ID + " = ? and " + KEY_ID_LISTE + " = ? ",
                        new String[]{String.valueOf(liste.getId()), String.valueOf(i)});
            }
            else {//Normalement on garde cette partie
                ContentValues values = remplireContentValues(i, false, liste.getId());
                db.insert(TABLE_LIST_COMPOSE, null, values);
            }
        }

        for (int i:idListExistante) {
            if(idListDifference2.contains(i)){//Normalement on garde cette partie
                db.delete(TABLE_LIST_COMPOSE, KEY_ID + " = ? and " + KEY_ID_LISTE + " = ? ",
                        new String[]{String.valueOf(liste.getId()), String.valueOf(i)});
            }
            else {
                ContentValues values = remplireContentValues(i, false, liste.getId());
                db.insert(TABLE_LIST_COMPOSE, null, values);
            }
        }


        db.close();
    }

    // -Delete
    public void deleteListeCompose(int id) {
//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();
        db.delete(TABLE_LISTE, KEY_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.delete(TABLE_LIST_COMPOSE, KEY_ID + " = ?",
                new String[]{Integer.toString(id)});
        db.close();
    }


    // -Get
    /**
     *
     * @param id de la liste a charger
     * @return une liste avec l'ensemble des categories et des items qui la compose
     */
    public GSListe getListeCompose(int id){

        GSListe liste = getListe(id);

//        SQLiteDatabase db = this.getReadableDatabase();
        openDB();

        String requete = "SELECT * FROM "+TABLE_LIST_COMPOSE+" WHERE "+KEY_ID+"="+id;

        Cursor cursor = db.rawQuery(requete, null);

        if (cursor != null)
            cursor.moveToFirst();

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                GSListe listeToAdd = getListe(cursor.getInt(1));
                for (GSCategory cate:listeToAdd.getListCategories()) {
                    liste.addCategorie(cate);
                }
            } while (cursor.moveToNext());
        }
        closeDB(); // Closing database connection

        return liste;

    }

    public ArrayList<Integer> getIdFromListeCompose(int id){
        ArrayList<Integer> idList = new ArrayList<>();

//        SQLiteDatabase db = this.getReadableDatabase();
        openDB();


        String requete = "SELECT * FROM "+TABLE_LIST_COMPOSE+" WHERE "+KEY_ID+"="+id;

        Cursor cursor = db.rawQuery(requete, null);

        if (cursor != null)
            cursor.moveToFirst();

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                idList.add(cursor.getInt(1));
            } while (cursor.moveToNext());
        }
        db.close();

        return idList;
    }

    /*public int getIdLePlusHaut(){
        List<Chore> liste = getListChores();
        int hight = 0;
        Chore c;
        for(int i=0;i<liste.size();i++){
            c = liste.get(i);
            if(c.getId()>hight);
            hight=c.getId();
        }
        return  hight;
    }*/


    //--------------------------
    // --- REMPLIRE VALUES ---
    //--------------------------
    // -Liste
    private ContentValues remplireContentValues(GSListe liste, boolean idNull){

        ContentValues values = new ContentValues();
        if(!idNull)
            values.put(KEY_ID, liste.getId());
        values.put(KEY_NOM, liste.getNom());
        values.put(KEY_TYPE, liste.getTypeListe().toString());
        values.put(KEY_NBP, liste.getNbPersonnes());
        values.put(KEY_NBPD, liste.getNbPersonnesDemande());
        values.put(KEY_FIREBASE_ID, liste.getFirebaseId());
        values.put(KEY_POSITION, liste.getPosition());
        return values;
    }
    // -Liste composee
    private ContentValues remplireContentValues(int idList, boolean idNull, int id){

        ContentValues values = new ContentValues();
        if(!idNull) {
            values.put(KEY_ID, id);
        }
        values.put(KEY_ID_LISTE, idList);
        values.put(KEY_ID_CATEGORIE, idList); //Non gere pour le moment
        values.put(KEY_SELECT, "false"); //Non gere pour le moment
        values.put(KEY_POSITION, 0); //Non gere pour le moment

        return values;
    }
    // -GSCategory
    private ContentValues remplireContentValues(GSCategory category, boolean idNull){

        ContentValues values = new ContentValues();
        if(!idNull)
            values.put(KEY_ID, category.getId());
        values.put(KEY_NOM, category.getNom());
        values.put(KEY_COLOR, category.getCouleur());
        values.put(KEY_ID_LISTE, category.getIdListe());
        values.put(KEY_POSITION, category.getEmplacement());
        values.put(KEY_FIREBASE_ID, category.getFirebaseId());

        return values;
    }

    // -Item
    private ContentValues remplireContentValues(GSItem item, boolean idNull){

        ContentValues values = new ContentValues();
        if(!idNull)
            values.put(KEY_ID, item.getId());
        values.put(KEY_NOM, item.getNom());
        values.put(KEY_ID_CATEGORIE, item.getIdCategorie());
        values.put(KEY_SELECT, Boolean.toString(item.isSelect()));
        values.put(KEY_LONGUEUR, item.getLongueur());
        values.put(KEY_LARGEUR, item.getLargeur());
        values.put(KEY_HAUTEUR, item.getHauteur());
        values.put(KEY_QUANTITE, item.getQuantite());
        values.put(KEY_NBFA, item.getNbFoisAchete());
        if(!idNull)
            values.put(KEY_IMAGE, item.getImage());
        else
            values.put(KEY_IMAGE, "");
        values.put(KEY_POSITION, item.getEmplacement());
        values.put(KEY_TYPE, item.getType());
        values.put(KEY_TYPE_LONGUEUR, item.getLongType());
        values.put(KEY_TYPE_LARGEUR, item.getLargType());
        values.put(KEY_TYPE_HAUTEUR, item.getHautType());
        values.put(KEY_FIREBASE_ID, item.getFirebaseId());

        return values;
    }

    // -Liste compose
    private ContentValues remplireContentValues(GSListe liste, GSCategory category,
                                                boolean selected, boolean idNull){

        ContentValues values = new ContentValues();
        if(!idNull)
            values.put(KEY_ID, liste.getId());
        values.put(KEY_ID_LISTE, liste.getNom());
        values.put(KEY_ID_CATEGORIE, category.getId());
        values.put(KEY_SELECT, liste.getNbPersonnes());

        return values;
    }

    public boolean estPresentDansLeDico(String key) {

        String selectQuery = "SELECT "+KEY_VALUE+" FROM " + TABLE_DICO+ " WHERE "
                + KEY_KEY + "='" + key+"'";

//        FLAG++;
//        SQLiteDatabase db = this.getWritableDatabase();
        openDB();

        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.getCount() <= 0){
            closeDB();
            return false;
        }
        closeDB();
        return true;
    }

    private void addPositionToLists(){
        LinkedList<GSListe> list = (LinkedList<GSListe>) getAllListe();
        int posN = 0;
        int posR = 0;
        for (GSListe l:list) {
            if(l.getTypeListe()==TypeListe.LISTE_NORMALE){
                l.setPosition(posN);
                posN++;
            }
            else if(l.getTypeListe()==TypeListe.LISTE_RECETTE){
                l.setPosition(posR);
                posR++;
            }
            updateListe(l);
        }
    }

    private void openDB(){
        FLAG++;
        if(FLAG <= 1){
            db = this.getWritableDatabase();
        }
    }

    private void closeDB(){
        FLAG--;
        if(FLAG == 0){
            db.close();
        }
    }

}