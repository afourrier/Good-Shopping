package database;

import com.fourrier.goodshopping.MainActivity;
import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import services.EComparaison;

/**
 * Created by Alexis on 10/02/16.
 */
public class GSItem {
    private int mId;
    private int mIdCategorie;
    private String mIdFirebaseCategorie;
    private String mNom;//+
    private boolean mSelect;//+
    private float mLongueur;//+
    private int mLongueurType;//+
    private float mLargeur;//+
    private int mLargeurType;//+
    private float mHauteur;//+
    private int mHauteurType;//+
    private int mType;//+
    private float mQuantite;//+
    private int mNbFoisAchete;//+
    private String mImage;
    private int mEmplacement;
    private String mColor;
    private String mNomCategorie;
    private String firebaseId;
    private String DEVICE_ID;

    // Utiliser uniquement pour ajouter un element a la bd locale
    public GSItem(String firebaseCategorieId) {
        this.mIdFirebaseCategorie = firebaseCategorieId;
        this.mNom = "";
        this.mSelect = false;
        this.mLongueur = 0;
        this.mLongueurType = 0;
        this.mLargeur = 0;
        this.mLargeurType = 0;
        this.mHauteur = 0;
        this.mHauteurType = 0;
        this.mType = 0;
        this.mQuantite = 0;
        this.mNbFoisAchete = 0;
        this.mImage = "";
        this.mEmplacement = 0;
        this.mColor = "#FFFFFF";
        firebaseId = "";
        DEVICE_ID = MainActivity.DEVICE_ID;
    }

    public GSItem(int id, String nom, boolean select, int type, float quantite){
        mId = id;
        mNom = nom;
        mSelect = select;
        mType = type;
        mQuantite = quantite;
        mLongueur = 0;
        mLargeur = 0;
        mHauteur = 0;
        mLongueurType = 0;
        mLargeurType = 0;
        mHauteurType = 0;
        mNbFoisAchete = 0;
        mImage = "";
        mEmplacement = 0;
        firebaseId = "";
        DEVICE_ID = MainActivity.DEVICE_ID;
    }

    public GSItem(int id, String nom, boolean select, int type, float quantite, int emplacement){
        this(id, nom, select, type, quantite);
        mEmplacement = emplacement;
    }

    public GSItem(int id, String nom, int idCategorie, boolean select, int type, float quantite, int emplacement, String fcId){
        this(id, nom, select, type, quantite, emplacement);
        mIdCategorie = idCategorie;
        mIdFirebaseCategorie = fcId;
    }

    public GSItem(int id, String nom, int id_categorie, boolean select, float longueur, float largeur,
                  float hauteur, float quantite, int nbFoisAchete, String image, int emplacement,
                  int type, int typeLongueur, int tyepeLargeur,
                  int typeHauteur, String fId, String fcId){
        this(id, nom, select, type, quantite, emplacement);
        mIdCategorie = id_categorie;
        mLongueur = longueur;
        mLargeur = largeur;
        mHauteur = hauteur;
        mLongueurType = typeLongueur;
        mLargeurType = tyepeLargeur;
        mHauteurType = typeHauteur;
        mNbFoisAchete = nbFoisAchete;
        mImage = image;
        firebaseId = fId==null?"":fId;
        mIdFirebaseCategorie = fcId;
    }


    public GSItem(HashMap map, int idCategorie){
        mNom = (String)map.get("name");
        mSelect = (Boolean)map.get("select");
        mQuantite = (Float)map.get("quantite");
        mType = (Integer)map.get("type");
        mIdCategorie = idCategorie;
        firebaseId = "";

        mLongueur = 0;
        mLargeur  = 0;
        mHauteur  = 0;

        mLongueurType = 0;
        mLargeurType  = 0;
        mHauteurType  = 0;
        DEVICE_ID = MainActivity.DEVICE_ID;
        if(map.get("longueur") != null){
            HashMap hm = (HashMap)map.get("longueur");
            mLongueur = (Float)hm.get("long");
            mLargeur  = (Float)hm.get("larg");
            mHauteur  = (Float)hm.get("haut");

            mLongueurType = (Integer)hm.get("longType");
            mLargeurType  = (Integer)hm.get("largType");
            mHauteurType  = (Integer)hm.get("hautType");
        }

        if(map.containsKey("nfa"))
            mNbFoisAchete = (Integer)map.get("nfa");
        else
            mNbFoisAchete = 0;
    }

    public int getLongType() {
        return mLongueurType;
    }

    public void setLongueurType(int mLongueurType) {
        this.mLongueurType = mLongueurType;
    }

    public int getLargType() {
        return mLargeurType;
    }

    public void setLargeurType(int mLargeurType) {
        this.mLargeurType = mLargeurType;
    }

    public int getHautType() {
        return mHauteurType;
    }

    public void setHauteurType(int mHauteurType) {
        this.mHauteurType = mHauteurType;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public void setEmplacement(int mEmplacement) {
        this.mEmplacement = mEmplacement;
    }

    public void setNom(String mNom) {
        this.mNom = mNom;
    }

    public void setSelect(boolean mSelect) {
        this.mSelect = mSelect;
    }

    public void setLongueur(float mLongueur) {
        this.mLongueur = mLongueur;
    }

    public void setLargeur(float mLargeur) {
        this.mLargeur = mLargeur;
    }

    public void setHauteur(float mHauteur) {
        this.mHauteur = mHauteur;
    }

    public void setQuantite(float mQuantite) {
        this.mQuantite = mQuantite;
    }

    public void setType(int type) {
        this.mType = type;
    }

    public void setNbFoisAchete(int mNbFoisAchete) {
        this.mNbFoisAchete = mNbFoisAchete;
    }

    public void setImage(String mImage) {
        this.mImage = mImage;
    }

    public void setColor(String color) {
        this.mColor = color;
    }

    @Exclude
    public String getNomCategorie() {
        return mNomCategorie;
    }

    public void setNomCategorie(String mNomCategorie) {
        this.mNomCategorie = mNomCategorie;
    }

    @Exclude
    public int getIdCategorie() {
        return mIdCategorie;
    }

    @Exclude
    public String getIdFirebaseCategorie() {
        return mIdFirebaseCategorie;
    }

    public void setIdFirebaseCategorie(String IdFirebaseCategorie) {
        this.mIdFirebaseCategorie = IdFirebaseCategorie;
    }

    public void setIdCategorie(int mIdCategorie) {
        this.mIdCategorie = mIdCategorie;
    }

    public void achete(){
        mNbFoisAchete++;
    }

    @Exclude
    public int getId()             {return mId;}
    public String getNom()         {return mNom;}
    public boolean isSelect()      {return mSelect;}
    public float getLongueur()     {return mLongueur;}
    public float getLargeur()      {return mLargeur;}
    public float getHauteur()      {return mHauteur;}
    public float getQuantite()     {return mQuantite;}
    public float getNbFoisAchete() {return mNbFoisAchete;}
    public String getDEVICE_ID()   {return DEVICE_ID;}

    @Exclude
    public String getImage()       {return mImage;}
    public int getEmplacement()    {return mEmplacement;}
    @Exclude
    public String getColor()       {return mColor;}
    public int getType()           {return mType;}

    public GSItem clone(){
        GSItem item = new GSItem(mId, mNom,mIdCategorie,mSelect, mLongueur,mLargeur,mHauteur,mQuantite,
                mNbFoisAchete,mImage,mEmplacement,mType,mLongueurType,mLargeurType,mHauteurType,firebaseId,mIdFirebaseCategorie);
        item.setColor(mColor);
        return item;
    }

    public String getFirebaseId() {
        return firebaseId;
    }

    public void setFirebaseId(String firebaseId) {
        this.firebaseId = firebaseId;
    }

    @Exclude
    public boolean isFirebaseIdEmpty(){
        return this.firebaseId.equals("");
    }

    @Override
    public String toString() {
        return "GSItem{" +
                "mId=" + mId +
                ", mIdCategorie=" + mIdCategorie +
                ", mIdFirebaseCategorie='" + mIdFirebaseCategorie + '\'' +
                ", mNom='" + mNom + '\'' +
                ", mSelect=" + mSelect +
                ", mLongueur=" + mLongueur +
                ", mLongueurType=" + mLongueurType +
                ", mLargeur=" + mLargeur +
                ", mLargeurType=" + mLargeurType +
                ", mHauteur=" + mHauteur +
                ", mHauteurType=" + mHauteurType +
                ", mType=" + mType +
                ", mQuantite=" + mQuantite +
                ", mNbFoisAchete=" + mNbFoisAchete +
                ", mImage='" + mImage + '\'' +
                ", mEmplacement=" + mEmplacement +
                ", mColor='" + mColor + '\'' +
                ", mNomCategorie='" + mNomCategorie + '\'' +
                ", firebaseId='" + firebaseId + '\'' +
                '}';
    }

    public boolean equals(HashMap<String,Object> map){
//        double lo = Double.parseDouble( map.get("longueur").toString());
//        double la = Double.parseDouble( map.get("largeur").toString());
//        double ha = Double.parseDouble( map.get("hauteur").toString());
//        double qu = Double.parseDouble( map.get("quantite").toString());
//        int lat   = Math.round(Float.parseFloat(map.get("largType").toString()));
//        int lot   = Math.round(Float.parseFloat(map.get("longType").toString()));
//        int hat   = Math.round(Float.parseFloat(map.get("hautType").toString()));
//        int nbf   = Math.round(Float.parseFloat(map.get("nbFoisAchete").toString()));
//        int t     = Math.round(Float.parseFloat(map.get("type").toString()));
//        int emp   = Math.round(Float.parseFloat(map.get("emplacement").toString()));
//
//        if(!map.get("firebaseId").equals(firebaseId)){
//            return true;
//        }
//        else if(emp != mEmplacement){
//            return false;
//        }
//        else if(!map.get("nom").equals(mNom)){
//            return false;
//        }
////        else if(!map.get("image").equals(mImage)){
////            return false;
////        }
//        else if(lo != mLongueur){
//            return false;
//        }
//        else if((Boolean) map.get("select") != mSelect){
//            return false;
//        }
//        else if(la != mLargeur){
//            return false;
//        }
//        else if(ha != mHauteur){
//            return false;
//        }
//        else if(lat != mLargeurType){
//            return false;
//        }
//        else if(lot != mLongueurType){
//            return false;
//        }
//        else if(nbf != mNbFoisAchete){
//            return false;
//        }
//        else if(hat != mHauteurType){
//            return false;
//        }
//        else if(qu != mQuantite){
//            return false;
//        }
//        else if(t != mType){
//            return false;
//        }
//        return true;

        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            switch (pair.getKey().toString().toLowerCase()){
                case "mlongueur":
                case "longueur":
                    if(mLongueur != (float)Double.parseDouble( map.get(pair.getKey().toString()).toString())){
                        return false;
                    }
                case "mlargeur":
                case "largeur":
                    if(mLargeur != (float)Double.parseDouble( map.get(pair.getKey().toString()).toString())){
                        return false;
                    }
                case "mhauteur":
                case "hauteur":
                    if(mHauteur != (float)Double.parseDouble( map.get(pair.getKey().toString()).toString())){
                        return false;
                    }
                    break;
                case "mquantite":
                case "quantite":
                    if(mQuantite != (float)Double.parseDouble( map.get(pair.getKey().toString()).toString())){
                        return false;
                    }
                case "mlargtype":
                case "largtype":
                    if(mLargeurType != (int) Float.parseFloat( map.get(pair.getKey().toString()).toString())){
                        return false;
                    }
                case "mhauttype":
                case "hauttype":
                    if(mHauteurType != (int) Float.parseFloat( map.get(pair.getKey().toString()).toString())){
                        return false;
                    }
                case "mlongtype":
                case "longtype":
                    if(mLongueurType != (int) Float.parseFloat( map.get(pair.getKey().toString()).toString())){
                        return false;
                    }
                case "mnbfoisachete":
                case "nbfoisachete":
                    if(mNbFoisAchete != (int) Float.parseFloat( map.get(pair.getKey().toString()).toString())){
                        return false;
                    }
                case "mtype":
                case "type":
                    if(mType != (int) Float.parseFloat( map.get(pair.getKey().toString()).toString())){
                        return false;
                    }
                case "memplacement":
                case "emplacement":
                    if(mEmplacement != (int) Float.parseFloat( map.get(pair.getKey().toString()).toString())){
                        return false;
                    }
                case "mnom":
                case "nom":
                    if(!mNom.equals(String.valueOf(map.get(pair.getKey().toString())))){
                        return false;
                    }
                case "mselect":
                case "select":
                    if(mSelect != (Boolean) map.get(pair.getKey().toString())){
                        return false;
                    }
                case "mfirebaseid":
                case "firebaseid":
                    if(!firebaseId.equals(String.valueOf(map.get(pair.getKey().toString())))){
                        return false;
                    }
            }
        }
        return true;
    }

    public EComparaison equals(GSItem item){

        if(!item.getFirebaseId().equals(firebaseId)){
            return EComparaison.FALSE_WITHOUT_EQUALS_FIRABSE_ID;
        }
        else if(!item.getNom().equals(mNom)){
            return EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID;
        }
        else if(item.getQuantite() != mQuantite){
            return EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID;
        }
        else if(item.getType() != mType){
            return EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID;
        }
        else if(item.getLongueur() != mLongueur){
            return EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID;
        }
        else if(item.getLongType() != mLongueurType){
            return EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID;
        }
        else if(item.getLargeur() != mLargeur){
            return EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID;
        }
        else if(item.getLargType() != mLargeurType){
            return EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID;
        }
        else if(item.getHauteur() != mHauteur){
            return EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID;
        }
        else if(item.getHautType() != mHauteurType){
            return EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID;
        }
        else if(item.getNbFoisAchete() != mNbFoisAchete){
            return EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID;
        }
        else if(item.getEmplacement() != mEmplacement){
            return EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID;
        }
        else if(item.isSelect() != mSelect){
            return EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID;
        }
        return EComparaison.TRUE;
    }

    public void setItem(HashMap<String,Object> map){
//        if(map.containsKey("longueur")){
//            double lo = Double.parseDouble( map.get("longueur").toString());
//        }
//        else if (map.containsKey("mLongueur")){
//            double lo = Double.parseDouble( map.get("mLongueur").toString());
//        }
//
//        double la = Double.parseDouble( map.get("largeur").toString());
//        double ha = Double.parseDouble( map.get("hauteur").toString());
//        double qu = Double.parseDouble( map.get("quantite").toString());
//        int lat   = Math.round(Float.parseFloat(map.get("largType").toString()));
//        int lot   = Math.round(Float.parseFloat(map.get("longType").toString()));
//        int hat   = Math.round(Float.parseFloat(map.get("hautType").toString()));
//        int nbf   = Math.round(Float.parseFloat(map.get("nbFoisAchete").toString()));
//        int t     = Math.round(Float.parseFloat(map.get("type").toString()));
//        int emp   = Math.round(Float.parseFloat(map.get("emplacement").toString()));
//        String fId = map.get("firebaseId").toString();
//
//        mEmplacement = emp;
//        mNom = String.valueOf(map.get("nom"));
//        mImage = String.valueOf(map.get("image"));
//        mLongueur = (float) lo;
//        mSelect = (Boolean) map.get("select");
//        mLargeur = (float) la;
//        mHauteur = (float) ha;
//        mLargeurType = lat;
//        mLongueurType = lot;
//        mNbFoisAchete = nbf;
//        mHauteurType = hat;
//        mQuantite = (float) qu;
//        mType = t;
//        firebaseId = fId;


        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            switch (pair.getKey().toString().toLowerCase()){
                case "mlongueur":
                case "longueur":
                    mLongueur = (float)Double.parseDouble( map.get(pair.getKey().toString()).toString());
                    break;
                case "mlargeur":
                case "largeur":
                    mLargeur = (float)Double.parseDouble( map.get(pair.getKey().toString()).toString());
                    break;
                case "mhauteur":
                case "hauteur":
                    mHauteur = (float)Double.parseDouble( map.get(pair.getKey().toString()).toString());
                    break;
                case "mquantite":
                case "quantite":
                    mQuantite = (float)Double.parseDouble( map.get(pair.getKey().toString()).toString());
                    break;
                case "mlargtype":
                case "largtype":
                    mLargeurType = (int) Float.parseFloat( map.get(pair.getKey().toString()).toString());
                    break;
                case "mhauttype":
                case "hauttype":
                    mHauteurType = (int) Float.parseFloat( map.get(pair.getKey().toString()).toString());
                    break;
                case "mlongtype":
                case "longtype":
                    mLongueurType = (int) Float.parseFloat( map.get(pair.getKey().toString()).toString());
                    break;
                case "mnbfoisachete":
                case "nbfoisachete":
                    mNbFoisAchete = (int) Float.parseFloat( map.get(pair.getKey().toString()).toString());
                    break;
                case "mtype":
                case "type":
                    mType = (int) Float.parseFloat( map.get(pair.getKey().toString()).toString());
                    break;
                case "memplacement":
                case "emplacement":
                    mEmplacement = (int) Float.parseFloat( map.get(pair.getKey().toString()).toString());
                    break;
                case "mnom":
                case "nom":
                    mNom = String.valueOf(map.get(pair.getKey().toString()));
                    break;
                case "mselect":
                case "select":
                    mSelect = (Boolean) map.get(pair.getKey().toString());
                    break;
                case "mfirebaseid":
                case "firebaseid":
                    firebaseId = String.valueOf(map.get(pair.getKey().toString()));
                    break;
            }
        }
    }
}
