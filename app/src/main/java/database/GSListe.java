package database;

import com.fourrier.goodshopping.MainActivity;
import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.LinkedList;

import adapters.Item;
import adapters.TypeItemMenu;

/**
 * Created by Alexis on 10/02/16.
 */
public class GSListe implements Item, IDatabaseItems{
    private int mId;
    private String mNom;
    private TypeListe mType;
    private int mNbPersonnes;
    private int mNbPersonnesDemande;
    private LinkedList<GSCategory> mListeCategory;
    private boolean mSelected;
    private boolean visible;
    private String firebaseId;
    private int position;

    public GSListe(String fId){
        mId = 0;
        mNom = "";
        mType = TypeListe.LISTE_NORMALE;
        mNbPersonnes = 1;
        mNbPersonnesDemande = 1;
        mListeCategory = new LinkedList<>();
        mSelected = false;
        visible = false;
        firebaseId = fId==null?"":fId;
    }

    public GSListe(int id, String nom, TypeListe type, int nbp, int nbpd, String fId, int position){
        mId = id;
        mNom = nom;
        mType = type;
        mNbPersonnes = nbp;
        mNbPersonnesDemande = nbpd;
        mListeCategory = new LinkedList<>();
        mSelected = false;
        visible = false;
        firebaseId = fId==null?"":fId;
        this.position = position;
    }

    /**
     *
     * @param listeCate
     * @return une liste de
     */
    public LinkedList remplireCategorie(LinkedList listeCate, LinkedList listeItems){
        int id;
        HashMap cat;
        GSCategory category;
        for(int i=0; i<listeCate.size(); i++){
            cat = ((HashMap)listeCate.get(i));
            id = 0;
            category = new GSCategory(id, (String)cat.get("nomCategorie"),
                    (String)cat.get("color"),0, i, listeItems,"");
            //mListeCategory.add(((HashMap)ll.get(i)).get("nomCategorie"));
            mListeCategory.add(category);
        }
        return mListeCategory;
    }

    /*public static LinkedList<GSListe> remplireListe(HashMap data){
        LinkedList res = new LinkedList();
        GSListe liste;
        LinkedList ll =  (LinkedList) data.get("listes");
        for(int i =0; i< ll.size(); i++) {
            HashMap map = (HashMap) ll.get(i);
            liste = new GSListe(0,
                    (String)map.get("name"), TypeListe.LISTE_NORMALE, 1,1, "");
            liste.remplireCategorie((LinkedList) map.get("categories"),
                    (LinkedList) map.get("listesDeCourses"));
            res.add(liste);
        }
        return res;
    }*/

    @Exclude
    public LinkedList<GSCategory> getListCategories(){
        return mListeCategory;
    }

    public GSCategory getCategorie(int num){
        return mListeCategory.get(num);
    }

    @Exclude
    public GSCategory getCategoryById(int id){
        for (GSCategory cate: mListeCategory) {
            if(cate.getId() == id){
                return cate;
            }
        }
        return mListeCategory.getFirst();
    }

    @Exclude
    public GSCategory getCategoryByFirebaseId(String id){
        for (GSCategory cate: mListeCategory) {
            if(cate.getFirebaseId().equals(id)){
                return cate;
            }
        }
        return null;
    }

    public void setItemForFirebaseId(String fId, GSItem item){
        int i = 0;
        boolean find = false;
        while (i< mListeCategory.size() && !find){
            find = mListeCategory.get(i).setItemForFirebaseId(fId, item);
            i++;
        }
    }

    public void setListeCategorie(LinkedList<GSCategory> listeCategory){
        mListeCategory = listeCategory;
    }

    @Override
    public String toString() {
        return "GSListe{" +
                "mId=" + mId +
                ", mNom='" + mNom + '\'' +
                ", mType=" + mType +
                ", mNbPersonnes=" + mNbPersonnes +
                ", mNbPersonnesDemande=" + mNbPersonnesDemande +
                ", mListeCategory=" + mListeCategory +
                ", mSelected=" + mSelected +
                ", visible=" + visible +
                ", firebaseId='" + firebaseId + '\'' +
                ", position=" + position +
                '}';
    }

    @Exclude
    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getNom() {
        return mNom;
    }

    @Exclude
    @Override
    public void setEmplacement(int emplacement) {
        setPosition(emplacement);
    }

    public void setNom(String mNom) {
        this.mNom = mNom;
    }

    public TypeListe getTypeListe() {
        return mType;
    }

    public void setType(TypeListe mType) {
        this.mType = mType;
    }

    public int getNbPersonnes() {
        return mNbPersonnes;
    }

    public void setNbPersonne(int mNbPersonne) {
        this.mNbPersonnes = mNbPersonne;
    }

    public int getNbPersonnesDemande() {
        return mNbPersonnesDemande;
    }

    public void setNbPersonnsDemande(int mNbPersonnsDemande) {
        this.mNbPersonnesDemande = mNbPersonnsDemande;
    }

    public void addCategorie(GSCategory category){
        mListeCategory.add(category);
    }
    public void setSelected(boolean selected){
        mSelected = selected;
    }

    public String getFirebaseId() {
        return firebaseId;
    }

    public void setFirebaseId(String firebaseId) {
        this.firebaseId = firebaseId;
    }

    @Exclude
    public boolean isFirebaseIdEmpty(){
        return this.firebaseId.equals("");
    }

    @Override
    @Exclude
    public boolean isSelected() {
        return mSelected;
    }

    @Exclude
    @Override
    public String getTitle() {
        return mNom;
    }

    @Exclude
    @Override
    public TypeItemMenu getTypeItemMenu(){
        return TypeItemMenu.ITEM_LISTE;
    }

    @Override
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Exclude
    @Override
    public boolean isVisible() {
        return  visible;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getDEVICE_ID(){ return MainActivity.DEVICE_ID;}

    //Pour firebase
    public HashMap<String, GSCategory> getCategories(){
        HashMap<String, GSCategory> cateMap = new HashMap<>();
        for (GSCategory cate: mListeCategory) {
            cateMap.put(String.valueOf(cate.getFirebaseId()), cate);
        }
        return cateMap;
    }

    public void setCategoryForFirebaseId(GSCategory cate){
        for (int i = 0; i < mListeCategory.size(); i++) {
            if(mListeCategory.get(i).getFirebaseId().equals(cate.getFirebaseId())){
                mListeCategory.set(i, cate);
            }
        }
    }

    public boolean equals(HashMap<String,Object> map){
        int nbp   = Math.round(Float.parseFloat(map.get("nbPersonnes").toString()));
        int nbpd  = Math.round(Float.parseFloat(map.get("nbPersonnesDemande").toString()));
        int pos  = Math.round(Float.parseFloat(map.get("position").toString()));

        if(!map.get("firebaseId").equals(firebaseId)){
            return true;
        }
        else if(nbp != mNbPersonnes){
            return false;
        }
        else if(nbpd != mNbPersonnesDemande){
            return false;
        }
        else if(!map.get("nom").equals(mNom)){
            return false;
        }
        else if(pos != position){
            return false;
        }
        return true;
    }

    public void setList(HashMap<String,Object> map){
        mNbPersonnes = Math.round(Float.parseFloat(map.get("nbPersonnes").toString()));
        mNbPersonnesDemande = Math.round(Float.parseFloat(map.get("nbPersonnesDemande").toString()));
        position = Math.round(Float.parseFloat(map.get("position").toString()));
        mNom = String.valueOf(map.get("nom"));
        mType = TypeListe.stringToEnum(String.valueOf(map.get("typeListe")));
    }
}
