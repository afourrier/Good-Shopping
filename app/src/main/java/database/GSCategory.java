package database;

import com.fourrier.goodshopping.MainActivity;
import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import services.EComparaison;

/**
 * Created by Alexis on 10/02/16.
 */
public class GSCategory implements IDatabaseItems{
    private int mId;
    private String mNom;
    private String mCouleur;
    private int mIdListe;
    private int mEmplacement;
    private LinkedList<GSItem> mListItems;
    private String firebaseId;

    public GSCategory(String fId){
        mId = 0;
        mNom = "";
        mCouleur = "";
        mIdListe = 0;
        mEmplacement = 0;
        mListItems = new LinkedList();
        firebaseId = fId==null?"":fId;
    }

    public GSCategory(int id, String nom, String couleur, int idListe, int emplacement, String fId){
        mId = id;
        mNom = nom;
        mCouleur = couleur;
        mIdListe = idListe;
        mEmplacement = emplacement;
        mListItems = new LinkedList();
        firebaseId = fId==null?"":fId;
    }

    public GSCategory(int id, String nom, String couleur, int idListe, int emplacement, LinkedList<GSItem> listeItem, String fId){
        this(id, nom, couleur, idListe, emplacement, fId);
        mListItems = (LinkedList<GSItem>) listeItem.clone();
//        HashMap map;
//        GSItem item;
//        for(int i=0; i<listeItem.size(); i++){
//            map = ((HashMap)listeItem.get(i));
//            if(map.get("categorie").equals(nom)){
//                item = new GSItem(map, mId);
//                item.setColor(mCouleur);
//                item.setNomCategorie(mNom);
//                mListItems.add(item);
//            }
//        }
    }

    public void add(GSItem item){
        mListItems.add(item);
    }

    public GSCategory clone(){
        return new GSCategory(mId, mNom, mCouleur, mIdListe, mEmplacement, mListItems, firebaseId);
    }

    @Override
    public String toString() {
        return "GSCategory{" +
                "mId=" + mId +
                ", mNom='" + mNom + '\'' +
                ", mCouleur='" + mCouleur + '\'' +
                ", mIdListe=" + mIdListe +
                ", mEmplacement=" + mEmplacement +
                ", mListItems=" + mListItems +
                '}';
    }

    @Exclude
    public GSItem getItem(int pos){
        return mListItems.get(pos);
    }

    public void  setItem(int pos, GSItem item){
        mListItems.set(pos, item);
    }

    public boolean setItemForFirebaseId(String fId, GSItem item){
        boolean find = false;
        int i = 0;
        while (i<mListItems.size() && !find){
            GSItem it = mListItems.get(i);
            if(it.getFirebaseId().equals(fId)){
                mListItems.set(i, item);
                find = true;
            }
            i++;
        }
        return find;
    }

    @Exclude
    public LinkedList<GSItem> getListItems(){
        return mListItems;
    }

    @Exclude
    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getNom() {
        return mNom;
    }

    public void setNom(String mNom) {
        this.mNom = mNom;
    }

    public String getCouleur() {
        return mCouleur;
    }

    public void setCouleur(String mCouleur) {
        this.mCouleur = mCouleur;
    }

    @Exclude
    public int getIdListe() {
        return mIdListe;
    }

    public void setIdListe(int mIdListe) {
        this.mIdListe = mIdListe;
    }

    public int getEmplacement() {
        return mEmplacement;
    }

    public void setEmplacement(int mEmplacement) {
        this.mEmplacement = mEmplacement;
    }

    public void setListItems(LinkedList<GSItem> mListItems) {
        this.mListItems = mListItems;
    }

    public void addItem(GSItem item){
        mListItems.add(item);
    }

    public void addItem(int pos, GSItem item){
        mListItems.add(pos, item);
    }

    public String getFirebaseId() {
        return firebaseId;
    }

    public void setFirebaseId(String firebaseId) {
        this.firebaseId = firebaseId;
    }

    public String getDEVICE_ID(){ return MainActivity.DEVICE_ID;}

    @Exclude
    public boolean isFirebaseIdEmpty(){
        return this.firebaseId.equals("");
    }

    //Pour firebase
    public HashMap<String, GSItem> getItems(){
        HashMap<String, GSItem> itemsMap = new HashMap<>();
        for (GSItem item:mListItems) {
            itemsMap.put(String.valueOf(item.getFirebaseId()), item);
        }
        return itemsMap;
    }

    public boolean equals(HashMap<String,Object> map){
        int emp   = Math.round(Float.parseFloat(map.get("emplacement").toString()));

        if(!map.get("firebaseId").equals(firebaseId)){
            return true;
        }
        else if(emp != mEmplacement){
            return false;
        }
        else if(!map.get("nom").equals(mNom)){
            return false;
        }
        else if(!map.get("couleur").equals(mCouleur)){
            return false;
        }
        return true;
    }

    public EComparaison equals(GSCategory category){

        if(!category.getFirebaseId().equals(firebaseId)){
            return EComparaison.FALSE_WITHOUT_EQUALS_FIRABSE_ID;
        }
        else if(category.getEmplacement() != mEmplacement){
            return EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID;
        }
        else if(!category.getNom().equals(mNom)){
            return EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID;
        }
        else if(!category.getCouleur().equals(mCouleur)){
            return EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID;
        }
        return EComparaison.TRUE;
    }

    public void setCatgegory(HashMap<String,Object> map){
        int emp   = Math.round(Float.parseFloat(map.get("emplacement").toString()));

        mEmplacement = emp;
        mNom = String.valueOf(map.get("nom"));
        mCouleur = String.valueOf(map.get("couleur"));
    }

    public ArrayList<Integer> listerLesIdDesItems(){
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (GSItem it:mListItems) {
            arrayList.add(it.getId());
        }
        return arrayList;
    }
}
