package adapters;

import database.TypeListe;

public interface Item {

  boolean isSelected();
  TypeItemMenu getTypeItemMenu();
  TypeListe getTypeListe();
  String getTitle();
  void setSelected(boolean selected);
  void setVisible(boolean visible);
  boolean isVisible();
  int getId();
  String getFirebaseId();

  String getNom();
}