package adapters;

import database.TypeListe;

public class AddItemMenu implements Item{
	 
	private String title;
	private TypeListe typeListe;
	private boolean visible;
	 
	public AddItemMenu(String title, TypeListe typeListe) {
		this.title = title;
		this.typeListe = typeListe;
	}
	  
	@Override
	public boolean isSelected() {
		 return true;
	 }
	 
	public String getTitle(){
		 return title;
	 }

	@Override
	public void setSelected(boolean selected) {}

	@Override
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	@Override
	public boolean isVisible() {
		return  visible;
	}

	@Override
	public int getId() {
		return -1;
	}

	@Override
	public String getFirebaseId() {
		return "";
	}

	@Override
	public String getNom() {
		return title;
	}

	public TypeListe getTypeListe(){return typeListe;}

	public TypeItemMenu getTypeItemMenu(){
		return TypeItemMenu.NOUVEAU_ITEM;
	}
}
