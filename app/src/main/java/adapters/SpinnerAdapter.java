package adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fourrier.goodshopping.MainActivity;
import com.fourrier.goodshopping.R;

import services.GestionCouleur;

public class SpinnerAdapter extends ArrayAdapter { 
private String[] mListTitre;
private String[] mListColor;
private Context mContext;

	public SpinnerAdapter(Context ctx, int txtViewResourceId, String[] name, String[] couleur) {
		super(ctx, txtViewResourceId, name);
		mContext = ctx;
		mListTitre = name;
		mListColor = couleur;
	} 
	
	@Override 
	public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
		return getCustomView(position, cnvtView, prnt); 
	} 
	
	@Override public View getView(int pos, View cnvtView, ViewGroup prnt) {
		return getCustomView(pos, cnvtView, prnt); 
	} 
	
	public View getCustomView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = ((MainActivity) mContext).getLayoutInflater(); 
		View mySpinner = inflater.inflate(R.layout.item_spinner, parent, false);
		String color = mListColor[position];
		LinearLayout layout = (LinearLayout) mySpinner .findViewById(R.id.linearLayoutItemSpinner);
		TextView main_text = (TextView) mySpinner .findViewById(R.id.textViewCategorieSpinner); 
		main_text.setText(mListTitre[position]);
		main_text.setTextColor(GestionCouleur.getTextColor(color));
		layout.setBackgroundColor(Color.parseColor(color));
		
		return mySpinner; 
	}


}
