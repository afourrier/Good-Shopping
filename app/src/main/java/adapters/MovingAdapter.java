package adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fourrier.goodshopping.R;
import com.nhaarman.listviewanimations.ArrayAdapter;

import java.util.LinkedList;

import database.GSCategory;
import database.ETypeDatabaseItem;
import database.IDatabaseItems;
import services.GestionCouleur;
import services.GestionListView;

public class MovingAdapter extends ArrayAdapter{
	private LinkedList<IDatabaseItems> liste;
	private Context mContext;
	private boolean move;
	private GestionListView mGestionLV;
	private ETypeDatabaseItem type;

	public MovingAdapter(Context context, LinkedList<IDatabaseItems> ll, boolean move,
                         GestionListView glv, ETypeDatabaseItem type) {
		super();
		mContext = context;
		liste = (LinkedList<IDatabaseItems>)ll.clone();
		mGestionLV = glv;
		this.move = move;
		this.type = type;
		IDatabaseItems itm;
		for(int i=0; i<liste.size();i++){
			itm = liste.get(i);
			add(itm.getNom());
		}
	}

	@Override
	public long getItemId(int position) {
		    return getItem(position).hashCode();
		}
		
	@Override
	public boolean hasStableIds(){
			return true;
		}
		
		
		 
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		IDatabaseItems itm = liste.get(position);


		View rowView = convertView;
		if(move){
			if (rowView == null) {
				rowView = LayoutInflater.from(mContext).inflate(R.layout.item_label, parent, false);
			}
			TextView textView = (TextView) rowView.findViewById(R.id.label);
			LinearLayout lay  = (LinearLayout)rowView.findViewById(R.id.layout_item_label);
			textView.setText(itm.getNom());

			if(type == ETypeDatabaseItem.CATEGORY) {
				GSCategory category = (GSCategory)itm;
				String color = category.getCouleur();
				lay.setBackgroundColor(Color.parseColor(color));
				textView.setTextColor(GestionCouleur.getTextColor(color));
			}
		}
		else{
			if(rowView == null)
				rowView = LayoutInflater.from(mContext).inflate(R.layout.item_choix_categorie, parent, false);
			final TextView tv  = (TextView) rowView.findViewById(R.id.textViewChoixCategorie);
			final LinearLayout ll = (LinearLayout)rowView.findViewById(R.id.layoutChoixCategorie);
			tv.setText(itm.getNom());

			if(type == ETypeDatabaseItem.CATEGORY) {
				GSCategory category = (GSCategory) itm;
				String color = category.getCouleur();
				ll.setBackgroundColor(Color.parseColor(color));
				tv.setTextColor(GestionCouleur.getTextColor(color));
				ll.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						mGestionLV.setCategorieSelected((GSCategory) liste.get(position));
					}
				});
			}

		}
		    
		return rowView;
	}


	public void move(int debut, int fin){
		liste.get(debut).setEmplacement(fin);
		IDatabaseItems categorie = liste.get(debut);
		if(fin<debut){
			for(int i = debut; i> fin; i--){
				liste.get(i-1).setEmplacement(i);
				liste.set(i, liste.get(i-1));
			}
			liste.set(fin, categorie);
		}
		else{
			for(int i = debut; i< fin; i++){
				liste.get(i+1).setEmplacement(i);
				liste.set(i, liste.get(i + 1));
			}

			liste.set(fin, categorie);
		}

	}

	public LinkedList<IDatabaseItems> getListe() {
		return liste;
	}
}