package adapters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

import com.fourrier.goodshopping.MainActivity;
import com.fourrier.goodshopping.NavigationDrawerFragment;
import com.fourrier.goodshopping.R;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import database.GSListe;
import database.TypeListe;
import firebase.auth.AuthActivity;
import services.GestionListes;
import services.TypePopup;

public class MenuAdapter extends ArrayAdapter<Item> {
 
	private MainActivity activity;
	private ArrayList<Item> items;
	private NavigationDrawerFragment navigationDrawerFragment;
	private LayoutInflater vi;
	private HashMap<TypeListe,ArrayList> mapTypeListe;

//	private HashMap<TypeListe, Integer> mapPosType;
	private HashMap<TypeListe, Boolean> mapVisibilityType;

	public MenuAdapter(MainActivity activity, ArrayList<Item> items,
                       NavigationDrawerFragment menuGauche) {
		super(activity,0, items);
		this.activity = activity;
		this.items = items;
		navigationDrawerFragment = menuGauche;
		vi = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//		remplireHashMapTypeListe();
//		viderItems();
		remplireListe();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		View v = convertView;
 
		final Item i = items.get(position);
		if (i != null) {

			if(i.getTypeItemMenu() == TypeItemMenu.SEPARATEUR){
				final SeparateurItemMenu sim = (SeparateurItemMenu) i;
				v = vi.inflate(R.layout.item_menu_separator, null);
				final ImageView img   = (ImageView) v.findViewById(R.id.imageViewItemSeparator);
				final TextView title  = (TextView) v.findViewById(R.id.textViewItemSeparator);
				final LinearLayout ll = (LinearLayout) v.findViewById(R.id.linearLayoutItemSeparator);

				if (title != null)
					title.setText(sim.getTitle());

				if(items.get(position).getTypeListe() == TypeListe.PARAMETRES){
					img.setImageResource(R.drawable.ic_person);
					title.setText(activity.isUserConnected()?R.string.paremeters:R.string.connexion);
				}
				else if(items.get(position).getTypeListe() == TypeListe.REFRESH_FIREBASE){
                    img.setImageResource(R.drawable.ic_cloud_download_black);
                    title.setText(R.string.firebase_refresh);
                }
                else if(items.get(position).getTypeListe() == TypeListe.NOTE){
                    img.setImageResource(R.drawable.ic_star_black);
                    title.setText(R.string.note);
                }
				else {//Rotation
					if (sim.isAnimate()) {
						float debut = 90f;
						float fin = 0f;
						if (sim.isSelected()) {
							debut = 0f;
							fin = 90f;
						}

						RotateAnimation anim = new RotateAnimation(debut, fin,
								Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
						anim.setInterpolator(new LinearInterpolator());
						//anim.setRepeatCount(Animation.INFINITE);
						anim.setDuration(500);
						anim.setFillAfter(true);
						anim.setFillEnabled(true);
						img.startAnimation(anim);
						sim.setAnimate(false);
					} else {
						if (sim.isSelected()) {
							img.setRotation(90);
						}
					}
				}

				ll.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {

						sim.setSelected(!sim.isSelected());
						sim.setAnimate(true);
//						changerVisibilite(sim.getTypeListe(), sim.isSelected());
//						notifyDataSetChanged();
						if(items.get(position).getTypeListe() == TypeListe.PARAMETRES) {
							Intent myIntent = new Intent(activity, AuthActivity.class);
							activity.startActivity(myIntent);
							return;
						}
						else if(items.get(position).getTypeListe() == TypeListe.REFRESH_FIREBASE) {
                            ((MainActivity)navigationDrawerFragment.getActivity()).refreshFirebase();
                            navigationDrawerFragment.close();
                        }
                        else if(items.get(position).getTypeListe() == TypeListe.NOTE) {
                            ((MainActivity)navigationDrawerFragment.getActivity()).noter();
                            navigationDrawerFragment.close();
                        }
						else {
							mapVisibilityType.put(sim.getTypeListe(), !mapVisibilityType.get(sim.getTypeListe()));
						}
						notifyDataSetChanged();
					}
				});

			}
			else if(i.getTypeItemMenu() == TypeItemMenu.ITEM_LISTE) {

				v = vi.inflate(R.layout.item_menu, null);
				final ImageView img = (ImageView) v.findViewById(R.id.img);
				final TextView title = (TextView) v.findViewById(R.id.titre);
				final LinearLayout ll = (LinearLayout) v.findViewById(R.id.linearLayoutItemMenu);
				final Item liste = i;

				if (!mapVisibilityType.get(liste.getTypeListe())){
					v.setVisibility(View.GONE);
					img.setVisibility(View.GONE);
					title.setVisibility(View.GONE);
					ll.setVisibility(View.GONE);
					return v;
				}

				if (i.isSelected()) {
					if (img != null) {//Choix tu type d'image
//						ll.setBackgroundResource(R.color.curentList);
						img.setImageResource(R.drawable.ic_shopping_cart_black);
					}
					if (title != null) {
						title.setText(liste.getNom());
					}
				} else {
					if (title != null) {
						title.setText(liste.getNom());
					}
				}

				ll.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						activity.changerDeListeDeCourses(false, liste.getId(), liste.getTypeListe());
//						setSelected(position);
						unselectAll();
						i.setSelected(true);
						notifyDataSetChanged();
					}
				});

				ll.setOnLongClickListener(new View.OnLongClickListener() {
					@Override
					public boolean onLongClick(View v) {
						new GestionListes(TypePopup.OLD, position, activity,
								navigationDrawerFragment, liste.getId(), liste.getTypeListe());
						return false;
					}
				});

				//if(!liste.isVisible())
				//	ll.setVisibility(View.GONE);

			}
			else if(i.getTypeItemMenu() == TypeItemMenu.NOUVEAU_ITEM) {
				v = vi.inflate(R.layout.item_menu, null);
				final ImageView img = (ImageView) v.findViewById(R.id.img);
				final TextView title = (TextView) v.findViewById(R.id.titre);
				final LinearLayout ll = (LinearLayout) v.findViewById(R.id.linearLayoutItemMenu);
				final View trait      = v.findViewById(R.id.viewSeparator);

				if (!mapVisibilityType.get(i.getTypeListe())){
					v.setVisibility(View.GONE);
					img.setVisibility(View.GONE);
					title.setVisibility(View.GONE);
					ll.setVisibility(View.GONE);
					trait.setVisibility(View.GONE);
					return v;
				}

				img.setImageResource(R.drawable.plus);
				title.setText(R.string.add);
				title.setTextColor(activity.getResources().getColor(R.color.separateur));
				trait.setVisibility(View.INVISIBLE);

				ll.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						new GestionListes(TypePopup.NEW, position, activity,
								navigationDrawerFragment, 0, (i).getTypeListe());
					}
				});
				//if(!((AddItemMenu)i).isVisible())
				//	ll.setVisibility(View.GONE);

			}
		}
		return v;
	}
	
	public void add(Item newItem){
		for (Item item: items) {
			if(item.getId() == newItem.getId()){
                return;
			}
		}
		Iterator<Item> iteratorItem = items.iterator();
		boolean insert = false;
		int pos = 0;
		while (iteratorItem.hasNext() && !insert){
			Item item = iteratorItem.next();
			if(item.getTypeItemMenu() == TypeItemMenu.NOUVEAU_ITEM
					&& item.getTypeListe() == newItem.getTypeListe()){
				items.add(pos, newItem);
                //insert(newItem, pos);
				insert = true;
			}
			pos++;
		}

//		notifyDataSetChanged();
	}



	public void remove(int idListe){
        int pos = 0;
        Iterator<Item> iteratorList = items.iterator();
		while (iteratorList.hasNext()) {
			Item list = iteratorList.next();
			if (list.getId() == idListe) {
                iteratorList.remove();
                super.remove(list);
			}
			pos++;
		}


//		GSListe liste = (GSListe)items.get(position);
//		//On le supprime du hashmap
//		ArrayList<Item> array = mapTypeListe.get(liste.getTypeListe());
//		for(int i=0; i<array.size();i++){
//			if((array.get(i)).getId()==liste.getId()){
//				array.remove(i);
//			}
//		}
//		items.remove(position);
		//On selectionne la premiere liste
		items.get(1).setSelected(true);
//		setSelected(1);
	}

	@Override
	public void remove(@Nullable Item object) {
//		super.remove(object);


//		TypeListe[] tl = new TypeListe[]{TypeListe.LISTE_NORMALE,TypeListe.LISTE_RECETTE};//Ajouter les type de liste que l'on peut supprimer
		Iterator<Item> iteratorList = items.iterator();
		while (iteratorList.hasNext()) {
			Item list = iteratorList.next();
			if (list.getTypeItemMenu()==TypeItemMenu.ITEM_LISTE &&
			        object.getFirebaseId().equals(list.getFirebaseId())) {
				iteratorList.remove();
            }
		}
//		for (TypeListe type:tl) {
//			Iterator<Item> iteratorList = mapTypeListe.get(type).iterator();
//			while (iteratorList.hasNext()) {
//				Item list = iteratorList.next();
//				if ((object).getFirebaseId().equals(list.getFirebaseId())) {
//					iteratorList.remove();
//				}
//			}
//		}
	}
	
	public void rename(int position, GSListe liste){
		//items.set(position, new AddItemMenu(String.valueOf(R.drawable.image_appli_noir_blanc), name));
		activity.modifierListe(position, liste);
		for(int i=0; i<items.size(); i++){
			if(items.get(i).getTypeItemMenu()== TypeItemMenu.ITEM_LISTE) {
				if ((items.get(i)).getId() == liste.getId())
					items.set(i, liste);
			}
		}
		setSelected(position);
	}

	public void rename(GSListe liste){
		for(int i=0; i<items.size(); i++){
			if(items.get(i).getId()== liste.getId()) {
				items.set(i, liste);
			}
		}
	}


	public void recharger(TypeListe typeListe){
		changerVisibilite(typeListe, false);
		changerVisibilite(typeListe, true);
	}
	
	public void setSelected(int position){
		for(int i=0; i<items.size(); i++){
			if(i!=position &&
					items.get(i).getTypeItemMenu()==TypeItemMenu.ITEM_LISTE){
				items.get(i).setSelected(false);
			}
			if(i==position &&
					items.get(i).getTypeItemMenu()==TypeItemMenu.ITEM_LISTE){
				items.get(i).setSelected(true);
				//setSelectedByIdVisibleOrNot((items.get(i)).getId());
			}
		}
	}

	public void setSelectedById(int id){
		for(int i=0; i<items.size(); i++){
			if(items.get(i).getTypeItemMenu()==TypeItemMenu.ITEM_LISTE){
				if((items.get(i)).getId()==id)
					items.get(i).setSelected(true);
				else
					items.get(i).setSelected(false);
			}
		}
		setSelectedByIdVisibleOrNot(id);
	}

	private void setSelectedByIdVisibleOrNot(int id){
		Set cles = mapTypeListe.keySet();
		Iterator it = cles.iterator();
		while (it.hasNext()){
			Object cle = it.next();
			ArrayList<Item> array = mapTypeListe.get(cle);
			for(int i=0; i<array.size();i++){
				if(array.get(i).getId()==id)
					array.get(i).setSelected(true);
				else
					array.get(i).setSelected(false);
			}
		}
	}

	public void changerVisibilite(TypeListe typeListe, boolean visible){

		if(typeListe == TypeListe.PARAMETRES){
			Intent myIntent = new Intent(activity, AuthActivity.class);
			activity.startActivity(myIntent);
			return;
		}

		if(visible){
			int i=0;
			boolean find = false;
			//On cherche l'item de separation du type de liste
			while (i<items.size() && !find){
				if(items.get(i).getTypeListe()==typeListe &&
						items.get(i).getTypeItemMenu()==TypeItemMenu.SEPARATEUR){
					//On ajoute tous les item dessous
					items.addAll(i+1, mapTypeListe.get(typeListe));
					items.add(i+mapTypeListe.get(typeListe).size()+1,new AddItemMenu("", typeListe));
					find = true;
				}
				i++;
			}
		}
		else{
			for(int i=items.size()-1; i>0;i--){
				if(items.get(i).getTypeListe()==typeListe &&
						items.get(i).getTypeItemMenu()!=TypeItemMenu.SEPARATEUR){
					//items.get(i).setVisible(visible);
					items.remove(i);
				}
			}
		}
	}

	public void remplireListe(){
		mapVisibilityType = new HashMap<>();

		mapVisibilityType.put(TypeListe.LISTE_NORMALE,false);
		mapVisibilityType.put(TypeListe.LISTE_RECETTE,false);
		mapVisibilityType.put(TypeListe.LISTE_COMPOSE,false);

//		addAllItems();
	}

	//La liste est remplie avant
	public void addAllItems(){
        LinkedList<GSListe> ll = ((MainActivity)navigationDrawerFragment.getActivity()).getAllListes();
        for (GSListe l:ll) {
            add(l);
        }
    }

	public void remplireHashMapTypeListe(){
		mapTypeListe = new HashMap<>();

		ArrayList<Item> listN = new ArrayList<>();
		ArrayList<Item> listF = new ArrayList<>();
		ArrayList<Item> listR = new ArrayList<>();

		//Liste normale
		for(int i=0; i<items.size();i++){
			if(items.get(i).getTypeListe()==TypeListe.LISTE_NORMALE
					&& items.get(i).getTypeItemMenu()== TypeItemMenu.ITEM_LISTE)
				listN.add(items.get(i));
			else if(items.get(i).getTypeListe()==TypeListe.LISTE_COMPOSE
					&& items.get(i).getTypeItemMenu()== TypeItemMenu.ITEM_LISTE)
				listF.add(items.get(i));
			else if(items.get(i).getTypeListe()==TypeListe.LISTE_RECETTE
					&& items.get(i).getTypeItemMenu()== TypeItemMenu.ITEM_LISTE)
				listR.add(items.get(i));
		}
		mapTypeListe.put(TypeListe.LISTE_NORMALE, listN);
		mapTypeListe.put(TypeListe.LISTE_COMPOSE, listF);
		mapTypeListe.put(TypeListe.LISTE_RECETTE, listR);



	}

	private void viderItems(){
		for(int i=items.size()-1; i>0;i--){
			if(items.get(i).getTypeItemMenu()!= TypeItemMenu.SEPARATEUR)
				items.remove(i);
		}
	}

	public void unselectAll(){
		for (Item item:items) {
			if(item.getTypeItemMenu()==TypeItemMenu.ITEM_LISTE){
				item.setSelected(false);
			}
		}
	}

    public void reloadList(ArrayList<Item> list){
        remplireListe();
        super.clear();
        items = (ArrayList<Item>) list.clone();
        super.addAll(items);

    }



}