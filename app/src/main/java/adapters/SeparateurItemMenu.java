package adapters;

import database.TypeListe;

/**
 * Created by Alexis on 09/05/16.
 */
public class SeparateurItemMenu implements Item{
    private String mNom;
    private boolean mSelected;
    private TypeListe typeListe;
    private boolean animate;

    public SeparateurItemMenu(String nom, boolean selected, TypeListe typeListe){
        mNom = nom;
        mSelected = selected;
        this.typeListe = typeListe;
        animate = false;
    }

    public void setSelected(boolean selected){
        mSelected = selected;
    }

    @Override
    public void setVisible(boolean visible) {}

    @Override
    public boolean isVisible() {
        return  true;
    }

    @Override
    public int getId() {
        return -2;
    }

    @Override
    public String getFirebaseId() {
        return "";
    }

    @Override
    public String getNom() {
        return mNom;
    }

    @Override
    public boolean isSelected() {
        return mSelected;
    }

    @Override
    public TypeItemMenu getTypeItemMenu() {
        return TypeItemMenu.SEPARATEUR;
    }

    @Override
    public TypeListe getTypeListe() {
        return typeListe;
    }

    @Override
    public String getTitle() {
        return mNom;
    }

    public boolean isAnimate(){
        return animate;
    }

    public void setAnimate(boolean animate){
        this.animate = animate;
    }
}
