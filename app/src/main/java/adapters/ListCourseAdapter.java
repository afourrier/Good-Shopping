package adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fourrier.goodshopping.MainActivity;
import com.fourrier.goodshopping.R;
import com.nhaarman.listviewanimations.ArrayAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.dragdrop.GripView;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import database.GSItem;
import services.GestionCouleur;
import services.GestionImage;
import services.Useful;

public class ListCourseAdapter extends ArrayAdapter {
	private LinkedList<GSItem> liste;
	private String[] mListeTypeQuantite;
	private String[] mListeLongueur;
	private String   mCategorieSelected;
	private boolean mInverse;
	private MainActivity mActivity;

	public ListCourseAdapter(MainActivity context, LinkedList<GSItem> ll,
							 String categorieSelected, boolean inverse) {
		super();
		mActivity = context;
		mInverse = inverse;
		liste = (LinkedList<GSItem>)ll.clone();
		mCategorieSelected = categorieSelected;
		List<String> lines = Arrays.asList(context.getResources().getStringArray(R.array.liste_type));
		mListeTypeQuantite = lines.toArray(new String[9]);
		List<String> linesLong = Arrays.asList(context.getResources().getStringArray(R.array.liste_longueurs));
		mListeLongueur  = linesLong.toArray(new String[6]);
		GSItem item;
		for(int i=0; i<ll.size();i++){
			item = liste.get(i);
			super.add(item);
		}
	}

	@Override
	public long getItemId(int position) {
        return getItem(position).hashCode();
	}//TOUJOURS UTILISER getItem(position).hashCode()
		
	@Override
	public boolean hasStableIds(){
			return true;
		}

	public void add(GSItem item){
		super.add(item);
		liste.add(item);
	}

    @Override
    public void add(int index, @NonNull Object item) {
        super.add(index, item);
        liste.add(index, (GSItem) item);
    }

    public void setItem(GSItem item){
		for (int j = 0; j < liste.size(); j++) {
			GSItem i = liste.get(j);
			if(i.getId() == item.getId()){
				liste.set(j, item);
			}
		}
	}

	public void removeItem(GSItem item){
		Iterator<GSItem> iterator = liste.iterator();
		while (iterator.hasNext()){
			GSItem it = iterator.next();
			if(it.getId()==item.getId()){
				super.remove(it);
				iterator.remove();
			}
		}
	}

    @NonNull
    @Override
    public Object remove(int location) {
        liste.remove(location);
        return super.remove(location);
    }

    @Override
	public View getView(final int position, View convertView, ViewGroup parent) {
        final GSItem item;
		View rowView = convertView;
		if (rowView == null) {
			rowView = LayoutInflater.from(mActivity).inflate(R.layout.item_courses, parent, false);
		}

		item = liste.get(position);

		TextView textView  = (TextView) rowView.findViewById(R.id.label);
		RelativeLayout lay = (RelativeLayout)rowView.findViewById(R.id.layout_item_courses);
		CheckBox cb        = (CheckBox)rowView.findViewById(R.id.checkBoxListCourses);
		GripView grip      = (GripView)rowView.findViewById(R.id.list_courses_draganddrop_touchview);
		ImageView image    = (ImageView)rowView.findViewById(R.id.imageViewImagePresent);

		String color = item.getColor();
		GestionCouleur.EColor imgColor = GestionCouleur.getImageColor(color);

		if(item.getImage().equals(""))
			image.setVisibility(View.GONE);
		else {
			image.setImageResource(imgColor== GestionCouleur.EColor.BLACK?
			R.drawable.ic_crop_original_black:R.drawable.ic_crop_original_white);
			image.setVisibility(View.VISIBLE);
		}
		image.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
//					mActivity.showImage();
					new GestionImage(mActivity, item);
//					Intent intent = new Intent(mActivity, PictureActivity.class);
//					intent.putExtra("imageName", item.getImage());
//					mActivity.startActivity(intent);
				}
			});
		    
		if(mCategorieSelected.equals("all"))
			grip.setVisibility(View.INVISIBLE);


		String ligne = item.getNom();
		boolean select = item.isSelect();

		//Affichage des quantites
		ligne+= Useful.affichageQuantite(item, mListeLongueur,
				mListeTypeQuantite, mActivity.getListeActive());

		textView.setText(ligne);
		//TODO verifier le nombre de lignes / text qui "roule"
		lay.setMinimumHeight(400);
		lay.setBackgroundColor(Color.parseColor(color));
		if(mInverse) {
			cb.setChecked(!select);
		}
		else {
			cb.setChecked(select);
		}

		if(cb.isChecked()){
			cb.setButtonDrawable(imgColor== GestionCouleur.EColor.BLACK?
					R.drawable.ic_check_box_black:R.drawable.ic_check_box_white);
		}
		else {
			cb.setButtonDrawable(imgColor== GestionCouleur.EColor.BLACK?
					R.drawable.ic_check_box_outline_blank_black:R.drawable.ic_check_box_outline_blank_white);
		}
		//if(cb.isChecked()){
		//	textView.getPaint().setStrikeThruText(true);
		//}
		textView.setTextColor(GestionCouleur.getTextColor(color));
		    
		cb.setTag (position);
        textView.setTag(item.getId());
		     
		return rowView;
	}

	@Override
	public int getCount() {
		return liste.size();
	}
		
	public void move(int debut, int fin){
		//int a,b,c ;
		GSItem item = liste.get(debut);
		//HashMap map = (HashMap) liste.get(debut);
		if(fin<debut){//Cocher
			for(int i = debut; i> fin; i--){
				if((i-1)>=0)
					liste.set(i, liste.get(i-1));
			}
			liste.set(fin, item);
		}
		else{
			for(int i = debut; i< fin; i++){
				liste.set(i, liste.get(i+1));
			}
			liste.set(fin, item);
		}
	}
}
