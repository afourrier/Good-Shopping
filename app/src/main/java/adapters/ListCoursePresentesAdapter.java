package adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fourrier.goodshopping.R;
import com.nhaarman.listviewanimations.itemmanipulation.dragdrop.GripView;

import java.util.ArrayList;

import database.GSListe;
import database.TypeListe;

public class ListCoursePresentesAdapter extends ArrayAdapter<GSListe> {
	private ArrayList<GSListe> liste;
	private Activity mActivity;
	private ArrayList<GSListe> mListesChecked;
	private ArrayList<Integer> mChecked;

	public ListCoursePresentesAdapter(Activity context, ArrayList<GSListe> ll, ArrayList<Integer> selected) {
		super(context, 0, ll);
		mActivity = context;
		liste = (ArrayList<GSListe>) ll.clone();
		mListesChecked = new ArrayList<>();
		mChecked = selected;
	}

	@Override
	public long getItemId(int position) {
		return getItem(position).hashCode();
	}//TOUJOURS UTILISER getItem(position).hashCode()
		
	@Override
	public boolean hasStableIds(){
			return true;
		}

	public void add(GSListe item){
		super.add(item);
	}
		
		 
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
			
		final GSListe item;
		View rowView = convertView;
		if (rowView == null) {
			rowView = LayoutInflater.from(mActivity).inflate(R.layout.item_courses, parent, false);
		}
		item = liste.get(position);

		TextView textView  = (TextView) rowView.findViewById(R.id.label);
		RelativeLayout lay = (RelativeLayout)rowView.findViewById(R.id.layout_item_courses);
		final CheckBox cb        = (CheckBox)rowView.findViewById(R.id.checkBoxListCourses);
		GripView grip      = (GripView)rowView.findViewById(R.id.list_courses_draganddrop_touchview);
		ImageView image    = (ImageView)rowView.findViewById(R.id.imageViewImagePresent);

		image.setVisibility(View.GONE);

		grip.setVisibility(View.GONE);


		String ligne = "[" + TypeListe.toString(mActivity, item.getTypeListe()) + "] " + item.getNom();
		System.out.println(ligne);
		textView.setText(ligne);
		//TODO verifier le nombre de lignes
		lay.setMinimumHeight(400);

		if(mChecked.contains(position)){
			cb.setChecked(true);
		}

		if(cb.isChecked()){
			cb.setButtonDrawable(R.drawable.ic_check_box_black);
		}
		else {
			cb.setButtonDrawable(R.drawable.ic_check_box_outline_blank_black);
		}

		cb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(cb.isChecked()){
					cb.setButtonDrawable(R.drawable.ic_check_box_black);
					mListesChecked.add(item);
				}
				else {
					cb.setButtonDrawable(R.drawable.ic_check_box_outline_blank_black);
					mListesChecked.remove(item);
				}
			}
		});

		return rowView;
	}

	public ArrayList<GSListe> getListeChecked(){
		return mListesChecked;
	}

}
