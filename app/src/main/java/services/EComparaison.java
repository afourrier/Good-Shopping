package services;

/**
 * Created by Alexis on 24/06/2017.
 */

public enum EComparaison {
    TRUE, FALSE_WITH_EQUALS_FIRABSE_ID, FALSE_WITHOUT_EQUALS_FIRABSE_ID;
}
