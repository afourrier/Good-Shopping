package services;

import java.util.ArrayList;
import java.util.LinkedList;

import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.fourrier.goodshopping.MainActivity;
import com.fourrier.goodshopping.NavigationDrawerFragment;
import com.fourrier.goodshopping.R;

import adapters.ListCoursePresentesAdapter;
import database.GSListe;
import database.GSItem;
import database.ListeCompose;
import database.TypeListe;

public class GestionListes {
	private MaterialDialog alertDialog;
	private GSListe liste;
	private EditText etL;
	private CheckBox checkBox;
	private LinkedList<GSListe> listes;
	private MainActivity activity;
	private LinearLayout layoutPersonnes;
	private EditText editTextNbp;
	private int nbpDebut;
	
	public GestionListes(final TypePopup type, final int position, final MainActivity activity,
						 final NavigationDrawerFragment menuGauche, final int idListe,
						 final TypeListe typeListe){
		
		this.activity = activity;
		listes = activity.getAllListes();
		nbpDebut = 0;

		if(typeListe==TypeListe.LISTE_COMPOSE){
			listeJointe(type, position, activity, menuGauche, idListe, typeListe);
		}
		else {
			listeNormal(type, position, activity, menuGauche, idListe, typeListe);
		}
	}

	private void listeNormal(final TypePopup type, final int position, final MainActivity activity, final NavigationDrawerFragment menuGauche, final int idListe, final TypeListe typeListe) {
		alertDialog = new MaterialDialog.Builder(activity)
				.title(R.string.newList)
				.customView(R.layout.popup_liste_material, true)
				.positiveText(R.string.save)
				.negativeText(R.string.supprimer)
				//Enregistrer
				.onPositive(new MaterialDialog.SingleButtonCallback() {
					@Override
					public void onClick(MaterialDialog dialog, DialogAction which) {
						String nom = etL.getText().toString();
						int id = idListe;
						if (type == TypePopup.OLD) {
							liste.setNom(nom);
							liste.setNbPersonne(Integer.parseInt(editTextNbp.getText().toString()));
							activity.modifierListe(position, liste);
							menuGauche.rename(position, liste);
							if (nbpDebut != Integer.parseInt(editTextNbp.getText().toString())) {
								new MaterialDialog.Builder(activity)
										.content(R.string.modifierQunatite)
										.positiveText(R.string.maj)
										.negativeText(R.string.conserver)
										.onPositive(new MaterialDialog.SingleButtonCallback() {
											@Override
											public void onClick(MaterialDialog dialog, DialogAction which) {
												maj(Float.parseFloat(editTextNbp.getText().toString()),
														(float) nbpDebut, idListe);
											}
										})
										.show();
								activity.changerDeListeDeCourses(false, id, typeListe);
							}
						} else if (type == TypePopup.NEW) {
							int nbp = Integer.parseInt(editTextNbp.getText().toString());
							activity.ajouterNouvelleListe(nom, typeListe,
									checkBox.isChecked(),nbp);
							//liste.setNbPersonne(nbp);
							//liste.setNbPersonnsDemande(liste.getNbPersonnes());
							//menuGauche.add(nom, typeListe, liste);
							//id = liste.getId();
						}
						//activity.changerDeListeDeCourses(position, false, id, typeListe);
					}
				})
				//Supprimer
				.onNegative(new MaterialDialog.SingleButtonCallback() {
					@Override
					public void onClick(MaterialDialog dialog, DialogAction which) {
						if(type == TypePopup.OLD) {
							boolean normale = false;
							activity.supprimerListe(position, idListe, typeListe);
							menuGauche.remove(idListe);
							//Verification qu'il reste encore des listes normales
							for(int i=0; i<listes.size();i++){
								if(listes.get(i).getTypeListe()==TypeListe.LISTE_NORMALE)
									normale=true;
							}
							//Cas de la supression de la premiere liste
							//Avec une seul liste presente
							if (listes.size() == 0 || !normale) {
								String nom = activity.getString(R.string.defaut);
								activity.ajouterNouvelleListe(nom, TypeListe.LISTE_NORMALE, false,1);
								//menuGauche.add(nom, typeListe, liste);
							}

							activity.changerDeListeDeCourses(true, idListe, typeListe);
						}
					}
				})
				.build();

		final View alertDialogView = alertDialog.getCustomView(); //factory.inflate(R.layout.popup_liste_material, null);
		//alertDialog = new AlertDialog.Builder(activity,AlertDialog.THEME_HOLO_LIGHT).create();
		///alertDialog.setView(alertDialogView);

		etL             = (EditText)alertDialogView.findViewById(R.id.editTextNomListe);
		checkBox        = (CheckBox)alertDialogView.findViewById(R.id.checkBoxlpr);
		layoutPersonnes = (LinearLayout)alertDialogView.findViewById(R.id.linearLayoutListeRecette);
		editTextNbp     = (EditText)alertDialogView.findViewById(R.id.editTextNbPersonneListe);

		if(typeListe == TypeListe.LISTE_RECETTE) {
            layoutPersonnes.setVisibility(View.VISIBLE);
            checkBox.setVisibility(View.GONE);
        }
		else {
            layoutPersonnes.setVisibility(View.GONE);
        }

		if(type == TypePopup.OLD){
            //On affiche ce qui est charge du fichier xml
            for(int i=0; i<listes.size();i++){
                if(listes.get(i).getId() == idListe)
                    liste = listes.get(i);
            }

            String name = liste.getNom();
            alertDialog.setTitle(name);
            if(!name.equals(""))
                etL.setText(name);

            checkBox.setVisibility(View.GONE);
            nbpDebut = liste.getNbPersonnes();
            if(typeListe==TypeListe.LISTE_RECETTE)
                editTextNbp.setText(Integer.toString(nbpDebut));
            //alertDialog.negativeText(R.string.supprimer);
        }
        else if(type == TypePopup.NEW){
            //mTitle.setText(activity.getString(R.string.newList));
            //fabSup.setVisibility(View.GONE);
            alertDialog.getActionButton(DialogAction.NEGATIVE).setVisibility(View.GONE);
        }

		alertDialog.show();
	}

	private void maj(float ancien, float nouveau, int idListe) {
		float mutiplicateur = nouveau/ancien;
		ArrayList<GSItem> listeItems = new ArrayList<>();
		GSListe liste = activity.getListeWithId(idListe);

		//On charge les la liste des liste de courses

		for (int i = 0; i < liste.getListCategories().size(); i++) {
			listeItems.addAll(liste.getCategorie(i).getListItems());
		}
		float q;
		GSItem item;
		for (int i = 0; i < listeItems.size(); i++) {
			item = listeItems.get(i);
			q = item.getQuantite();
			if(q>0){
				item.setQuantite(q * mutiplicateur);
				activity.modifierItem(item.getIdCategorie(), item, true);
			}
		}
	}

	private void listeJointe(final TypePopup type, final int position, final MainActivity activity,
							 final NavigationDrawerFragment menuGauche, final int idListe,
							 final TypeListe typeListe){


		LinkedList<GSListe> toutesLesListes = (LinkedList<GSListe>) activity.getAllListes().clone();
		ArrayList<GSListe> listes = new ArrayList<>();
		for (GSListe liste:toutesLesListes) {
			if(liste.getTypeListe()!=TypeListe.LISTE_COMPOSE){
				listes.add(liste);
			}
			if (liste.getId() == idListe && type == TypePopup.OLD) {
				this.liste = liste;
			}
		}

		ArrayList<Integer> list = new ArrayList<>();

		if(type == TypePopup.OLD) {
			ArrayList<Integer> idList = activity.getIdFromListeCompose(idListe);

			for (int i = 0; i < listes.size(); i++) {
				if(idList.contains(listes.get(i).getId())) {
					list.add(i);
				}
			}
		}
			//TODO charger les listes selectionner et les cocher

		final ListCoursePresentesAdapter adapter = new ListCoursePresentesAdapter(activity, listes, list);


		alertDialog = new MaterialDialog.Builder(activity)
				.title((type == TypePopup.OLD)?liste.getNom():activity.getString(R.string.newListFusione))
				.customView(R.layout.popup_liste_fusionee_material, true)
				.positiveText(R.string.save)
				.negativeText(type == TypePopup.OLD?R.string.supprimer: R.string.annuler)
				//Enregistrer
				.onPositive(new MaterialDialog.SingleButtonCallback() {
					@Override
					public void onClick(MaterialDialog dialog, DialogAction which) {
						String nom = etL.getText().toString();
//						int id = idListe;
						if (type == TypePopup.OLD) {
							liste.setNom(nom);
							activity.modifierListeComposee(position, liste, adapter.getListeChecked());

							menuGauche.rename(position, liste);
//							}
						} else if (type == TypePopup.NEW) {
//							int nbp = Integer.parseInt(editTextNbp.getText().toString());
							ListeCompose liste = activity.ajouterNouvelleListeCompose(nom, adapter.getListeChecked());
							menuGauche.add(liste);
							int id = liste.getId();

							System.out.println();
						}
						//activity.changerDeListeDeCourses(position, false, id);
					}
				})
				//Supprimer
				.onNegative(new MaterialDialog.SingleButtonCallback() {
					@Override
					public void onClick(MaterialDialog dialog, DialogAction which) {
						if(type == TypePopup.OLD) {//TODO gerer
//							//On supprime juste la liste fusionnee
//							boolean normale = false;
//							activity.supprimerListe(position, idListe);
//							menuGauche.remove(position);
//							//Verification qu'il reste encore des listes normales
//							for(int i=0; i<listes.size();i++){
//								if(listes.get(i).getTypeListe()==TypeListe.LISTE_NORMALE)
//									normale=true;
//							}
//							//Cas de la supression de la premiere liste
//							//Avec une seul liste presente
//							if (listes.size() == 0 || !normale) {
//								String nom = activity.getString(R.string.defaut);
//								GSListe liste = activity.ajouterNouvelleListe(nom, TypeListe.LISTE_NORMALE, false,1);
//								menuGauche.add(nom, typeListe, liste);
//							}
//
							activity.supprimerListe(position, idListe, typeListe);
							activity.changerDeListeDeCourses(true, idListe, typeListe);
						}
					}
				})
				.build();

		final View alertDialogView = alertDialog.getCustomView();
		ListView lv = (ListView) alertDialogView.findViewById(R.id.listes_existantes);

		lv.setAdapter(adapter);

		etL             = (EditText)alertDialogView.findViewById(R.id.editTextNomListe);

		if(type == TypePopup.OLD){
			etL.setText(liste.getNom());
		}

		alertDialog.show();
	}

}
