package services;

import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.view.Display;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.fourrier.goodshopping.MainActivity;
import com.fourrier.goodshopping.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import database.GSItem;

/**
 * Created by Alexis on 07/05/16.
 */
public class GestionImage {
    private MainActivity mActivity;
    private Dialog alertDialog;
    private ImageView imageView;
    private File path;
    private GSItem mItem;
    private ProgressBar progressBar;
    private LinearLayout top;
    private LinearLayout bottom;
    private ImageButton back;
    private ImageButton delete;

    public GestionImage(final MainActivity activity, final GSItem item){
        mActivity = activity;

        ContextWrapper cw = new ContextWrapper(mActivity.getApplicationContext());
        path = cw.getDir("imageDir", Context.MODE_PRIVATE);
        mItem = item;



        alertDialog=new Dialog(activity,android.R.style.Theme_Black_NoTitleBar);
        alertDialog.setContentView(R.layout.popup_picture);
        alertDialog.setTitle(item.getNom());

        imageView = (ImageView)alertDialog.findViewById(R.id.imageViewPicture);
        top = (LinearLayout) alertDialog.findViewById(R.id.picture_layout_top);
        bottom = (LinearLayout) alertDialog.findViewById(R.id.picture_layout_bottom);
        back = (ImageButton) alertDialog.findViewById(R.id.imageButtonPictureBack);
        delete = (ImageButton) alertDialog.findViewById(R.id.imageButtonPictureDelete);
        progressBar = (ProgressBar)alertDialog.findViewById(R.id.progressBarPicture);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(top.getVisibility()==View.GONE){
                    top.setVisibility(View.VISIBLE);
                    bottom.setVisibility(View.VISIBLE);
                    back.setVisibility(View.VISIBLE);
                    delete.setVisibility(View.VISIBLE);
                }
                else {
                    top.setVisibility(View.GONE);
                    bottom.setVisibility(View.GONE);
                    back.setVisibility(View.GONE);
                    delete.setVisibility(View.GONE);
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.hide();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(activity)
                        .title(R.string.supprimerPhoto)
                        .positiveText(R.string.oui)
                        .negativeText(R.string.non)
                        //Enregistrer
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                removeFile();
                                alertDialog.hide();
                            }
                        })
                        .build()
                        .show();
            }
        });

        new LoadPicture().execute();
//        loadImageFromStorage();

        alertDialog.show();

    }

    private class LoadPicture extends AsyncTask<Void, Void, Void> {
        private Bitmap bitmap;

        protected Void doInBackground(Void... idListe) {
            bitmap = ImageFile.loadImageFromStorage(path, mItem.getImage());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            imageView.setImageBitmap(bitmap);
            top.setVisibility(View.VISIBLE);
            bottom.setVisibility(View.VISIBLE);
            back.setVisibility(View.VISIBLE);
            delete.setVisibility(View.VISIBLE);
        }

    }

    private Bitmap loadImageFromStorage() {
        Bitmap b = ImageFile.loadImageFromStorage(path, mItem.getImage());

        Display display = mActivity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        double ratio = (float)b.getHeight()/(float)b.getWidth();

        int largeur = width-(width/4);
        double d = largeur*ratio;
        int hauteur = (int)d;
        /*if(b.getWidth()>b.getHeight()){
            largeur = 1500;
            hauteur = 900;
        }*/

        try {
            imageView.setImageBitmap(b);//Bitmap.createScaledBitmap(b, largeur, hauteur, false));
        }
        catch (Exception e){
        }
        return b;
    }

    private void removeFile(){
        if(ImageFile.removeFile(path, mItem.getImage())){
            mItem.setImage("");
            mActivity.modifierItem(mItem.getIdCategorie(), mItem, true);
        }
    }

    private Bitmap decodeFileFile(File path, String name) {
        try {
            File f=new File(path, name);
            // decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            // Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 10;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;

            while (true) {
                if (width_tmp * 2 > REQUIRED_SIZE
                        || height_tmp * 2 > REQUIRED_SIZE)
                    break;
                width_tmp *= 2;
                height_tmp *= 2;
                scale /= 2;
            }
            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

}
