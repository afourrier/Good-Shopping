package services;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.fourrier.goodshopping.MainActivity;
import com.fourrier.goodshopping.R;

import java.util.HashMap;
import java.util.LinkedList;

import database.GSCategory;
import database.ETypeDatabaseItem;
import database.GSItem;

/**
 * Created by Alexis on 06/05/16.
 */
public class GestionQuantite {
    private String m_categorie;
    private int m_categorieId;
    private String m_color;
    private TextView changerCate;
    private EditText quantite;
    private EditText longueur;
    private Spinner spinLong;
    private EditText largeur;
    private Spinner spinLarg;
    private EditText hauteur;
    private Spinner spinHaut;
    private Spinner spin;
    private TextView addPicture;
    private TextView duplicate;
    private ImageView imagePicture;
    private EditText name;
    private Switch mSwitch;
    private LinearLayout layoutLongueur;
    private LinearLayout layoutChangeCate;
    private MaterialDialog alertDialog;
    private boolean quantitee;
    private MainActivity mActivity;
    private LinkedList<GSCategory> mCategories;
    private LinkedList<GSItem> mListeDeCoursesCategorie;
    private GSItem item;

    public GestionQuantite(final MainActivity activity, final GSItem item, final TypePopup typePopup){
        mActivity = activity;
        mCategories = mActivity.getCategoriesListe();
        mListeDeCoursesCategorie = mActivity.getListeDeCoursesCategorie();

        alertDialog = new MaterialDialog.Builder(mActivity)
                //.title(R.string.quantite)
                .customView(R.layout.popup_quantite_material, true)
                .positiveText(R.string.save)
                .negativeText(R.string.supprimer)
                //.neutralText(R.string.dupliquer)
                //Enregistrer
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        if (typePopup == TypePopup.NEW) {
                            mActivity.setQuantite(gererLeNull((String) quantite.getText().toString()));
                            mActivity.setType(spin.getSelectedItemPosition());
                            if (!quantitee) {//Si c'est une longueur
                                mActivity.setHashMapLongueur(remplirMapLongueur());
                            }
                        } else {
//                            GSItem item = mListeDeCoursesCategorie.get(position);
                            int idCategorieDepart = item.getIdCategorie();
                            item.setNom(name.getText().toString());
                            item.setQuantite(gererLeNull(quantite.getText().toString()));
                            item.setType(spin.getSelectedItemPosition());
                            item.setNomCategorie(m_categorie);
                            item.setColor(m_color);
                            item.setIdCategorie(m_categorieId);

                            //Ajout/Suppression de longueur
                            if (quantitee == true) {//On supprime la ligne
                                item.setLongueur(0);
                                item.setLargeur(0);
                                item.setHauteur(0);
                                item.setLongueurType(0);
                                item.setLargeurType(0);
                                item.setHauteurType(0);
                            } else if (!quantitee) {//Si c'est une longueur
                                HashMap mapLong = remplirMapLongueur();
                                if (mapLong.containsKey("long"))
                                    item.setLongueur((float) mapLong.get("long"));
                                if (mapLong.containsKey("longType"))
                                    item.setLongueurType((int) mapLong.get("longType"));

                                if (mapLong.containsKey("larg"))
                                    item.setLargeur((float) mapLong.get("larg"));
                                if (mapLong.containsKey("largType"))
                                    item.setLargeurType((int) mapLong.get("largType"));

                                if (mapLong.containsKey("haut"))
                                    item.setHauteur((float) mapLong.get("haut"));
                                if (mapLong.containsKey("hautType"))
                                    item.setHauteurType((int) mapLong.get("hautType"));
                            }
                            mActivity.modifierItem(idCategorieDepart, item, true);
                        }
                    }
                })
                        //Supprimer
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        if (typePopup == TypePopup.OLD) {//Modifier
                            mActivity.supprimerItem(item, 0);
                            mListeDeCoursesCategorie.remove(item);
                            mActivity.gererListView();
                            alertDialog.hide();
                        }
                    }
                })
                //Dupliquer
                /*.onNeutral(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        mActivity.dupliquerItem(item);
                    }
                })*/
                .build();

        final View alertDialogView = alertDialog.getCustomView();//factory.inflate(R.layout.popup_quantite_material, null);
        //alertDialog = new AlertDialog.Builder(mActivity,AlertDialog.THEME_HOLO_LIGHT).create();
        //alertDialog.setView(alertDialogView);

        name                 = (EditText)alertDialogView.findViewById(R.id.editTextNomItem);
        quantite             = (EditText)alertDialogView.findViewById(R.id.editTextQuantite);
        spin                 = (Spinner)alertDialogView.findViewById(R.id.spinnerType);
        longueur             = (EditText)alertDialogView.findViewById(R.id.editTextLongueur);
        spinLong             = (Spinner)alertDialogView.findViewById(R.id.spinnerLongueur);
        largeur              = (EditText)alertDialogView.findViewById(R.id.editTextLargeur);
        spinLarg             = (Spinner)alertDialogView.findViewById(R.id.spinnerLargeur);
        hauteur              = (EditText)alertDialogView.findViewById(R.id.editTextHauteur);
        spinHaut             = (Spinner)alertDialogView.findViewById(R.id.spinnerHauteur);
        changerCate          = (TextView)alertDialogView.findViewById(R.id.textViewChangerCategorie);
        layoutLongueur       = (LinearLayout)alertDialogView.findViewById(R.id.layoutLongueur);
        addPicture           = (TextView)alertDialogView.findViewById(R.id.textViewAddPicture);
        duplicate            = (TextView)alertDialogView.findViewById(R.id.textViewduplicate);
        imagePicture         = (ImageView)alertDialogView.findViewById(R.id.image_picture_add);
        layoutChangeCate     = (LinearLayout)alertDialogView.findViewById(R.id.linearLayoutChangerCate);
        mSwitch              = (Switch)alertDialogView.findViewById(R.id.switchDimension);

        m_categorie = "All";
        m_color     = mActivity.getString(R.string.blanc);

        mSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mSwitch.isChecked())
                    setCategorieSelected(2);
                else
                    setCategorieSelected(1);
            }
        });

//        List<String> lines = Arrays.asList(mActivity.getResources().getStringArray(R.array.liste_type));
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,android.R.layout.simple_spinner_dropdown_item, lines);
//
//        List<String> linesL = Arrays.asList(mActivity.getResources().getStringArray(R.array.liste_longueurs));
//        ArrayAdapter<String> adapterLongueur = new ArrayAdapter<String>(mActivity,android.R.layout.simple_list_item_1, linesL);
//
//          spin.setAdapter(adapter);
//        spinLong.setAdapter(adapterLongueur);
//        spinLarg.setAdapter(adapterLongueur);
//        spinHaut.setAdapter(adapterLongueur);
        int pos = 0;

        if(mCategories.size()<=1)
            changerCate.setVisibility(View.GONE);

        if(typePopup == TypePopup.NEW){
            name.setVisibility(View.GONE);
            changerCate.setVisibility(View.GONE);
            quantite.selectAll();
            addPicture.setVisibility(View.GONE);
            duplicate.setVisibility(View.GONE);
            ((InputMethodManager)mActivity.getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            setCategorieSelected(1);
            layoutChangeCate.setVisibility(View.GONE);
            alertDialog.getActionButton(DialogAction.NEGATIVE).setVisibility(View.GONE);
            alertDialog.getActionButton(DialogAction.NEUTRAL).setVisibility(View.GONE);


        }
        else{
//            item = mListeDeCoursesCategorie.get(position);
            m_categorieId = item.getIdCategorie();
            m_categorie = item.getNomCategorie();
            m_color     = item.getColor();
            //On determine si il sagit d'une quantitee ou d'une longueur
            if((item.getLargeur()+item.getLongueur()+item.getHauteur())>0) {
                setCategorieSelected(2);
                longueur.setText(gestionDeLaVirgule(item.getLongueur()));
                largeur.setText(gestionDeLaVirgule(item.getLargeur()));
                hauteur.setText(gestionDeLaVirgule(item.getHauteur()));

                spinLong.setSelection(item.getLongType());
                spinLarg.setSelection(item.getLargType());
                spinHaut.setSelection(item.getHautType());
                mSwitch.setChecked(true);
            }
            else
                setCategorieSelected(1);

            //Si il y a une photo on change l'image du fab
            if(!item.getImage().equals("")) {
                setImgSaved();
            }
            //Si il y a une seule categorie on affiche pas le layout
            if(mActivity.getCategoriesListe().size()==1)
                layoutChangeCate.setVisibility(View.GONE);

            mActivity.setItemSelected(item);
            final String[] listeItems = new String[]{mActivity.getString(R.string.photo_appareil),
                    mActivity.getString(R.string.photo_importer)};
            addPicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(item.getImage().equals("")) {
                        new MaterialDialog.Builder(mActivity)
                                .title(R.string.photo_ajouter)
                                .items(R.array.liste_choix_photo)
                                .itemsCallback(new MaterialDialog.ListCallback() {
                                    @Override
                                    public void onSelection(MaterialDialog dialog, View view, int which,
                                                            CharSequence text) {
                                        if(text.equals(listeItems[0])){
                                            mActivity.autorisationPrendrePhoto ();
                                        }
                                        else if(text.equals(listeItems[1])){
                                            mActivity.importerPhoto();
                                        }
                                    }
                                })
                                .show();
                    }
                    else {
                        new GestionImage(mActivity, item);
                    }
                }
            });

            duplicate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mActivity.dupliquerItem(item);
                    Toast.makeText(mActivity, item.getNom()+" "+mActivity.getString(R.string.duplique), Toast.LENGTH_SHORT).show();
                }
            });

            name.setText(item.getNom());

            if(mActivity.getAllListes().size()>0)
                pos = item.getType();

            quantite.setText(gestionDeLaVirgule(item.getQuantite()));

        }

        spin.setSelection(pos);

        //changerCate.setBackgroundColor(Color.parseColor(m_color));
        changerCate.setTextColor(Color.parseColor(m_color));
        changerCate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new GestionListView(mActivity, false, m_categorie, GestionQuantite.this, ETypeDatabaseItem.CATEGORY);
            }
        });

        alertDialog.show();
    }

    public void setNouvelleCategorie(GSCategory category){
        m_categorie = category.getNom();
        m_categorieId = category.getId();
        m_color = category.getCouleur();
        changerCate.setTextColor(Color.parseColor(m_color));
    }

    public void setCategorieSelected(int type){
        if(type==1){//Quantite
            alertDialog.setTitle(mActivity.getString(R.string.quantite));
            layoutLongueur.setVisibility(View.GONE);
            quantitee = true;
            spin.setVisibility(View.VISIBLE);
        }
        else if(type==2){//Longueur
            if(quantite.getText().toString().equals("") ||
                    quantite.getText().toString().equals("0")) {
                quantite.setText("1");
            }
            alertDialog.setTitle(mActivity.getString(R.string.dimension));
            layoutLongueur.setVisibility(View.VISIBLE);
            quantitee = false;
            spin.setVisibility(View.INVISIBLE);

        }
    }

    public float gererLeNull(String str){
        if(str.equals(""))
            str = "0";
        return Float.parseFloat(str);
    }

    public HashMap remplirMapLongueur(){
        HashMap mapLong = new HashMap();
        mapLong.put("long", gererLeNull((String)longueur.getText().toString()));
        mapLong.put("longType", spinLong.getSelectedItemPosition());

        mapLong.put("larg", gererLeNull((String)largeur.getText().toString()));
        mapLong.put("largType", spinLarg.getSelectedItemPosition());

        mapLong.put("haut", gererLeNull((String)hauteur.getText().toString()));
        mapLong.put("hautType", spinHaut.getSelectedItemPosition());
        return mapLong;
    }

    public String gestionDeLaVirgule(float f){
        String res = Float.toString(f);
        //String[] split = res.split(".");
        if(res.substring(res.length()-2,res.length()).equals(".0"))
            res = res.substring(0, res.length()-2);
        return res;
    }

    public void setImgSaved(){
        imagePicture.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_crop_original_black));
        addPicture.setText(R.string.see_picture);
    }
}
