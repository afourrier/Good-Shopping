package services;

import android.app.Activity;

import com.fourrier.goodshopping.IActivity;
import com.fourrier.goodshopping.R;

import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import database.GSCategory;
import database.GSItem;
import database.GSListe;
import database.TypeListe;

/**
 * Created by Alexis on 10/02/16.
 */
public class Useful {

    public static String affichageZero(float lon, int type, float sum, String[] listeLongueur){
        int i = (int)lon;
        String res = "";

        if(i!=0){
            if(lon>i)
                res= Float.toString(lon);
            else
                res= Integer.toString(i);
        }

        if(!res.equals("")){
            res+=listeLongueur[type];
            if(sum>0)res+=" x ";
        }
        return res;
    }

    public static String affichageZero(float quantite, float multiplicateur, boolean longueur){
        String res = "";
        if(longueur)
            res+=" : ";
        else
            res+=" (";

        int i = ((int)(multiplicateur * quantite));
        if((multiplicateur * quantite) > i)
            res+= Float.toString(multiplicateur * quantite);
        else
            res+=Integer.toString(i);

        if(longueur)
            res+=" (";

        return res;
    }

    public static String affichageQuantite(GSItem item, String[] listeLongueur,
                                           String[] listeTypeQuantite, GSListe listeSelected){
        String ligne = "";
        float quantite = item.getQuantite();
        float largeur = item.getLargeur();
        float longueur = item.getLongueur();
        float hauteur = item.getHauteur();
        float multiplicateur = 1;

        //Calcul de la quantite pour les listes recette
        if(listeSelected.getTypeListe()==TypeListe.LISTE_RECETTE){
            multiplicateur = (float)listeSelected.getNbPersonnesDemande()/(float)listeSelected.getNbPersonnes();
        }


        if((largeur+longueur+hauteur)>0){//map.containsKey("longueur")){
            ligne += affichageZero(quantite, multiplicateur, true);//" : " + Float.toString(multiplicateur * quantite) + " (";
            ligne += Useful.affichageZero(longueur, item.getLongType(),
                    largeur + hauteur, listeLongueur);
            ligne += Useful.affichageZero(largeur, item.getLargType(),
                    hauteur, listeLongueur);
            ligne += Useful.affichageZero(hauteur, item.getHautType(),
                    0, listeLongueur);
            ligne += ")";
        }
        else{
            if(quantite>0){
                ligne+=affichageZero(quantite, multiplicateur, false);
                /*int i = ((int)(multiplicateur * quantite));
                if((multiplicateur * quantite) > i)
                    ligne+= " ("+Float.toString(multiplicateur * quantite);
                else
                    ligne+=" ("+Integer.toString(i);*/
                //Type enregistre
                if(item.getType()>=0)/*!map.get("type").equals("null")*/{
                    ligne+= listeTypeQuantite[item.getType()]+")";
                }
            }
        }
        return ligne;
    }

    private static LinkedList<GSItem> remplirListe(Activity activity, GSCategory category, String[] str) {
        LinkedList<GSItem> res = new LinkedList<>();
        GSItem item;
        for (int i = 0; i < str.length; i++) {
            item = new GSItem(
                    0,
                    str[i],
                    0,
                    false,
                    0,
                    0,
                    i,
                    category.getFirebaseId()
            );
            item.setColor(category.getCouleur());
            item.setNomCategorie(category.getNom());
            item.setIdCategorie(category.getId());
            if(((IActivity)activity).isUserConnected()){
                item.setFirebaseId(((IActivity)activity).getNewFirebaseID());
            }
            res.add(item);
        }
        return res;
    }

    public static GSListe nouvelleListe(Activity activity, String nom){
        LinkedList<GSCategory> ll = new LinkedList<>();
        int idListe = 0;

        String fId = "";
        if(((IActivity)activity).isUserConnected()){
            fId = ((IActivity)activity).getNewFirebaseID();
        }

        ll.add(creerCategorie(activity,idListe, R.string.viandeEtPoisson,R.string.frc3,R.array.liste_viande_poisson,0));
        ll.add(creerCategorie(activity,idListe, R.string.fruitsEtLegumes,R.string.vc3,R.array.liste_fruits,1));
        ll.add(creerCategorie(activity,idListe, R.string.cremerie,R.string.gc2,R.array.liste_cremerie,2));
        ll.add(creerCategorie(activity,idListe, R.string.gateaux,R.string.vic3,R.array.liste_gateaux,3));
        ll.add(creerCategorie(activity,idListe, R.string.surgeles,R.string.cc3,R.array.liste_surgeles,4));
        ll.add(creerCategorie(activity,idListe, R.string.boissons,R.string.bc3,R.array.liste_boissons,5));
        ll.add(creerCategorie(activity,idListe, R.string.hygiene,R.string.mc3,R.array.liste_hygiene,6));
        ll.add(creerCategorie(activity,idListe, R.string.maison,R.string.oc3,R.array.liste_maison,7));
        ll.add(creerCategorie(activity,idListe, R.string.autre,R.string.rc3,R.array.liste_epicerie,8));

        GSListe liste = new GSListe(idListe, nom, TypeListe.LISTE_NORMALE, 1,1,fId,0);
        if(((IActivity)activity).isUserConnected()){
            liste.setFirebaseId(((IActivity)activity).getNewFirebaseID());
        }
        liste.setListeCategorie(ll);

        return liste;
    }

    private static GSCategory creerCategorie(Activity activity, int idListe, int nom,
                                             int couleur, int listeNomItems, int pos ){
        String fId = "";
        if(((IActivity)activity).isUserConnected()){
            fId = ((IActivity)activity).getNewFirebaseID();
        }
        GSCategory category = new GSCategory(0,
                activity.getString(nom),
                activity.getString(couleur),
                idListe, pos, fId);
        //Creation de la liste d'item
        List<String> lines = Arrays.asList(activity.getResources().getStringArray(listeNomItems));
        String[] liste = lines.toArray(new String[]{});
        //On remplie la liste d'itmes
        category.setListItems(remplirListe(activity, category, liste));
        if(((IActivity)activity).isUserConnected()){
            category.setFirebaseId(((IActivity)activity).getNewFirebaseID());
        }
        return category;
    }

    public static String[] getAllItems(Activity activity){
        LinkedList<String> list = remplirListeDeCourses(activity);
        String[] res = new String[list.size()];
        for(int i=0; i<list.size();i++){
            res[i] = list.get(i);
        }

        return res;
    }

    private static LinkedList remplirListeDeCourses(Activity activity){
        LinkedList ll = new LinkedList();

        List<String> lines = Arrays.asList(activity.getResources().getStringArray(R.array.liste_viande_poisson));
        String[] liste = lines.toArray(new String[10]);
        ll.addAll(Arrays.asList(liste));

        lines = Arrays.asList(activity.getResources().getStringArray(R.array.liste_fruits));
        liste = lines.toArray(new String[11]);
        ll.addAll(Arrays.asList(liste));

        lines = Arrays.asList(activity.getResources().getStringArray(R.array.liste_cremerie));
        liste = lines.toArray(new String[6]);
        ll.addAll(Arrays.asList(liste));

        lines = Arrays.asList(activity.getResources().getStringArray(R.array.liste_gateaux));
        liste = lines.toArray(new String[15]);
        ll.addAll(Arrays.asList(liste));

        lines = Arrays.asList(activity.getResources().getStringArray(R.array.liste_surgeles));
        liste = lines.toArray(new String[7]);
        ll.addAll(Arrays.asList(liste));

        lines = Arrays.asList(activity.getResources().getStringArray(R.array.liste_boissons));
        liste = lines.toArray(new String[9]);
        ll.addAll(Arrays.asList(liste));

        lines = Arrays.asList(activity.getResources().getStringArray(R.array.liste_hygiene));
        liste = lines.toArray(new String[19]);
        ll.addAll(Arrays.asList(liste));

        lines = Arrays.asList(activity.getResources().getStringArray(R.array.liste_maison));
        liste = lines.toArray(new String[28]);
        ll.addAll(Arrays.asList(liste));

        lines = Arrays.asList(activity.getResources().getStringArray(R.array.liste_epicerie));
        liste = lines.toArray(new String[32]);
        ll.addAll(Arrays.asList(liste));

        return ll;
    }

    public static String today_toString(){
        String res = "";
        Calendar c = Calendar.getInstance();
        res+=Integer.toString(c.get(Calendar.DATE));
        res+="-";
        res+=Integer.toString(c.get(Calendar.MONTH));
        res+="-";
        res+=Integer.toString(c.get(Calendar.YEAR));
        return res;
    }

    public static String calendarToString(Calendar c){
        String res = "";
        res+=Integer.toString(c.get(Calendar.DATE));
        res+="-";
        res+=Integer.toString(c.get(Calendar.MONTH));
        res+="-";
        res+=Integer.toString(c.get(Calendar.YEAR));
        return res;
    }

    public static Calendar stringToCalendar(String str){
        Calendar c = Calendar.getInstance();
        String[] tab = str.split("-");
        c.set(Integer.parseInt(tab[2]),Integer.parseInt(tab[1]),Integer.parseInt(tab[0]));
        return c;
    }

}
