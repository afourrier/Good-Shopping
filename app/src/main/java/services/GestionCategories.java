package services;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.fourrier.goodshopping.MainActivity;
import com.fourrier.goodshopping.R;

import java.util.HashMap;

import database.GSCategory;
import database.ETypeDatabaseItem;

public class GestionCategories implements IColorSelector{
	private MainActivity mActivity;
	private MaterialDialog alertDialog;
	private AutoCompleteTextView nom;
	private ImageButton color;
	private ImageView palette;
	private String couleur;
	private String categorieDepart;
	private TextView tvColor;
	private HashMap<CharSequence, CharSequence> defaultCategorieColor;

	public GestionCategories(final TypePopup type, final int position,
							 final MainActivity activity, final GSCategory category){

		mActivity = activity;
		alertDialog = new MaterialDialog.Builder(activity)
				.title(R.string.newCategories)
				.customView(R.layout.popup_categories_material, true)
				.positiveText(R.string.save)
				.negativeText(R.string.supprimer)
						//Enregistrer
				.onPositive(new MaterialDialog.SingleButtonCallback() {
					@Override
					public void onClick(MaterialDialog dialog, DialogAction which) {
						String name = nom.getText().toString();
						if (type == TypePopup.NEW) {
							activity.ajouterCategorie(name, couleur);
						} else {
							activity.modifierCategorie(name, couleur);
						}
						alertDialog.hide();
					}
				})
						//Supprimer
				.onNegative(new MaterialDialog.SingleButtonCallback() {
					@Override
					public void onClick(MaterialDialog dialog, DialogAction which) {
						if(type == TypePopup.OLD){
							supprimerCategorie(category);
							alertDialog.hide();
						}
					}
				})
				.build();


		final View alertDialogView = alertDialog.getCustomView();
		
		categorieDepart = "";
		
	    nom     = (AutoCompleteTextView)alertDialogView.findViewById(R.id.editTextNomCategorie);
	    color   = (ImageButton)alertDialogView.findViewById(R.id.buttonChangeColorCategorie);
		palette   = (ImageView) alertDialogView.findViewById(R.id.palette_category);
		tvColor = (TextView)alertDialogView.findViewById(R.id.textViewColor);

		CharSequence[] cate = activity.getResources().getTextArray(R.array.liste_categorie);
		CharSequence[] cateColor =  activity.getResources().getTextArray(R.array.liste_categorie_color);

		ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>
				(activity,android.R.layout.simple_list_item_1, cate);

		defaultCategorieColor = new HashMap<>();
		for (int i = 0; i < cate.length; i++) {
			defaultCategorieColor.put(cate[i], cateColor[i]);
		}

		nom.setAdapter(adapter);

		nom.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if(defaultCategorieColor.containsKey(s.toString())){
					setColorSelected((String) defaultCategorieColor.get(s.toString()));
				}
			}
		});

		couleur = activity.getString(R.string.blanc);

	    if(type == TypePopup.OLD){
	    	categorieDepart = category.getNom();
			couleur = category.getCouleur();
	    	nom.setText(categorieDepart);
			alertDialog.setTitle(mActivity.getString(R.string.category));
	    }
		else{
			alertDialog.getActionButton(DialogAction.NEGATIVE).setVisibility(View.GONE);
		}

	    color.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mActivity.lancerPopupCouleur(GestionCategories.this, couleur);
			}

		});
		tvColor.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				color.performClick();
			}

		});
		palette.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				color.performClick();
			}

		});

		setColorSelected(couleur);
	    


		alertDialog.show();

	}
	
	private void reorganierCategories(){
		new GestionListView(mActivity,true, categorieDepart, null, ETypeDatabaseItem.CATEGORY);
	}
	
	private void supprimerCategorie(final GSCategory category){

		new AlertDialogWrapper.Builder(mActivity)
				.setMessage(R.string.supprimerCate)
				.setNegativeButton(R.string.non, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				})
				.setPositiveButton(R.string.oui, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						mActivity.supprimerCategorie(category);
						mActivity.gererListView();
						dialog.dismiss();
					}
				})
				.show();
	}

	@Override
	public void setColorSelected(String color) {
		couleur = color;
		GradientDrawable bgShape = (GradientDrawable)this.color.getBackground().getCurrent();
		bgShape.setColor(Color.parseColor(color));
	}
}
