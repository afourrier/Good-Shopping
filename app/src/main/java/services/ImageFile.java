package services;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by Alexis on 08/05/16.
 */
public class ImageFile {

    public static boolean saveToInternalStorage(Activity activity, Bitmap bitmapImage, String name){
        boolean res = false;
        ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,name);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
            res = true;
        } catch (Exception e) {
            e.printStackTrace();
            res = false;
        }

        return res;
    }

    public static Bitmap loadImageFromStorage(File path, String name) {

        Bitmap b = null;

        try {
            File f=new File(path, name);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;

            b = BitmapFactory.decodeStream(new FileInputStream(f), null, options);

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return b;
    }

    public static boolean removeFile(File path, String name){
        boolean res = false;
        try{
            File f=new File(path, name);
            res = f.delete();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return  res;
    }


}
