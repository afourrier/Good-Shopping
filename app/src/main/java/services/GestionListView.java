package services;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ListView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.fourrier.goodshopping.MainActivity;
import com.fourrier.goodshopping.R;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.nhaarman.listviewanimations.itemmanipulation.dragdrop.OnItemMovedListener;
import com.nhaarman.listviewanimations.itemmanipulation.dragdrop.TouchViewDraggableManager;

import java.util.ArrayList;
import java.util.LinkedList;

import adapters.MovingAdapter;
import database.ETypeDatabaseItem;
import database.GSCategory;
import database.GSListe;
import database.IDatabaseItems;

public class GestionListView implements OnItemMovedListener {
	private DynamicListView mListView;
	private MaterialDialog alertDialog;
	private MovingAdapter adapter;
	private LinkedList mListe;
	private String mCategorieSelected;
	private Object mObject;
	private int mDebut;
	private int mFin;

	public GestionListView(final MainActivity activity, final boolean move, String categorie,
						   final Object obj, final ETypeDatabaseItem type){
		
		mObject = obj;
		mCategorieSelected = categorie;
		if(type == ETypeDatabaseItem.LIST){
            mListe = new LinkedList();
            for (GSListe liste:activity.getAllListes()) {
                if (liste.getTypeListe() == obj) {
                    mListe.add(liste);
                }
            }
		}
		else {
			mListe = activity.getCategoriesListe();
		}
		alertDialog = new MaterialDialog.Builder(activity)
				.title(move?R.string.reorganiser:R.string.changerCategorie)
				.customView(R.layout.popup_listview, false)//false --> Permet de supprimer les margins
				.positiveText(move?R.string.valider:R.string.annuler)
				.negativeText(move?R.string.annuler:R.string.empty)
				.onPositive(new MaterialDialog.SingleButtonCallback() {
					@Override
					public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
						if(move){
							if(type == ETypeDatabaseItem.LIST){
                                ArrayList<GSListe> listeRes = new ArrayList<>();
                                int pos = 0;
                                for (IDatabaseItems itm:adapter.getListe()) {
                                    GSListe gsListe = (GSListe)itm;
                                    gsListe.setPosition(pos);
                                    listeRes.add(gsListe);
                                    pos++;
                                }
                                activity.reorganiserListes(listeRes);
                            }
							else {
								activity.modifierPositionCategorie(mDebut, mFin);
								activity.gererSpinnerCategories(mCategorieSelected);
								activity.gererListView();
							}
						}
					}
				})
				.show();

		View alertDialogView = alertDialog.getCustomView();

		mListView = (DynamicListView)alertDialogView.findViewById(R.id.dynamiclistview);

		adapter = new MovingAdapter(activity, mListe, move, this, type);
		AlphaInAnimationAdapter animationAdapter = new AlphaInAnimationAdapter(adapter);
		animationAdapter.setAbsListView(mListView);
		assert animationAdapter.getViewAnimator() != null;
		animationAdapter.getViewAnimator().setInitialDelayMillis(100);
		mListView.setAdapter(animationAdapter);
		
		mListView.enableDragAndDrop();
	
		mListView.setDraggableManager(new TouchViewDraggableManager(R.id.list_row_draganddrop_touchview));
			
		mListView.setOnItemMovedListener(this);

		if(!move) {
			mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			mListView.setItemChecked(2, true);
		}

	}
	
	@Override
	public void onItemMoved(final int originalPosition, final int newPosition) {
		mDebut = originalPosition;
		mFin = newPosition;
		adapter.move(originalPosition, newPosition);
		adapter.notifyDataSetChanged();
    }
	
	public void setCategorieSelected(GSCategory category){
		if(mObject.getClass().getName().contains("GestionQuantite"))
			((GestionQuantite) mObject).setNouvelleCategorie(category);
		//On ferme la popup
		alertDialog.dismiss();
	}
	
}
