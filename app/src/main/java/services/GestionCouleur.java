package services;

import android.graphics.Color;

public class GestionCouleur {

	public enum EColor{
		WHITE, BLACK;
	}

	public static int getTextColor(String background){
		String res = "#000000";

		String R = "#"+background.substring(1,3);
		String V = "#"+background.substring(3,5);
		String B = "#"+background.substring(5,7);
		double calcul = 0.3*Integer.decode(R)+0.59*Integer.decode(V)+0.11*Integer.decode(B);

		if(calcul<=128)
			res = "#FFFFFF";
		return Color.parseColor(res);
	}

	public static EColor getImageColor(String background){
		EColor res = EColor.BLACK;

		String R = "#"+background.substring(1,3);
		String V = "#"+background.substring(3,5);
		String B = "#"+background.substring(5,7);
		double calcul = 0.3*Integer.decode(R)+0.59*Integer.decode(V)+0.11*Integer.decode(B);

		if(calcul<=128)
			res = EColor.WHITE;
		return res;
	}

}
