package services;

/**
 * Created by Alexis on 15/02/2017.
 */

public enum EAction {
    ADD, ALTER, REMOVE, CHECK;
}
