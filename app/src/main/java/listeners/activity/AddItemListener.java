package listeners.activity;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.fourrier.goodshopping.MainActivity;
import com.fourrier.goodshopping.R;

import java.util.HashMap;

import database.GSItem;
import services.GestionCategories;
import services.TypePopup;

/**
 * Created by Alexis on 04/02/2017.
 */

public class AddItemListener implements View.OnClickListener {

    private MainActivity mainActivity;

    public AddItemListener(MainActivity activity) {
        mainActivity = activity;
    }

    @Override
    public void onClick(View v) {
        if (!mainActivity.getmCategorieSelected().equals("all")) {
            if (!mainActivity.getTextToAdd().equals("")) {
                int mNumCategorieSelected = mainActivity.getmNumCategorieSelected();
                GSItem item = new GSItem(0,
                        mainActivity.getTextToAdd(),
                        mainActivity.getCategorys().get(mNumCategorieSelected-1).getId(),
                        false, mainActivity.getType(), mainActivity.getQuantity(),
                        mainActivity.getCategorys().get(mNumCategorieSelected-1).getListItems().size(),
                        mainActivity.getCategorys().get(mNumCategorieSelected-1).getFirebaseId()
                );
                //On lui donne la couleur de la categorie
                item.setColor(mainActivity.getCategorys().get(mNumCategorieSelected - 1).getCouleur());
                HashMap mHashMapLongueur = mainActivity.getmHashMapLongueur();
                if (mHashMapLongueur != null) {
                    if(mHashMapLongueur.containsKey("long"))
                        item.setLongueur((float)mHashMapLongueur.get("long"));
                    if(mHashMapLongueur.containsKey("longType"))
                        item.setLongueurType((int)mHashMapLongueur.get("longType"));

                    if(mHashMapLongueur.containsKey("larg"))
                        item.setLargeur((float)mHashMapLongueur.get("larg"));
                    if(mHashMapLongueur.containsKey("largType"))
                        item.setLargeurType((int)mHashMapLongueur.get("largType"));

                    if(mHashMapLongueur.containsKey("haut"))
                        item.setHauteur((float)mHashMapLongueur.get("haut"));
                    if(mHashMapLongueur.containsKey("hautType"))
                        item.setHauteurType((int)mHashMapLongueur.get("hautType"));
                    mHashMapLongueur = null;
                }
                mainActivity.setQuantite(0);
                mainActivity.setType(0);
                mainActivity.resetEditTextAdd();
                //mAdapter.move(mListeDeCoursesCategorie.size() - 1, 0);
                //mAdapter.notifyDataSetChanged();

                mainActivity.addItem(item);


            } else {
                mainActivity.requestFocus();
                ((InputMethodManager) mainActivity.getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
        } else {

            if (mainActivity.getCategoriesListe().size() > 0) {
                Toast.makeText(mainActivity, mainActivity.getString(R.string.besoinCategorie), Toast.LENGTH_SHORT).show();
                mainActivity.openSpinnerCategorie();
            } else {
                Toast.makeText(mainActivity, mainActivity.getString(R.string.besoinCreerCategorie), Toast.LENGTH_SHORT).show();
                new GestionCategories(TypePopup.NEW, 0, mainActivity, null);
            }
        }
    }
}
