package com.fourrier.goodshopping;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.color.ColorChooserDialog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.nhaarman.listviewanimations.itemmanipulation.dragdrop.OnItemMovedListener;
import com.nhaarman.listviewanimations.itemmanipulation.dragdrop.TouchViewDraggableManager;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.OnDismissCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import adapters.ListCourseAdapter;
import adapters.SpinnerAdapter;
import database.DatabaseHandler;
import database.ETypeDatabaseItem;
import database.GSCategory;
import database.GSItem;
import database.GSListe;
import database.ListeCompose;
import database.TypeListe;
import listeners.activity.AddItemListener;
import services.ETypeFab;
import services.GestionCategories;
import services.GestionCouleur;
import services.GestionListView;
import services.GestionQuantite;
import services.IColorSelector;
import services.TypePopup;
import services.Useful;

public class MainActivity extends AppCompatActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks, OnItemMovedListener,
		ColorChooserDialog.ColorCallback, IActivity{

/**
* Fragment managing the behaviors, interactions and presentation of the
* navigation drawer.
*/
	private NavigationDrawerFragment mNavigationDrawerFragment;

	private final String MY_AD_UNIT_ID="ca-app-pub-7984960772660730/4372462005";
	public final int REQUEST_CAMERA = 0;
	public final int SELECT_FILE    = 1;
	private AdView adView;
	private CharSequence mTitle;
	private LinkedList<GSItem> mListeDeCoursesCategorie;
	private LinkedList<GSCategory> mCategories;
	private LinkedList<GSListe> mToutesLesListes; //Contien des liste sans categorie (Juste pour leur nom)
	private DynamicListView mListView;
	private ListCourseAdapter mAdapter;
	private String mCategorieSelected;
	private boolean mInverse;
	private boolean mProposition;
	private FloatingActionButton add;
	private FloatingActionButton quantite;
	private Button mBoutongererSpinnerCategories;
	private AutoCompleteTextView editAdd;
	private Spinner mSpinnerCategories;
	private float quantity;
	private int type;
	private int mPremierCoche;
	private int mNumCategorieSelected;
	private HashMap mHashMapLongueur;
	private RelativeLayout mLayoutHaut;
	private boolean mExit;
	private Toolbar mToolbar;
	private DatabaseHandler mDb;
	private GSFirebase firebase;
	private Async async;

	private GSListe mListeActive;
	private GSItem mItemSelected;
	private IColorSelector mPopup;
	private Uri mImageUri;
	private String strToFind;
	private DatabaseReference database;
	private GestionQuantite popupGestionQuantite;
    private ETypeFab typeFab;
    private ValueEventListener valueEventListener;

	private boolean FIREBASE_FLAG; //Permet de savoir si il y a eu une modification
	public final static String FIREBASE_CONNEXION = "FIREBASE_CONNEXION";
	public final static String FIREBASE_DECONNEXION = "FIREBASE_DECONNEXION";
	public final static String FIREBASE_MAIL = "FIREBASE_MAIL";
	public final static String FIREBASE_PASS = "FIREBASE_PASS";
	public final static String GLOBAL = "GLOBAL";
	public final static int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;
	public final static int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 2;
    public static String DEVICE_ID;


    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        System.out.println("MainActivity.onCreate");
        mDb = new DatabaseHandler(this);
		mCategorieSelected = "all";
		strToFind = "";
		mNumCategorieSelected = 0;
		mHashMapLongueur = null;
		mExit = false;
		database = FirebaseDatabase.getInstance().getReference();
		firebase = new GSFirebase(database, mDb);
		async = new Async(this, mDb, firebase);
//		compteurFirabase = 0;
		FIREBASE_FLAG = false;
		popupGestionQuantite = null;
        DEVICE_ID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
		getData();
	}

	public void afterLoading(){
		setContentView(R.layout.my_toolbar);
        //if(gratuit)
        gererPub();
		//Declaration de la toolbar
		mToolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
		setSupportActionBar(mToolbar);
		mToolbar.setVisibility(View.VISIBLE);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));

		mToolbar.setNavigationIcon(
				new ColorDrawable(getResources().getColor(android.R.color.transparent)));

		mNavigationDrawerFragment.openMenuLateral();

		//Listener boutons
		add                            = (FloatingActionButton)findViewById(R.id.buttonAddToListe);
		add.setOnClickListener(new AddItemListener(this));

		editAdd = (AutoCompleteTextView)findViewById(R.id.editTextAddToListe);


		quantite                       = (FloatingActionButton)findViewById(R.id.buttonAddQuantity);
		quantite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
                if(typeFab == ETypeFab.FIND){

//                }
//                if(mCategorieSelected.contentEquals("all")){
					int nbFind = find(editAdd.getText().toString());
					if(nbFind>0) {
						strToFind = editAdd.getText().toString();
						Toast.makeText(getApplicationContext(),
								String.format(getResources().getQuantityString(R.plurals.items_find,nbFind),nbFind), Toast.LENGTH_SHORT).show();
						gererListView();
					}
					else {
						Toast.makeText(getApplicationContext(), getString(R.string.error_not_find), Toast.LENGTH_SHORT).show();
					}
				}
				else if(typeFab == ETypeFab.ADD){
                    addItemsToAnotherList();
                }
				else {
					popupGestionQuantite = new GestionQuantite(MainActivity.this, null, TypePopup.NEW);
				}
			}
		});

		mBoutongererSpinnerCategories  = (Button)findViewById(R.id.buttonAddCategorie);
		mBoutongererSpinnerCategories.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(mNumCategorieSelected==0)
					new GestionCategories(TypePopup.NEW, 0, MainActivity.this, null);
				else
					new GestionCategories(TypePopup.OLD, mNumCategorieSelected-1, MainActivity.this,
							mCategories.get(mNumCategorieSelected-1));
				//ajouterCategorie(mNumCategorieSelected);
			}
		});

		editAdd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId==EditorInfo.IME_ACTION_DONE) {
					add.performClick();
                    // TODO: 17/07/2017 ne fonctione pas
                }
				if(actionId==EditorInfo.IME_ACTION_PREVIOUS) {
				}
				return false;
			}
		});

		mSpinnerCategories             = (Spinner)findViewById(R.id.spinnerCategories);
		mLayoutHaut                    = (RelativeLayout)findViewById(R.id.layoutHaut);
//		gererListView();
		gererSpinnerCategories("all");

		if(mProposition)
			editAdd.setThreshold(2);
		else
			editAdd.setThreshold(20);

		ArrayAdapter<String> adapter = new ArrayAdapter<>
				(MainActivity.this,android.R.layout.simple_list_item_1,Useful.getAllItems(MainActivity.this));

		editAdd.setAdapter(adapter);

		editAdd.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				new EditTextValidator().execute();
			}
		});

        loadDataFromFirebase();

	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager
				.beginTransaction()
				.replace(R.id.container,
						PlaceholderFragment.newInstance(position + 1)).commit();
	}

	public void onSectionAttached(int number) {

	}

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(android.support.v7.app.ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.main, menu);
			restoreActionBar();
			MenuItem item = menu.findItem(R.id.nbpd);
			if(item!=null) {
				if(mListeActive!= null) {
					if (mListeActive.getTypeListe() == TypeListe.LISTE_RECETTE)
						item.setVisible(true);
					else
						item.setVisible(false);
				}
				else{
					item.setVisible(false);
				}
			}
            item = menu.findItem(R.id.menu_add_to_another_list);
            if(item!=null) {
                if(mListeActive!= null) {
                    if (mListeActive.getTypeListe() == TypeListe.LISTE_RECETTE)
                        item.setVisible(true);
                    else
                        item.setVisible(false);
                }
                else{
                    item.setVisible(false);
                }
            }
			item = menu.findItem(R.id.menu_reorganiser_categories);
			if(item!=null) {
				if (mListeActive.getTypeListe() == TypeListe.LISTE_COMPOSE) {
					item.setVisible(false);
				}
				else{
					item.setVisible(true);
				}
			}

			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
        SharedPreferences prefs =getSharedPreferences(MainActivity.GLOBAL, MODE_PRIVATE);
		if (item.getItemId() == R.id.touscocherdecocher) {
			 if(mPremierCoche==-1){
				 cocherDecocherTousLesItems(false);
				 item.setIcon(R.drawable.ic_check_box_black);
			 }
			 else{
				 cocherDecocherTousLesItems(true);
				 item.setIcon(R.drawable.ic_check_box_outline_blank_black);
			 }
			return true;
		}
		else if (item.getItemId() == R.id.nbpd) {
			new MaterialDialog.Builder(this)
					.title(R.string.nbpd)
					.content(R.string.explication)
					.negativeText(R.string.annuler)
					.positiveText(R.string.save)
					.onPositive(new MaterialDialog.SingleButtonCallback() {
						@Override
						public void onClick(MaterialDialog dialog, DialogAction which) {
							int nbpd = Integer.parseInt(dialog.getInputEditText().getText().toString());
							if (nbpd <= 0)
								nbpd = mListeActive.getNbPersonnes();
							mListeActive.setNbPersonnsDemande(nbpd);
							mDb.updateListe(mListeActive);
                            firebase.alterListFromFirebase(mListeActive);
						}
					})
					.inputRange(0, -1)
					.input(null, Integer.toString(mListeActive.getNbPersonnesDemande()), false,
							new MaterialDialog.InputCallback() {
								@Override
								public void onInput(MaterialDialog dialog, CharSequence input) {
								}
							})
					.inputType(InputType.TYPE_CLASS_NUMBER)
					.show();

			return true;
		}
		else if (item.getItemId() == R.id.menu_add_to_another_list) {
            addItemsToAnotherList();
			return true;
		}
        else if (item.getItemId() == R.id.menu_share) {

            share();
            return true;
        }
		else if (item.getItemId() == R.id.menu_reorganiser_listes) {
            String[] choix = new String[]{getString(R.string.listeNormale), getString(R.string.listeRecettes)};
            new MaterialDialog.Builder(this)
                    .title(R.string.choose_list_type)
                    .items(choix)
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            new GestionListView(MainActivity.this,true, mCategorieSelected,
                                    which==0?TypeListe.LISTE_NORMALE:TypeListe.LISTE_RECETTE, ETypeDatabaseItem.LIST);
                        }
                    })
                    .show();
			return true;
		}

		else if (item.getItemId() == R.id.menu_reorganiser_categories) {
			new GestionListView(this,true, mCategorieSelected, null, ETypeDatabaseItem.CATEGORY);
			return true;
		}
		else if (item.getItemId() == R.id.menu_inverser) {
			mInverse=!mInverse;//On inverse les cases
            prefs.getBoolean(getString(R.string.inverse_key), mInverse);
			if(mInverse)
				item.setTitle(getString(R.string.CPA));
			else
				item.setTitle(getString(R.string.DPA));
			gererListView();
			return true;
		}
		else if(item.getItemId() == R.id.menu_suggestions) {
			mProposition = !mProposition;//On inverse
            prefs.getBoolean(getString(R.string.proposition_key), mProposition);

            item.setChecked(mProposition);
			if(mProposition)
				editAdd.setThreshold(2);
			else
				editAdd.setThreshold(20);
		}
		
		return super.onOptionsItemSelected(item);
	}

    private void addItemsToAnotherList() {
        final ArrayList<GSListe> listes = new ArrayList<>();
        ArrayList<String> listeName = new ArrayList<>();
        for (GSListe liste:mToutesLesListes) {
            if(liste.getTypeListe() == TypeListe.LISTE_NORMALE){
                listes.add(liste);
                listeName.add(liste.getNom());
            }
        }
        String[] array = listeName.toArray(new String[listeName.size()]);
        new MaterialDialog.Builder(this)
                .title(R.string.witch_to_add)
                .items(array)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                        async.addRecetteToList(mListeActive.getId(),listes.get(which).getId());

                        return true;
                    }
                })
                .positiveText(R.string.choose)
                .negativeText(R.string.annuler)
                .show();
    }

    @Override
	public void onColorSelection(ColorChooserDialog dialog, int selectedColor) {
		String hexColor = String.format("#%06X", (0xFFFFFF & selectedColor));
		mPopup.setColorSelected(hexColor);
		mPopup = null;
	}

	public void lancerPopupCouleur(IColorSelector popup, String color){
		mPopup = popup;

		new ColorChooserDialog.Builder(this, R.string.color)
				.titleSub(R.string.color)  // title of dialog when viewing shades of a color
				.accentMode(false)  // when true, will display accent palette instead of primary palette
				.doneButton(R.string.valider)  // changes label of the done button
				.cancelButton(R.string.annuler)  // changes label of the cancel button
				.backButton(R.string.md_back_label)  // changes label of the back button
				//.preselect(Color.parseColor(color))
				.dynamicButtonColor(true)  // defaults to true, false will disable changing action buttons' color to currently selected color
				.show();
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			((MainActivity) activity).onSectionAttached(getArguments().getInt(
					ARG_SECTION_NUMBER));
		}
	}

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(MainActivity.GLOBAL, MODE_PRIVATE);
        boolean load = true;

        if(prefs.getBoolean(FIREBASE_CONNEXION, false)){
            loadDataFromFirebaseConnexion();
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(MainActivity.FIREBASE_CONNEXION, false);
            edit.commit();
            load = false;
        }
        if(prefs.getBoolean(FIREBASE_DECONNEXION, false)){
            if(mNavigationDrawerFragment!=null){
                mNavigationDrawerFragment.refresh();
            }
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(MainActivity.FIREBASE_DECONNEXION, false);
            edit.commit();
            load = false;
        }

        if(adView!= null) {
            adView.resume();
            if (load) {
                loadDataFromFirebase();//Si adView == null : On create
            }
        }
    }

    @Override
    protected void onPause() {
        if(adView!= null) {
            adView.pause();
        }
        //TODO stop firebase listener
        if(database != null && valueEventListener != null && isUserConnected()) {
            database.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .removeEventListener(valueEventListener);
        }
        super.onPause();

    }
	
	@Override
	public void onBackPressed() {
		if(mNumCategorieSelected!=0){
			mSpinnerCategories.setSelection(0,true);
		}
		else {
			if(!strToFind.equals("") && mCategorieSelected.equals("all")){
				strToFind = "";
				gererListView();
				return;
			}
			if (!mExit) {
				Toast.makeText(getApplicationContext(), getString(R.string.backPressed), Toast.LENGTH_SHORT).show();
				Thread thread = new Thread() {
					public void run() {
						mExit = true;
						try {
							this.sleep(5000);//5 sec
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						mExit = false;
					}
				};
				thread.start();
			} else {
				finish();
			}
		}
    }

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == REQUEST_CAMERA) {

				try {
					Bitmap thumbnail = MediaStore.Images.Media.getBitmap(
							getContentResolver(), mImageUri);

//					new SaveImage().execute(thumbnail);
					async.saveImage(thumbnail);
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else if (requestCode == SELECT_FILE) {
				Uri selectedImageUri = data.getData();
				String[] projection = {MediaStore.MediaColumns.DATA};
				for(int i=0; i<projection.length;i++){
				}

				CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null,
						null);
				Cursor cursor = cursorLoader.loadInBackground();
				int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
				cursor.moveToFirst();

				String selectedImagePath = cursor.getString(column_index);

				Bitmap bm;
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inJustDecodeBounds = true;
				BitmapFactory.decodeFile(selectedImagePath, options);
				final int REQUIRED_SIZE = 200;
				int scale = 1;
				while (options.outWidth / scale / 2 >= REQUIRED_SIZE
						&& options.outHeight / scale / 2 >= REQUIRED_SIZE)
					scale *= 2;
				options.inSampleSize = scale;
				options.inJustDecodeBounds = false;
				bm = BitmapFactory.decodeFile(selectedImagePath, options);

//				new SaveImage().execute(bm);
				async.saveImage(bm);
			}
		}
	}

	public void autorisationPrendrePhoto(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }
        }
        else {
            prendrePhoto();
        }
    }

	public void prendrePhoto(){
		ContentValues value = new ContentValues();
		value.put(MediaStore.Images.Media.TITLE, "New Picture");
		value.put(MediaStore.Images.Media.DESCRIPTION, "Good Shopping");
		mImageUri = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, value);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
		startActivityForResult(intent, REQUEST_CAMERA);
	}

    public void autorisationImporterPhoto(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        }
        else {
            importerPhoto();
        }
    }

	public void importerPhoto(){
		Intent intent = new Intent(
				Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		this.startActivityForResult(
				Intent.createChooser(intent, getString(R.string.selectFile)),
				SELECT_FILE);
	}


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    prendrePhoto();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, R.string.errorTakePicture, Toast.LENGTH_SHORT).show();
                }
                return;
            }

            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    importerPhoto();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, R.string.erreurImportImage, Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

	public GSListe getListeActive()                {return mListeActive;}
	public LinkedList<GSListe> getAllListes()      {return mToutesLesListes;}
	public LinkedList getListeDeCoursesCategorie() {return mListeDeCoursesCategorie;}
	public LinkedList getCategoriesListe()         {return mCategories;}
	public int getPremierCoche()                   {return mPremierCoche;}
	public Toolbar getToolbar()                    {return mToolbar;}
	public  boolean getInverse()                   {return mInverse;}
	public  boolean getProposition()               {return mProposition;}
	public String getTextToAdd()                   {return editAdd.getText().toString();}
	public String getmCategorieSelected()          {return mCategorieSelected;}

	public NavigationDrawerFragment getNavigationDrawerFragment() {return mNavigationDrawerFragment;}

	public ListCourseAdapter getAdapter() {return mAdapter;}

	public void setToutesLesListes(LinkedList<GSListe> mToutesLesListes) {this.mToutesLesListes = mToutesLesListes;}

	public GestionQuantite getPopupGestionQuantite() {return popupGestionQuantite;}

	public GSItem getItemSelected() {return mItemSelected;}

	public void setmItemSelected(GSItem mItemSelected) {this.mItemSelected = mItemSelected;}

	public LinkedList<GSCategory> getCategorys() {
		return mCategories;
	}

	public int getType() {
		return type;
	}

	public float getQuantity() {
		return quantity;
	}

	public int getmNumCategorieSelected() {
		return mNumCategorieSelected;
	}

	public HashMap getmHashMapLongueur() {
		return mHashMapLongueur;
	}

	public void setQuantite(float quantite){
		quantity = quantite;
	}

	public void setType(int type){
		this.type = type;
	}

	public void setHashMapLongueur(HashMap map){
		mHashMapLongueur = map;
	}

	public void setCategorys(LinkedList<GSCategory> mCategories) {this.mCategories = mCategories;}

	public void setInverse(boolean mInverse) {this.mInverse = mInverse;}

	public void setProposition(boolean mProposition) {this.mProposition = mProposition;}

	public void setListeActive(GSListe mListeActive) {this.mListeActive = mListeActive;}

	public void replaceCategory(GSCategory category){
        for (GSCategory c:mCategories) {
            if(c.getId() == category.getId()){
                mCategories.set(mCategories.indexOf(c), category);
            }
        }
    }

    public void setFIREBASE_FLAG(boolean FIREBASE_FLAG) {
        this.FIREBASE_FLAG = FIREBASE_FLAG;
    }

    public void addCategoryToActiveList(GSCategory category){
		mCategories.add(category);
	}

	public void resetEditTextAdd(){
		editAdd.setText("");
	}

	public void requestFocus(){
		editAdd.requestFocus();
	}

	public void openSpinnerCategorie(){
		mSpinnerCategories.performClick();
	}

	public void setCategorieSelected(String categorie){
		mCategorieSelected = categorie;
		gererListView();
	}

	public void setItemSelected(GSItem mItemSelected) {
		this.mItemSelected = mItemSelected;
	}

	public void noter(){
        new MaterialDialog.Builder(this)
                .title(R.string.note)
                .content(R.string.noting_it)
                .positiveText(R.string.oui)
                .neutralText(R.string.plusTard)
                .negativeText(R.string.jamais)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        SharedPreferences prefs = getSharedPreferences(MainActivity.GLOBAL, MODE_PRIVATE);
                        loadDataFromFirebaseConnexion();
                        SharedPreferences.Editor edit = prefs.edit();
                        edit.putBoolean(getString(R.string.rate_key), true);
                        edit.commit();
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setData(Uri.parse("market://details?id=com.fourrier.goodshopping"));
                        SharedPreferences prefs = getSharedPreferences(MainActivity.GLOBAL, MODE_PRIVATE);
                        loadDataFromFirebaseConnexion();
                        SharedPreferences.Editor edit = prefs.edit();
                        edit.putBoolean(getString(R.string.rate_key), true);
                        edit.commit();
                        startActivity(intent);
                    }
                })
                .show();
	}

	public void gererListView(){
        mListeDeCoursesCategorie = new LinkedList<>();
		mListView = (DynamicListView)findViewById(R.id.listview_non_coche);
		ArrayList itemsCoche = new ArrayList();
		mPremierCoche = -1;
		//GSListe liste = mListeDeListes.get(mNumListeSelected);
		if(mNumCategorieSelected == 0){
			//LinkedList<GSCategory> listeCategorie = mListeActive.getListCategories();
			for(int i=0; i < mCategories.size();i++){
				//On recherche un item si strToFind n'est pas vide
				if(!strToFind.equals("")){
					for (GSItem item:mCategories.get(i).getListItems()) {
						if(item.getNom().toLowerCase().contains(strToFind.toLowerCase())){
							mListeDeCoursesCategorie.add(item);
						}
					}
				}
				else {
					mListeDeCoursesCategorie.addAll(mCategories.get(i).getListItems());
				}
			}
		}
		else{
			if(mCategories.size()>0)
				mListeDeCoursesCategorie.addAll(mCategories.get(mNumCategorieSelected-1).getListItems());//-1 car 0 = ALL
		}
		GSItem item;
		//On recupere les items coche
		for(int i=0; i<mListeDeCoursesCategorie.size();i++){
			item = (GSItem)mListeDeCoursesCategorie.get(i);
			if(item.isSelect())
				itemsCoche.add(i);
		}
		//reordonerListeEnFonctionsDesCategories();
		int location;
		//Premier coche = taille de la liste de course - nombre d'items coche
		mPremierCoche = mListeDeCoursesCategorie.size()-itemsCoche.size();
		mPremierCoche--;
		//On ajoute a la fin
		for(int i=0; i<itemsCoche.size(); i++){
			location = (Integer) itemsCoche.get(i);
			item = (GSItem) mListeDeCoursesCategorie.get(location);
			mListeDeCoursesCategorie.addLast(item);
		}
		//On supprime
		for(int i=0; i<itemsCoche.size(); i++){
			location = (Integer) itemsCoche.get(i);
			location = location-i;//On retir les elements deja supprime
			mListeDeCoursesCategorie.remove(location);
		}

		mAdapter = new ListCourseAdapter(this, mListeDeCoursesCategorie, mCategorieSelected, mInverse);//, mOrdreItems);
		AlphaInAnimationAdapter animationmAdapter = new AlphaInAnimationAdapter(mAdapter);
		animationmAdapter.setAbsListView(mListView);
		assert animationmAdapter.getViewAnimator() != null;
		animationmAdapter.getViewAnimator().setInitialDelayMillis(100);
		mListView.setAdapter(animationmAdapter);

		if(!mCategorieSelected.equals("all")){//On desactive le drag & drop pour les items pour ALL
			mListView.enableDragAndDrop();
		}
		else{
			mListView.disableDragAndDrop();
		}

		mListView.setOnItemMovedListener(this);

		mListView.setDraggableManager(new TouchViewDraggableManager(R.id.list_courses_draganddrop_touchview));

		mListView.enableSwipeToDismiss(
				new OnDismissCallback() {
					@Override
					public void onDismiss(final ViewGroup listView, final int[] reverseSortedPositions) {
						for (int position : reverseSortedPositions) {

							//mAdapter.remove(position);
							cocherCase(position);
						}
					}
				}
		);

        if (mListeActive.getTypeListe() == TypeListe.LISTE_RECETTE){
            quantite.show();
        }
        else {
            quantite.hide();
        }
	}

	public void gererSpinnerCategoriesCurent(){
        gererSpinnerCategories(mCategorieSelected);
	}
	
	public void gererSpinnerCategories(String categorieNom){
        //On recupere la liste de category de la liste selectionnee
		//mListeDeListes.get(mNumListeSelected).getListCategories();
		final String[] listeCategorie = new String[mCategories.size()+1];
		final String[] listeCouleur   = new String[mCategories.size()+1];
		listeCategorie[0] = getResources().getString(R.string.all);
		listeCouleur[0]   = getResources().getString(R.string.blanc);
		GSCategory category;
		for(int i=0;i<mCategories.size();i++){
			category = mCategories.get(i);
			listeCategorie[i+1] = category.getNom();
			listeCouleur[i+1] = category.getCouleur();
		}
		SpinnerAdapter spinnerAdapter = new SpinnerAdapter(this, R.layout.item_spinner,listeCategorie, listeCouleur);
		mSpinnerCategories.setAdapter(spinnerAdapter);
		
		mSpinnerCategories.setOnItemSelectedListener(new OnItemSelectedListener() {
			//int first = 0;
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {
				//if(first>0){
				mNumCategorieSelected = position;

				if (mNumCategorieSelected == 0) {
					//Ajouter category
					mBoutongererSpinnerCategories.setBackgroundResource(R.drawable.ic_add_black);
				} else {
					//Modifier category
					mBoutongererSpinnerCategories.setBackgroundResource(
							GestionCouleur.getImageColor(listeCouleur[position])== GestionCouleur.EColor.BLACK?
					R.drawable.ic_edit_black:R.drawable.ic_edit_white);
				}

				if (position == 0) {
					setCategorieSelected("all");
					quantite.setImageResource(R.drawable.ic_find_white);
                    typeFab = ETypeFab.FIND;
				}
				else {
					setCategorieSelected(listeCategorie[position]);
					quantite.setImageResource(R.drawable.ic_edit_white);
                    typeFab = ETypeFab.QUANTITY;
                }

                if (mListeActive.getTypeListe() == TypeListe.LISTE_RECETTE){
                    quantite.setImageResource(R.drawable.ic_playlist_add_white);
                    quantite.show();
                    typeFab = ETypeFab.ADD;
                }

				//On change la couleur du layout
				String color = listeCouleur[position];
				mLayoutHaut.setBackgroundColor(Color.parseColor(color));
				//}
				//first++;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
		setCategorieSelected(categorieNom);
	}

	public void cocherCaseForId(int id, boolean select){
		//Item present dans la categorie actuelle
		boolean find = false;
		int i = 0;
		while (i<mListeDeCoursesCategorie.size() && !find){
			if(mListeDeCoursesCategorie.get(i).getId()==id){
				cocherCase(i);
				find = true;
			}
			i++;
		}
		//Item dans une autre categorie`
		if(!find){
			for(i=0; i<mCategories.size();i++){
				if(!find) {
					LinkedList<GSItem> itemCategory = mCategories.get(i).getListItems();
					for (int j = 0; j < itemCategory.size(); j++) {
						if (itemCategory.get(j).getId()==id) {

							itemCategory.get(j).setSelect(select);
							find = true;
						}
					}
				}
			}
		}
	}

	public void cocherCase(int p){
		final int position = p;//(Integer) mOrdreItems.get(p);
		GSItem item = mListeDeCoursesCategorie.get(position);
        //TODO remove or modify
		if(mPremierCoche==-1){
			mPremierCoche=0;//mListeDeCoursesCategorie.size();
			//mPremierCoche--;
		}
		else if(mPremierCoche==mListeDeCoursesCategorie.size()){
			mPremierCoche--;
		}

		if(!item.isSelect()){
			item.achete();
			item.setSelect(true);
       		mAdapter.move(position, mPremierCoche);
			move(position, mPremierCoche);
       		mPremierCoche--;
       	} else{
			item.setSelect(false);
			mAdapter.move(position, 0);
			move(position, 0);
			mPremierCoche++;
			//Trouver la bonne position
       	}
		//mListeDeCoursesCategorie.remove(position);
		mDb.updateItem(item);
		firebase.alterItemFromFirebase(item, this);

		if(mNavigationDrawerFragment!=null) {
			mNavigationDrawerFragment.onPrepareOptionsMenu();
		}

		mAdapter.notifyDataSetChanged();

	}

	public void move(int debut, int fin){
		//int a,b,c ;
		GSItem item = mListeDeCoursesCategorie.get(debut);
		//HashMap map = (HashMap) liste.get(debut);
		if(fin<debut){//Cocher
			for(int i = debut; i> fin; i--){
				if((i-1)>=0)
					mListeDeCoursesCategorie.set(i, mListeDeCoursesCategorie.get(i-1));
			}
			mListeDeCoursesCategorie.set(fin, item);
		}
		else{
			for(int i = debut; i< fin; i++){
				mListeDeCoursesCategorie.set(i, mListeDeCoursesCategorie.get(i+1));
			}
			mListeDeCoursesCategorie.set(fin, item);
		}
	}

	private void addNewItem(GSItem item){
		if(firebase.isUserConnected()){
			item.setFirebaseId(firebase.getNewFirebaseID());
		}
		int id = mDb.addItem(item);
		item.setId(id);
		mCategories.get(mNumCategorieSelected-1).addItem(item);//On ajoute l'item pour ne pas recharger la BDD
		mPremierCoche++;
		mListeDeCoursesCategorie.add(mPremierCoche, item);
		mAdapter.add(item);
		mAdapter.move(item.getEmplacement(), mPremierCoche);//On le place au debut
		mAdapter.notifyDataSetChanged();
		firebase.addItemToFirebase(item, this);
	}
	
	public void MyHandler(View v) {
		CheckBox cb = (CheckBox) v;
		//on recupere la position a l'aide du tag defini dans la classe MyListAdapter
		int position = Integer.parseInt(cb.getTag().toString());
		
		cocherCase(position);
	}
	
	public void modifierLibelleItem(View v){
		TextView tv = (TextView) v;
		//on recupere la position a l'aide du tag defini dans la classe MyListAdapter
		int id = Integer.parseInt(tv.getTag().toString());
        boolean find = false;
        GSItem item = null;
        Iterator<GSCategory> cateIterator = mListeActive.getListCategories().iterator();
        while (cateIterator.hasNext() && !find) {
            GSCategory cate = cateIterator.next();
            Iterator<GSItem> itemsIterator = cate.getListItems().iterator();
            while (itemsIterator.hasNext() && !find) {
                GSItem itm = itemsIterator.next();
                if (itm.getId() == id){
                    find = true;
                    item = itm;
                }
            }
        }
        if(item != null) {
            popupGestionQuantite = new GestionQuantite(this, item, TypePopup.OLD);
        }
	}
	
	@Override
	public void onItemMoved(final int originalPosition, final int newPosition) {

		mAdapter.move(originalPosition, newPosition);
		mAdapter.notifyDataSetChanged();
		modifierPositionItems(originalPosition, newPosition);
    }
	
	public void changerDeListeDeCourses(boolean remove, int idListe, TypeListe typeListe){
		//On commence par enregistrer la liste
		if(!remove) {
//			chargerListe(idListe, typeListe);
			async.loadData(idListe, typeListe);
		}
		else{
            mToutesLesListes.getFirst().setSelected(true);
			async.loadData(mToutesLesListes.getFirst().getId(),TypeListe.LISTE_NORMALE);
//			chargerListe(mToutesLesListes.getFirst().getId(),TypeListe.LISTE_NORMALE);
		}

	}

    public void getData(){
        async.loadAllListsEmpty(false);
        async.loadDataBegining();
    }

//	public void chargerListe(int idListe, TypeListe typeListe){
//
//		new LoadData().execute(idListe, TypeListe.getNum(typeListe));
//	}

	public void ajouterCategorie(String nom, String couleur){
		String fId = "";
		if(firebase.isUserConnected()){
			fId = firebase.getNewFirebaseID();
		}
		GSCategory category = new GSCategory(0, nom, couleur, mListeActive.getId(), mCategories.size(), fId);
		if(firebase.isUserConnected()){
			category.setFirebaseId(firebase.getNewFirebaseID());
		}
		int id = mDb.addCategorie(category);
		category.setId(id);
		mListeActive.addCategorie(category);
		//On recharge la liste de categories
		mCategories = (LinkedList)mDb.getAllCategoriesFromListeWithItems(mListeActive.getId());
		//On rafraichi le spiner
		gererSpinnerCategories(nom);
		//On ajoute la category a firebase
		firebase.addCategoryToFirebase(category, this);
	}

	public GSListe getListeWithId(int id){
		return mDb.getListe(id);
	}

	public ListeCompose ajouterNouvelleListeCompose(String nom, ArrayList<GSListe> listes){
		ListeCompose maNouvelleListe;
		int id = 0;

			maNouvelleListe = new ListeCompose(nom, listes);

		id = mDb.addListeCompose(maNouvelleListe);
		maNouvelleListe.setId(id);
		mToutesLesListes = (LinkedList)mDb.getAllListe();
		return maNouvelleListe;
	}

	public void dupliquerItem(GSItem i){
		GSItem item = i.clone();
		item.setImage("");
		int ec = getEmplacementCategorie(item.getIdCategorie());
		item.setEmplacement(mCategories.get(ec).getListItems().size());
		//Modification de l'id firebase
		item.setFirebaseId(firebase.getNewFirebaseID());
		int id = mDb.addItem(item);
		item.setId(id);
		mPremierCoche++;
		mCategories.get(ec).getListItems().add(item);
		if(mNumCategorieSelected==0){//ALL
			//On l'ajoute au debut
			mListeDeCoursesCategorie.add(0, item);
			mAdapter.add(item);//TODO si all indiquer position dans la liste
			mAdapter.move(mListeDeCoursesCategorie.size()-1, 0);
		}
		else{
			mListeDeCoursesCategorie.add(mPremierCoche, item);
			mAdapter.add(item);
			mAdapter.move(item.getEmplacement(), mPremierCoche);
		}

		firebase.addItemToFirebase(item, this);
		mAdapter.notifyDataSetChanged();
	}

	public void modifierListe(int position, GSListe liste){
		mDb.updateListe(liste);
		mToutesLesListes.set(position-1, liste);//TODO tester le -1 en fonction du type de liste
		firebase.alterListFromFirebase(liste);
	}

	public void modifierListeComposee(int position, GSListe liste, ArrayList<GSListe> listes){
		mDb.updateListeCompose(liste);
		mDb.updateListeCompose(listes,liste);
		mToutesLesListes.set(position-2, liste); 		//TODO update mtoutesLesLites
		//TODO refresh listview
		//TODO -1 ?
	}

	public void modifierCategorie(String nom, String couleur){
		GSCategory category = mCategories.get(mNumCategorieSelected-1);
		category.setNom(nom);
		category.setCouleur(couleur);
		mDb.updateCategorie(category);
		//On recharge la liste de categories
		mCategories.set(mNumCategorieSelected - 1, category);
		//On rafraichi le spiner
		gererSpinnerCategories(nom);
		GSItem item;
		for(int i = 0; i< category.getListItems().size(); i++){
			item = category.getItem(i);
			item.setNomCategorie(nom);
			item.setColor(couleur);
			category.setItem(i,item);
		}
		firebase.alterCategoryFromFirebase(category, this);
	}

	public void modifierPositionCategorie(int debut, int fin){
		mCategories.get(debut).setEmplacement(fin);
		GSCategory category = mCategories.get(debut);
		if(fin<debut){
			for(int i = debut; i> fin; i--){
				mCategories.get(i-1).setEmplacement(i);
				mCategories.set(i, mCategories.get(i-1));
			}
			mCategories.set(fin, category);
		}
		else{
			for(int i = debut; i< fin; i++){
				mCategories.get(i+1).setEmplacement(i);
				mCategories.set(i, mCategories.get(i + 1));
			}

			mCategories.set(fin, category);
		}
		//Creation du thread pour enregister dans la BDD
		Thread thread = new Thread(){
			public void run() {
				for(int i=0;i<mCategories.size(); i++) {
					mDb.updateCategorie(mCategories.get(i));
				}
			}
		};
		//Lancement du thread
		thread.start();
		//TODO reorganier dans firebase
		firebase.changeCategoryOrder(this);
	}

	public void modifierPositionItems(int debut, int fin){
		mListeDeCoursesCategorie.get(debut).setEmplacement(fin);
		GSItem item = mListeDeCoursesCategorie.get(debut);
		if(fin<debut){
			for(int i = debut; i> fin; i--){
				mListeDeCoursesCategorie.get(i - 1).setEmplacement(i);
				mListeDeCoursesCategorie.set(i, mListeDeCoursesCategorie.get(i-1));
			}
			mListeDeCoursesCategorie.set(fin, item);
		}
		else{
			for(int i = debut; i< fin; i++){
				mListeDeCoursesCategorie.get(i + 1).setEmplacement(i);
				mListeDeCoursesCategorie.set(i, mListeDeCoursesCategorie.get(i + 1));
			}

			mListeDeCoursesCategorie.set(fin, item);

		}
		mCategories.get(mNumCategorieSelected -1).setListItems(mListeDeCoursesCategorie);

		//Creation du thread pour enregister dans la BDD
		Thread thread = new Thread(){
			public void run() {
				for(int i=0;i<mListeDeCoursesCategorie.size(); i++){
					mDb.updateItem(mListeDeCoursesCategorie.get(i));
				}
			}
		};
		//Lancement du thread
		thread.start();
	}

	public void modifierItem(int idCategorieDepart, GSItem item, boolean sendToFirebase){
		if(item.getIdCategorie()!=idCategorieDepart) {
			GSItem itemDebut = item.clone();
			for(int i=0; i<mCategories.size();i++) {
				if (mCategories.get(i).getId() == item.getIdCategorie()) {
					//On position l'item a la fin de la liste
					item.setEmplacement(mCategories.get(i).getListItems().size());
					item.setColor(mCategories.get(i).getCouleur());
					mCategories.get(i).addItem(item);
					item.setIdFirebaseCategorie(mCategories.get(i).getFirebaseId());
				}
				else if(mCategories.get(i).getId() == idCategorieDepart){
					LinkedList<GSItem> listItems = mCategories.get(i).getListItems();
					for (int j = 0; j<listItems.size(); j++){
						if(listItems.get(j).getId() == item.getId())
							mCategories.get(i).getListItems().remove(j);
					}
				}
			}
			firebase.changeItemCategoryOnFirebase(itemDebut, item, this);
		}
		else{
			//On update aussi dans la category
			int categorieId = item.getIdCategorie();
			GSCategory category;
			for(int i=0; i<mCategories.size();i++){
				if(mCategories.get(i).getId() == categorieId) {
					LinkedList<GSItem> listItems = mCategories.get(i).getListItems();
					for (int j = 0; j<listItems.size(); j++){
						if(listItems.get(j).getId() == item.getId())
							mCategories.get(i).getListItems().set(j, item);
					}
				}
			}
		}
		mDb.updateItem(item);
		mAdapter.notifyDataSetChanged();
		gererListView();
		if(sendToFirebase) {
			firebase.alterItemFromFirebase(item, this);
		}
	}

	public void supprimerListe(int position, int listeId, TypeListe typeListe){
		if(typeListe == TypeListe.LISTE_COMPOSE){
			mDb.deleteListeCompose(listeId);
			mToutesLesListes.remove(position);
			return;
		}
		GSListe liste = null;
		int pos = 0;
		for(int i=0; i<mToutesLesListes.size();i++){
			if(mToutesLesListes.get(i).getId() == listeId) {
				liste = mToutesLesListes.get(i);
				pos = i;
			}
		}
		mDb.deleteListe(liste);
		mToutesLesListes.remove(pos);
		changerDeListeDeCourses(true, listeId, typeListe);
		firebase.removeListFromFirebase(liste);
	}

	public void removeFromCurentList(GSItem item){
        boolean find = false;
        Iterator<GSCategory> cateIterator = mListeActive.getListCategories().iterator();
        while (cateIterator.hasNext() && !find) {
            GSCategory cate = cateIterator.next();
            Iterator<GSItem> itemsIterator = cate.getListItems().iterator();
            while (itemsIterator.hasNext() && !find) {
                GSItem itm = itemsIterator.next();
                if (itm.getFirebaseId().equals(item.getFirebaseId())){
                    find = true;
                    itemsIterator.remove();
                }
            }
        }
    }

	public void supprimerCategorie(GSCategory category) {
		mDb.deleteCategorie(category);
		//On supprime aussi de la liste courante
		for(int i=0; i<mCategories.size();i++){
			if(mCategories.get(i).getId()== category.getId())
				mCategories.remove(i);
		}
		mNumCategorieSelected = 0;
		gererSpinnerCategories("all");
		firebase.removeItemFromFirebase(category, this);
	}

	public void supprimerItem(GSItem item, int position){
		mDb.deleteItem(item);
		//On supprime aussi de la category
		int categorieId = item.getIdCategorie();
		GSCategory category;
		for(int i=0; i<mCategories.size();i++){
			if(mCategories.get(i).getId() == categorieId) {
				LinkedList<GSItem> listItems = mCategories.get(i).getListItems();
				for (int j = 0; j<listItems.size(); j++){
					if(listItems.get(j).getId() == item.getId())
						mCategories.get(i).getListItems().remove(j);
				}
			}
		}
		mAdapter.remove(position);
		mAdapter.notifyDataSetChanged();
		firebase.removeItemFromFirebase(item, this);
	}

	public String getNewFirebaseID(){
		return firebase.getNewFirebaseID();
	}

	public boolean isUserConnected(){
		return firebase.isUserConnected();
	}

	public ArrayList<Integer> getIdFromListeCompose(int id){
		return mDb.getIdFromListeCompose(id);
	}

	private int getEmplacementCategorie(int idCategorie){
		int res = 1;
		for (int i=0; i<mCategories.size();i++){
			if(mCategories.get(i).getId()==idCategorie){
				res = i;
			}
		}
		return res;
	}
	
	public void cocherDecocherTousLesItems(boolean coche){
		GSItem item;
		for(int i=0; i<mListeDeCoursesCategorie.size();i++){
			item = mListeDeCoursesCategorie.get(i);
			if(item.isSelect()!=coche) {
				item.setSelect(coche);
				mDb.updateItem(item);
				firebase.alterItemFromFirebase(item, this);
			}
		}
		gererListView();
		mNavigationDrawerFragment.onPrepareOptionsMenu();
	}

	public void reorganiserListes(ArrayList<GSListe> array){
        for (GSListe liste:array) {
            //Update db
            mDb.updateListe(liste);
            //Update firebase
            firebase.alterListFromFirebase(liste);
        }
        async.loadAllListsEmpty(true);

    }

    public void reloadDrawer(){
        mNavigationDrawerFragment.reload(this);
    }

	public void share(){
		String liste = "";
		GSItem item;
		for(int i=0; i<mPremierCoche+1;i++){
			item = mListeDeCoursesCategorie.get(i);
			liste += item.getNom();
			liste += gererAffichageQuantite(item);
			liste += "\n";
		}

		//------------------------------------------------
		Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");
		sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, mListeActive.getNom());
		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, liste);
		startActivity(Intent.createChooser(sharingIntent, getString(R.string.shareVia)));
		//	-------------------------------------------------
	}

	private String gererAffichageQuantite(GSItem item){
		String str = " ";

		String[] listeTypeQuantite;
		String[] listeLongueur;
		List<String> lines = Arrays.asList(this.getResources().getStringArray(R.array.liste_type));
		listeTypeQuantite = lines.toArray(new String[9]);
		List<String> linesLong = Arrays.asList(this.getResources().getStringArray(R.array.liste_longueurs));
		listeLongueur  = linesLong.toArray(new String[6]);

		str+=Useful.affichageQuantite(item, listeLongueur, listeTypeQuantite, mListeActive);

		return str;
	}

	public void addItemToItemsList(GSItem item){
        for (GSCategory category:mCategories) {
            if(category.getId() == item.getIdCategorie()){
                category.addItem(item);
            }
        }
        mListeDeCoursesCategorie.addFirst(item);
    }

	public void addItem(final GSItem item){
        String nom = item.getNom();
		final ArrayList<String> itemExistant = new ArrayList<>();
		final ArrayList<Integer> itemId = new ArrayList<>();

		boolean exist = false;
		for(int i=0; i<mCategories.size();i++){
			LinkedList<GSItem> itemCategory = mCategories.get(i).getListItems();
			for (int j = 0; j < itemCategory.size(); j++) {
				if(itemCategory.get(j).getNom().equalsIgnoreCase(nom) && itemCategory.get(j).isSelect()){
					itemExistant.add("("+ mCategories.get(i).getNom() +") "+ itemCategory.get(j).getNom());
					itemId.add(itemCategory.get(j).getId());
					exist = true;
				}
			}
		}
        //Si il y a un element qui existe deja
		if(exist) {
			itemExistant.add(getString(R.string.new_item));
			new MaterialDialog.Builder(this)
					.title(R.string.item_already_exist)
					.items(itemExistant.toArray(new String[itemExistant.size()]))
					.itemsCallbackSingleChoice(itemExistant.size()-1, new MaterialDialog.ListCallbackSingleChoice() {
						@Override
						public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
							/**
							 * If you use alwaysCallSingleChoiceCallback(), which is discussed below,
							 * returning false here won't allow the newly selected radio button to actually be selected.
							 **/
							if (which == itemExistant.size() - 1) {
								addNewItem(item);
							} else {
								cocherCaseForId(itemId.get(which), false);
							}

							return true;
						}
					})
					.positiveText(R.string.choose)
					.show();
		}
		else{
			addNewItem(item);
		}
	}

	public void ajouterNouvelleListe(String nom, TypeListe type, boolean preRemplie, int nbp){
        mNavigationDrawerFragment.unselectAll();
		async.ajouterNouvelleListe(nom, type, preRemplie, nbp);
	}


	private class EditTextValidator extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
            runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (!editAdd.getText().toString().equals("")) {
						quantite.show();
                        if(mCategorieSelected.contentEquals("all")) {
                            quantite.setImageResource(R.drawable.ic_find_white);
                            typeFab = ETypeFab.FIND;
                        }
                        else {
                            quantite.setImageResource(R.drawable.ic_edit_white);
                            typeFab = ETypeFab.QUANTITY;
                        }
                    }
					else {
                        if (mListeActive.getTypeListe() == TypeListe.LISTE_RECETTE){
                            quantite.setImageResource(R.drawable.ic_playlist_add_white);
                            typeFab = ETypeFab.ADD;
                        }
                        else {
                            quantite.hide();
                        }
						if(!strToFind.equals("") && mCategorieSelected.equals("all")){
							strToFind = "";
							gererListView();
						}
					}
				}
			});
			return null;
		}
	}

	public int find(String name){
		int compteur = 0;
		for (GSCategory cate:mCategories) {
			for (GSItem item:cate.getListItems()) {
				if(item.getNom().toLowerCase().contains(name.toLowerCase())){
					compteur++;
				}
			}
		}
		return compteur;
	}

	public void refreshFirebase(){
        loadDataFromFirebase();
    }

	protected void loadDataFromFirebase(){
        if(firebase.isUserConnected()) {
            System.out.println("MainActivity.loadDataFromFirebase");
            database.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .addValueEventListener(valueEventListener = new ValueEventListener() {
//                    .addListenerForSingleValueEvent(new ValueEventListener() {
						@Override
						public void onDataChange(DataSnapshot dataSnapshot) {
                            if (!FIREBASE_FLAG) {
								try {
									async.loadDataFromFirebase(dataSnapshot, false);
								} catch (ExecutionException e) {
									e.printStackTrace();
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
						}

						@Override
						public void onCancelled(DatabaseError databaseError) {}
					});
		}
    }

	private void loadDataFromFirebaseConnexion(){
        System.out.println("MainActivity.loadDataFromFirebaseConnexion");
        System.out.println(firebase.isUserConnected());
        if(firebase.isUserConnected()) {
            System.out.println(database.child(FirebaseAuth.getInstance().getCurrentUser().getUid()));
            database.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
					.addListenerForSingleValueEvent(new ValueEventListener() {
						@Override
						public void onDataChange(final DataSnapshot dataSnapshot) {
                            System.out.println(dataSnapshot);
                            try {
                                // TODO: 07/07/2017 voulez vous garder les listes présentes ?
                                async.loadDataFromFirebase(dataSnapshot, true);//Attention risque de pas attendre
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } catch (ExecutionException e) {
                                e.printStackTrace();
                            }
						}

						@Override
						public void onCancelled(DatabaseError databaseError) {
                            System.out.println(databaseError);
                        }
					});

//			mNavigationDrawerFragment.refresh();
		}
	}

	// --- PUB --- PUB --- PUB --- PUB --- PUB --- PUB --- PUB --- PUB --- PUB --- PUB --- PUB --- PUB ---
	public void gererPub(){
   	 // Creez l'objet adView.
       adView = new AdView(this);
       adView.setAdUnitId(MY_AD_UNIT_ID);
       adView.setAdSize(AdSize.BANNER);

       // Recherchez l'entite LinearLayout en supposant qu'elle est associee a
       // l'attribut android:id="@+id/mainLayout".
       LinearLayout layout = (LinearLayout)findViewById(R.id.pub);

       // Ajoutez-y l'objet adView.
       layout.addView(adView);

       // Initiez une demande generique.
       AdRequest adRequest = new AdRequest.Builder()//.build();
               .addTestDevice("149BD419FB36A7B665EBAF7458011309")
            .addTestDevice("8DFAB5B67DB8868340F764FA6BA15CE1").build();//TODO remove

       // Chargez l'objet adView avec la demande d'annonce.
       adView.loadAd(adRequest);
     }

     @Override
     public void onDestroy() {
         if(adView != null){
             adView.destroy();
         }
         super.onDestroy();
     }

}