package com.fourrier.goodshopping;

/**
 * Created by Alexis on 13/07/2017.
 */

public interface IActivity {
    boolean isUserConnected();
    String getNewFirebaseID();
}
