package com.fourrier.goodshopping;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.database.DataSnapshot;

import java.text.Collator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;

import adapters.ListCourseAdapter;
import database.DatabaseHandler;
import database.GSCategory;
import database.GSItem;
import database.GSListe;
import database.TypeListe;
import services.EAction;
import services.EComparaison;
import services.ImageFile;
import services.Useful;

/**
 * Created by Alexis on 19/06/2017.
 */

public class Async {
    private MainActivity mainActivity;
    private DatabaseHandler mDb;
    private GSFirebase firebase;

    public Async(MainActivity mainActivity, DatabaseHandler mDb, GSFirebase firebase) {
        this.mainActivity = mainActivity;
        this.mDb = mDb;
        this.firebase = firebase;
    }

    public void addRecetteToList(final int idRecette, final int idNormale){
        new MaterialDialog.Builder(mainActivity)
                .title(R.string.what_do_you_want)
                .items(R.array.liste_choix_quantite)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        new AddRecetteToList().execute(idRecette, idNormale, which);
                        return true;
                    }
                })
                .positiveText(R.string.choose)
                .negativeText(R.string.annuler)
                .show();

    }

    public void saveImage(Bitmap thumbnail){
        new SaveImage().execute(thumbnail);
    }

    public void createListe(){
        new CreateList().execute();
    }

    public void loadDataBegining(){
        new LoadDataBegining().execute();
    }

    public void loadData(int idListe, TypeListe typeListe){
        new LoadData().execute(idListe, TypeListe.getNum(typeListe));
    }

    public void loadAllListsEmpty(boolean reload){
        new LoadAllListsEmpty().execute(reload);
    }

    public void loadDataFromFirebase(DataSnapshot dataSnapshot, boolean connexion) throws ExecutionException, InterruptedException {
        if(connexion){
            new LoadDataFromFirebase().execute(dataSnapshot, true).get();
            addListsToFireBase();
            mainActivity.loadDataFromFirebase();
        }
        else {
            new LoadDataFromFirebase().execute(dataSnapshot, false);
        }

    }

    public void addListsToFireBase() throws ExecutionException, InterruptedException {
        new AddListsToFireBase().execute().get();
    }

    public void ajouterNouvelleListe(String nom, TypeListe type, boolean preRemplie, int nbp){
        new CreateNewList().execute(nom, type, nbp, mainActivity, preRemplie);
    }

    private class LoadDataFromFirebase extends AsyncTask<Object, Void, Void> {
        private final int LIST = 1;
        private final int CATEGORY = 3;
        private final int ITEM = 5;
        private GSListe liste;
        private GSCategory category;
        private HashMap<Integer,HashMap<EAction,ArrayList<Object>>> newObject;
        private String deviceID;
        private LinkedList<GSCategory> categorysToRemove;
        private ArrayList<GSListe> compteur_list;
        private LinkedList<GSListe> listesVide;
        private boolean connexion;



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mainActivity.setFIREBASE_FLAG(true);
        }

        /**
         * 0 --> DataSnapshot
         * 1 --> onCreate
         * @param data
         * @return
         */
        protected Void doInBackground(Object... data) {
            DataSnapshot dataSnapshot = (DataSnapshot) data[0];
            listesVide = (LinkedList<GSListe>) mDb.getAllListe();
            connexion = (boolean) data[1];
            newObject = new HashMap<>();

            HashMap<EAction, ArrayList<Object>> hmL = new HashMap<>();
            hmL.put(EAction.ADD, new ArrayList<>());
            hmL.put(EAction.ALTER, new ArrayList<>());
            hmL.put(EAction.REMOVE, new ArrayList<>());

            HashMap<EAction, ArrayList<Object>> hmC = new HashMap<>();
            hmC.put(EAction.ADD, new ArrayList<>());
            hmC.put(EAction.ALTER, new ArrayList<>());
            hmC.put(EAction.REMOVE, new ArrayList<>());

            HashMap<EAction, ArrayList<Object>> hmI = new HashMap<>();
            hmI.put(EAction.ADD, new ArrayList<>());
            hmI.put(EAction.ALTER, new ArrayList<>());
            hmI.put(EAction.CHECK, new ArrayList<>());
            hmI.put(EAction.REMOVE, new ArrayList<>());

            newObject.put(LIST, hmL);
            newObject.put(CATEGORY, hmC);
            newObject.put(ITEM, hmI);

            compteur_list = new ArrayList<>();

            deviceID = mainActivity.DEVICE_ID;

            parcourirDataSnapshot(dataSnapshot, 0);
            return null;
        }

        private void parcourirDataSnapshot(DataSnapshot dataSnapshot,final int niveau){
            Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();

            while (iterator.hasNext()) {
                DataSnapshot ds = iterator.next();
                String firebaseDeviceId = "";
                if(niveau == ITEM) {
                    HashMap hashMap = (HashMap) ds.getValue();
                    if (hashMap.containsKey("device_ID")) {
                        firebaseDeviceId = (String) hashMap.get("device_ID");
                    }

                }
                if(niveau == LIST) {
                    categorysToRemove = new LinkedList<>();
//					System.out.println(niveau + " ----> " + ds.getValue().toString());//TODO remove

                    String firebaseId = (String)((HashMap) ds.getValue()).get("firebaseId");

                    GSListe new_list = new GSListe(firebaseId);
                    new_list.setList(((HashMap) ds.getValue()));
                    compteur_list.add(new_list);


                    if(mDb.isListPresent(firebaseId)){
                        liste = mDb.getListeWithFirebaseId(firebaseId);
                        Iterator ite = listesVide.iterator();
                        boolean find = false;
                        while (ite.hasNext() && !find){
                            if (((GSListe)ite.next()).getId() == liste.getId()){
                                ite.remove();
                                find = true;
                            }
                        }
                    }
                    else {
                        newObject.get(LIST).get(EAction.ADD).add(new_list);
                        int id = mDb.addListe(new_list);
                        liste = new_list;
                        liste.setId(id);
                    }

                    categorysToRemove = (LinkedList<GSCategory>) liste.getListCategories().clone();

                    if(!liste.equals((HashMap)ds.getValue())){
                        liste.setList((HashMap)ds.getValue());
                        mDb.updateListe(liste);

                        newObject.get(LIST).get(EAction.ALTER).add(liste);
                    }

                    if(!iterator.hasNext()){
                        for (GSListe l:listesVide) {
                            if(l.getTypeListe().toString().equals(dataSnapshot.getKey()) && !connexion) {
                                mDb.deleteListe(l);
                                newObject.get(LIST).get(EAction.REMOVE).add(l);
                            }
                        }
                    }

                    //TODO ajout/suppression liste
                }
                else if(niveau == CATEGORY) {
//					System.out.println(niveau + " ----> " + ds.getValue().toString());//TODO remove
                    String catgoryNumber  = (String)((HashMap) ds.getValue()).get("firebaseId");

                    category = new GSCategory(catgoryNumber);
                    category.setCatgegory(((HashMap) ds.getValue()));
                    category.setIdListe(liste.getId());
//                    categorysToRemove.add(category);
                    boolean bool = false;//mDb.isCategoryPresent(catgoryNumber);
                    for (GSCategory cate: categorysToRemove) {
                        if(cate.getFirebaseId().equals(category.getFirebaseId())){
                            bool = true;
                        }
                    }
                    if(!bool){
//                        System.out.println("AJOUT DE LA CATEGORIE : "+category.getNom());
                        category.setIdListe(liste.getId());
                        int catgoryId = mDb.addCategorie(category);
                        category.setId(catgoryId);
                        liste.addCategorie(category);
                        if(liste.getId() == mainActivity.getListeActive().getId()){
                            newObject.get(CATEGORY).get(EAction.ADD).add(category);
                        }

                    }
                    else{

                        GSCategory cate = liste.getCategoryByFirebaseId(category.getFirebaseId());
                        if(cate == null){
                            cate = mDb.getCategorieWithFirebaseId(category.getFirebaseId(), liste.getId());
                            mDb.updateCategorie(cate);
                        }
                        category.setId(cate.getId());
                        category.setListItems(cate.getListItems());

                        if(cate.equals(category)==EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID){
                            mDb.updateCategorie(category);
                            if(!category.getCouleur().equals(cate.getCouleur())){
                                for (GSItem i:category.getListItems()) {
                                    i.setColor(category.getCouleur());
                                }
                            }
                            if(liste.getId() == mainActivity.getListeActive().getId()) {
                                mainActivity.replaceCategory(category.clone());
                                newObject.get(CATEGORY).get(EAction.ALTER).add(category);
                            }
                            liste.getListCategories().set(liste.getListCategories().indexOf(cate), category);
                        }

                        //Regarder si elle a des items, si elle en a et qu'il n'y a aps d'enfants au ds
                        //Supprimer tous les items de cette categorie
                        if(cate.getListItems().size()>0 && !((HashMap) ds.getValue()).containsKey("items")){
                            for (GSItem item:cate.getListItems()) {
                                newObject.get(ITEM).get(EAction.REMOVE).add(item);
                                mDb.deleteItem(item);
                            }
                        }

                        categorysToRemove.remove(cate);
                    }
                    //Dernier element
                    if(!iterator.hasNext()){

                        for (GSCategory cate: categorysToRemove) {
                            mDb.deleteCategorie(cate);
                            if(liste.getId() == mainActivity.getListeActive().getId()) {
                                newObject.get(CATEGORY).get(EAction.REMOVE).add(cate);
                            }
                        }
                    }
                }
                else if(niveau == ITEM && dataSnapshot.getKey().equals("items")) {
//                    System.out.println(niveau + " ----> " + ds.getValue().toString());//TODO remove

                    GSItem new_item = new GSItem((String)((HashMap) ds.getValue()).get("firebaseId"));
                    new_item.setItem(((HashMap) ds.getValue()));
                    new_item.setIdCategorie(category.getId());
                    new_item.setColor(category.getCouleur());
                    new_item.setIdFirebaseCategorie(category.getFirebaseId());
                    boolean add = true;
                    Iterator<GSItem> itemsIterator = category.getListItems().iterator();
                        while (itemsIterator.hasNext() && add) {
                            GSItem item = itemsIterator.next();
                            //Si meme id firebase, mais avec une modif
                            EComparaison isEquals = item.equals(new_item);
                            if (isEquals == EComparaison.FALSE_WITH_EQUALS_FIRABSE_ID) {
                                if(!firebaseDeviceId.equals(deviceID)) {
                                    new_item.setId(item.getId());
                                    mDb.updateItem(new_item);
                                    if (liste.getFirebaseId().equals(mainActivity.getListeActive().getFirebaseId())) {
                                        mainActivity.getListeActive().setItemForFirebaseId(new_item.getFirebaseId(), new_item);//TODO utiliser un setter
                                        newObject.get(ITEM).get(EAction.ALTER).add(new_item);
                                    }
                                    //Si la selection de l'item change, on le (de)coche
                                    if (item.isSelect() != new_item.isSelect()) {
                                        newObject.get(ITEM).get(EAction.CHECK).add(new_item);
                                    }
                                }
                                add = false;
                                itemsIterator.remove();
                            } else if (isEquals == EComparaison.TRUE) {
                                add = false;
                                itemsIterator.remove();
                            }
                        }
//                    }
                    if(!iterator.hasNext()){
//                        System.out.println(category.getNom()+" items restants : "+category.getListItems());//TODO remove
                        for (GSItem it: category.getListItems()) {
                            if(liste.getFirebaseId().equals(mainActivity.getListeActive().getFirebaseId())) {
                                newObject.get(ITEM).get(EAction.REMOVE).add(it);
                                mDb.deleteItem(it);
                            }
                        }
                    }
                    if(add){
                        int id = mDb.addItem(new_item);
                        new_item.setId(id);
                        if(liste.getFirebaseId().equals(mainActivity.getListeActive().getFirebaseId())) {
                            newObject.get(ITEM).get(EAction.ADD).add(new_item);
                        }
                    }
                }


                Iterator newIterator = ds.getChildren().iterator();
                if(newIterator.hasNext() && niveau<ITEM/* && !find*/) {
                    parcourirDataSnapshot(ds, niveau+1);
                }
            }
//			System.out.println("IS IT FIND ? "+(find?"Yes":"No"));
        }

        @Override
        protected void onPostExecute(Void v) {
            super.onPostExecute(v);
            mainActivity.setFIREBASE_FLAG(false);
            System.out.println("FIN");// TODO: 08/07/2017 remove
            NavigationDrawerFragment drawer = mainActivity.getNavigationDrawerFragment();
            ListCourseAdapter adapter = mainActivity.getAdapter();
            for (int key : newObject.keySet()) {
                HashMap<EAction, ArrayList<Object>> map = newObject.get(key);
                if(key == LIST){
                    if(map.get(EAction.ALTER).size()>0){
                        for (Object listes:map.get(EAction.ALTER)) {
                            drawer.refresh((GSListe)listes);
                        }
                        if(map.get(EAction.ALTER).size()>0){
                            loadAllListsEmpty(true);
                        }
                    }
                    if(map.get(EAction.REMOVE).size()>0){
                        for (Object listes:map.get(EAction.REMOVE)) {
                            drawer.remove((GSListe) listes);
                            //Si c'est la liste selectionnee, on redirige vers la premiere
                            if(((GSListe) listes).getId() == mainActivity.getListeActive().getId()){
                                mainActivity.changerDeListeDeCourses(true, 0, null);
                            }
                        }
                        drawer.refresh();
                    }
                    if(map.get(EAction.ADD).size()>0){
                        for (Object listes:map.get(EAction.ADD)) {
                            drawer.add((GSListe)listes);
                        }
                        drawer.refresh();
                    }
                }
                else if(key == CATEGORY){
                    int totalModif = 0;
                    for (Object cate:map.get(EAction.ADD)) {
                        mainActivity.addCategoryToActiveList((GSCategory) cate);
                        if(((GSCategory) cate).getIdListe()==mainActivity.getListeActive().getId()){
                            totalModif++;
//                            mainActivity.gererSpinnerCategoriesCurent();
                        }
                    }
                    for (Object cate:map.get(EAction.ALTER)) {
                        if(((GSCategory) cate).getIdListe()==mainActivity.getListeActive().getId()){
                            totalModif++;
//                            mainActivity.gererSpinnerCategoriesCurent();
                        }
                    }
                    for (Object cate:map.get(EAction.REMOVE)) {
                        Iterator<GSCategory> iteratorCate = mainActivity.getCategorys().iterator();
                        while (iteratorCate.hasNext()){
                            GSCategory cateFromListeCate = iteratorCate.next();
                            if(cateFromListeCate.getFirebaseId().equals(((GSCategory)cate).getFirebaseId())){
                                iteratorCate.remove();
                            }
                            if(((GSCategory) cate).getIdListe()==mainActivity.getListeActive().getId()){
                                totalModif++;
//                                mainActivity.gererSpinnerCategoriesCurent();
                            }
                        }
                    }
                    if(totalModif>0){
                        mainActivity.gererSpinnerCategoriesCurent();
                    }
                }
                else if(key == ITEM){
                    // TODO: 06/07/2017 effectuer les modification dans la liste courante
                    int totalModif = 0;
                    for (Object item:map.get(EAction.ADD)) {
                        adapter.add(0, item);
                        mainActivity.addItemToItemsList((GSItem) item);
                        totalModif++;
                    }
                    for (Object item:map.get(EAction.ALTER)) {
                        adapter.setItem((GSItem) item);
                        if(map.get(EAction.CHECK).contains(item)){
                            mainActivity.cocherCaseForId(((GSItem)item).getId(), ((GSItem)item).isSelect());
                        }
                        totalModif++;
                    }
                    for (Object item:map.get(EAction.REMOVE)) {
                        adapter.removeItem((GSItem) item);
                        mainActivity.removeFromCurentList((GSItem) item);
                        totalModif++;
                    }
                    if(totalModif>0){
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }
    }

    private class LoadData extends AsyncTask<Integer, Void, Integer> {
        private MaterialDialog dialog;

        @Override
        protected Integer doInBackground(Integer... idListe) {
            GSListe listeActive;
            if(TypeListe.listToNum(idListe[1])==TypeListe.LISTE_COMPOSE){
                listeActive = mDb.getListeCompose(idListe[0]);
            }
            else {
                listeActive = mDb.getListe(idListe[0]);
            }
            mainActivity.setListeActive(listeActive);
//            mCategories = mListeActive.getListCategories();
            mainActivity.setCategorys(listeActive.getListCategories());
            return 0;
        }

        @Override
        protected void onPreExecute() {
            dialog = new MaterialDialog.Builder(mainActivity)
                    .title(R.string.loading)
                    .content(R.string.please_wait)
                    .progress(true, 0)
                    .cancelable(false)
                    .canceledOnTouchOutside(false)
                    .show();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            dialog.hide();
            mainActivity.gererListView();
            mainActivity.getNavigationDrawerFragment().close();
            mainActivity.gererSpinnerCategories("all");
        }
    }

    private class AddListsToFireBase extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mainActivity.setFIREBASE_FLAG(true);
            System.out.println("AJOUT DES LISTES A FIREBASE");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mainActivity.setFIREBASE_FLAG(false);
            mainActivity.reloadDrawer();
        }

        @Override
        protected Void doInBackground(Void... v) {
            if(firebase.isUserConnected()) {
                for (GSListe liste : mainActivity.getAllListes()) {
                    if (liste.getTypeListe() != TypeListe.LISTE_COMPOSE) {
                        //Si la liste n'a pas d'ID firebase, on présice le nom de l'appareil
                        if(liste.getFirebaseId().equals("")){
                            liste.setNom(liste.getNom()+" ("+android.os.Build.MODEL+")");
                            mDb.updateListe(liste);
                        }
                        GSListe l = firebase.confirmFirebaseId_List(mDb.getListe(liste.getId()));
                        firebase.addListToFireBase(l);
                        mainActivity.setToutesLesListes((LinkedList<GSListe>) mDb.getAllListe());
//                        mainActivity.firebaseModification(30);

                    }
                }
            }
            return null;
        }
    }

    private class CreateList extends AsyncTask<Void, Void, Void> {
        private MaterialDialog dialog;

        @Override
        protected Void doInBackground(Void... v) {

            mainActivity.setProposition(true);
            //Creation de la liste par defaut
            GSListe listeActive = Useful.nouvelleListe(mainActivity, mainActivity.getString(R.string.defaut));
            //mListeActive = Useful.nouvelleListe(mainActivity, mainActivity.getString(R.string.defaut));
            mainActivity.setListeActive(listeActive);
//            mCategories = listeActive.getListCategories();
            mainActivity.setCategorys(listeActive.getListCategories());
//            mDb.addListe(listeActive);
            mDb.addListeCategoriesItems(listeActive);
            mainActivity.setToutesLesListes((LinkedList<GSListe>) mDb.getAllListe());
            return null;
        }

        @Override
        protected void onPreExecute() {
            dialog = new MaterialDialog.Builder(mainActivity)
                    .title(R.string.creating_list)
                    .content(R.string.please_wait)
                    .progress(true, 0)
                    .cancelable(false)
                    .canceledOnTouchOutside(false)
                    .show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.hide();
            mainActivity.afterLoading();
            mainActivity.reloadDrawer();

        }

    }

    private class LoadDataBegining extends AsyncTask<Integer, Void, Integer> {
        private MaterialDialog dialog;

        @Override
        protected Integer doInBackground(Integer... idListe) {
            GSListe listeActive = mDb.getListe(((GSListe)mainActivity.getAllListes().getFirst()).getId());//TODO modifier
            mainActivity.setListeActive(listeActive);
            mainActivity.setCategorys(listeActive.getListCategories());
//            mCategories = listeActive.getListCategories();
            SharedPreferences prefs =
                    mainActivity.getSharedPreferences(MainActivity.GLOBAL, mainActivity.MODE_PRIVATE);

            mainActivity.setInverse(prefs.getBoolean(mainActivity.getString(R.string.inverse_key), false));
            mainActivity.setProposition(prefs.getBoolean(mainActivity.getString(R.string.proposition_key), true));
            return 0;
        }

        @Override
        protected void onPreExecute() {
            dialog = new MaterialDialog.Builder(mainActivity)
                    .title(R.string.loading)
                    .content(R.string.please_wait)
                    .progress(true, 0)
                    .cancelable(false)
                    .canceledOnTouchOutside(false)
                    .show();
        }

        @Override
        protected void onPostExecute(Integer integer) {
//            dialog.hide();
            dialog.cancel();
            mainActivity.afterLoading();
        }
    }


    private class SaveImage extends AsyncTask<Bitmap, Integer, Boolean> {
        private MaterialDialog dialog;

        @Override
        protected Boolean doInBackground(Bitmap... params) {
            String name = System.currentTimeMillis() + ".jpg";
            boolean res = ImageFile.saveToInternalStorage(mainActivity, params[0], name);
            GSItem itemSelected = mainActivity.getItemSelected();
            if(res) {
                itemSelected.setImage(name);
                mDb.updateItem(itemSelected);
                mainActivity.setItemSelected(null);
            }
            return res;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//			Toast.makeText(MainActivity.this, R.string.saving_img,
//					Toast.LENGTH_SHORT).show();
            dialog = new MaterialDialog.Builder(mainActivity)
                    .title(R.string.loading)
                    .content(R.string.please_wait)
                    .progress(true, 0)
                    .cancelable(false)
                    .canceledOnTouchOutside(false)
                    .show();

        }

        @Override
        protected void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            mainActivity.getAdapter().notifyDataSetChanged();
            dialog.hide();

            if(bool) {
                Toast.makeText(mainActivity, R.string.img_saved,
                        Toast.LENGTH_SHORT).show();
                if(mainActivity.getPopupGestionQuantite()!=null) {
                    mainActivity.getPopupGestionQuantite().setImgSaved();
                }
            }
            else{
                Toast.makeText(mainActivity, mainActivity.getString(R.string.erreurImportImage),
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    private class CreateNewList extends AsyncTask<Object,Void, GSListe>{
        private MaterialDialog dialog;

        @Override
        protected GSListe doInBackground(Object... params) {

            GSListe maNouvelleListe;
            String nom         = (String) params[0];
            TypeListe type     = (TypeListe) params[1];
            int nbp            = (Integer) params[2];
            MainActivity activity  = (MainActivity) params[3];
            boolean preRemplie = (boolean) params[4];

            int position = 0;
            for (GSListe l:mDb.getAllListe()) {
                if(l.getTypeListe() == type){
                    position++;
                }
            }

            String fId = "";
            if(firebase.isUserConnected()){
                fId = firebase.getNewFirebaseID();
            }

            if(preRemplie) {
                maNouvelleListe = Useful.nouvelleListe(activity, nom);
                maNouvelleListe.setType(type);
                maNouvelleListe.setNbPersonnsDemande(nbp);
                maNouvelleListe.setNbPersonne(nbp);
                maNouvelleListe.setPosition(position);
                maNouvelleListe.setFirebaseId(fId);
            }
            else {
                maNouvelleListe = new GSListe(0, nom, type, nbp, nbp, fId,position);
            }
            int id = mDb.addListeCategoriesItems(maNouvelleListe);
            maNouvelleListe.setId(id);
            maNouvelleListe.setSelected(true);

            mainActivity.setToutesLesListes((LinkedList<GSListe>) mDb.getAllListe());

            //Enregistrement de la nouvelle liste dans firebase
            firebase.addListToFireBase(maNouvelleListe);
            return maNouvelleListe;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new MaterialDialog.Builder(mainActivity)
                    .title(R.string.loading)
                    .content(R.string.please_wait)
                    .progress(true, 0)
                    .cancelable(false)
                    .canceledOnTouchOutside(false)
                    .show();
        }

        @Override
        protected void onPostExecute(GSListe liste) {
            super.onPostExecute(liste);
            mainActivity.getNavigationDrawerFragment().add(liste);
            mainActivity.changerDeListeDeCourses(false, liste.getId(), liste.getTypeListe());

            dialog.hide();
        }
    }

    private class AddRecetteToList extends AsyncTask<Integer, Void, Void> {
        private ArrayList<ArrayList<GSItem>> listArrayList;
        private GSListe listeN;
        private boolean add;
        private float multiplicateur;
        private MaterialDialog dialog;
        private ArrayList<MaterialDialog.Builder> dialogs;
        private int curentDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listArrayList = new ArrayList<>();
            dialog = new MaterialDialog.Builder(mainActivity)
                    .title(R.string.treatment)
                    .content(R.string.please_wait)
                    .progress(false, 20, true)
                    .canceledOnTouchOutside(false)
                    .show();
        }

        @Override
        protected Void doInBackground(Integer... params) {
            GSListe recette = mDb.getListe(params[0]);
            listeN  = mDb.getListe(params[1]);
            add =  params[2]==0?true:false;

            int size = 0;
            for (GSCategory c:recette.getListCategories()) {
                size+= c.getListItems().size();
            }

            dialog.setMaxProgress(size);

            multiplicateur = (float)recette.getNbPersonnesDemande()/(float)recette.getNbPersonnes();
            ArrayList<GSItem> itemFind;
            for (GSCategory categoryRecette :recette.getListCategories()) {
                for (GSItem itemRecette: categoryRecette.getListItems()) {
                    itemFind = new ArrayList<>();
                    for (GSCategory categoryListe : listeN.getListCategories()) {
                        for (GSItem itemListe: categoryListe.getListItems()) {
                            if(compare(itemRecette.getNom(), itemListe.getNom())){
                                itemFind.add(itemListe);
                            }
                        }

                    }
                    //Un seul item trouve, on l'utilise (MAJ quantites)
                    if(itemFind.size()==1){
                        updateItem(itemRecette, itemFind.get(0));
                    }
                    else if (itemFind.size()>1){//Plusieurs items touve, demande lequel choisir
                        //Traitement dans les postexecute
                        itemFind.add(itemRecette);//On ajoute l'item recherche à la fin
                        listArrayList.add((ArrayList<GSItem>) itemFind.clone());
                        //updateItem(itemRecette, itemFind.get(0), multiplicateur);
                    }
                    else{
                        //Aucun item trouve
                        //Recherche du nom de la categorie de l'item:
                        //Si trouve une seul, on ajoute l'item a cette categorie
                        ArrayList<GSCategory> listeCate = new ArrayList<>();
                        for (GSCategory category:listeN.getListCategories()) {
                            if (compare(itemRecette.getNomCategorie(), category.getNom())){
                                listeCate.add(category);
                            }
                        }
                        if(listeCate.size()==1){
                            addItem(itemRecette,listeCate.get(0));
                        }
                        else {
                            itemFind.add(itemRecette);
                            listArrayList.add((ArrayList<GSItem>) itemFind.clone());
                        }
                        //Si non on demande à quel categorie l'ajouter
                    }
                }
                publishProgress();

            }

            return null;
        }

        private void updateItem(GSItem itemRecette, GSItem itemNormale){
            float newQuantity = itemRecette.getQuantite()*multiplicateur;
            if(add){
                newQuantity+=itemNormale.getQuantite();
            }
            itemNormale.setQuantite(newQuantity);
            itemNormale.setType(itemRecette.getType());
            itemNormale.setSelect(false);
			mDb.updateItem(itemNormale);
            firebase.alterItemFromFirebase(itemNormale, TypeListe.LISTE_NORMALE.toString(), listeN.getFirebaseId());
        }

        private void addItem(GSItem itemRecette, GSCategory categoryNormale){
            GSItem item = itemRecette.clone();
            item.setIdCategorie(categoryNormale.getId());
            item.setFirebaseId(firebase.getNewFirebaseID());
            item.setIdFirebaseCategorie(categoryNormale.getFirebaseId());
            item.setSelect(false);
            mDb.addItem(item);
            firebase.addItemToFirebase(item, TypeListe.LISTE_NORMALE.toString(), listeN.getFirebaseId(), categoryNormale.getFirebaseId());
        }

        private boolean compare(String str1, String str2){
            final Collator instance = Collator.getInstance();
            instance.setStrength(Collator.NO_DECOMPOSITION);
            if(instance.compare(str1.toLowerCase(), str2.toLowerCase()) == 0){
                return true;
            }
            if(instance.compare((str1.toLowerCase()+"s"), str2.toLowerCase()) == 0){
                return true;
            }
            if(instance.compare(str1.toLowerCase(), (str2.toLowerCase()+"s")) == 0){
                return true;
            }

            return false;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            dialog.incrementProgress(1);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            //Plusieurs items touve, demande lequel choisir
            //Item non touve, on demande à quel categorie l'ajouter (possibilite de creer une categorie)
            dialog.dismiss();
            dialogs = new ArrayList<>();
            for (final ArrayList<GSItem> array:listArrayList) {
                //Demande de la categorie dans laquel l'ajouter
                if(array.size()==1){
                    String title = mainActivity.getString(R.string.category_choise, array.get(0).getNom());
                    String[] choix = new String[listeN.getListCategories().size()];
                    for (int i = 0; i < listeN.getListCategories().size(); i++) {
                        choix[i] = listeN.getListCategories().get(i).getNom();
                    }
                    dialogs.add(new MaterialDialog.Builder(mainActivity)
                            .title(title)
                            .items(choix)
                            .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                                @Override
                                public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                    addItem(array.get(0), listeN.getListCategories().get(which));
                                    curentDialog++;
                                    if(dialogs.size()>curentDialog){
                                        dialogs.get(curentDialog).show();
                                    }
                                    return true;
                                }
                            })
                            .positiveText(R.string.choose)
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    showToast(array.get(0).getNom(), listeN.getNom());
                                    curentDialog++;
                                    if(dialogs.size()>curentDialog){
                                        dialogs.get(curentDialog).show();
                                    }
                                }
                            })
                            .negativeText(R.string.annuler));

                }
                else {
                    //Le dernier element est l'item a ajouter
                    String[] choix = new String[array.size()-1];
                    for (int i = 0; i < array.size()-1; i++) {
                        choix[i] = "["+array.get(i).getNomCategorie()+"] " + array.get(i).getNom();
                    }
                    dialogs.add(new MaterialDialog.Builder(mainActivity)
                            .title(R.string.witch_item_alter)
                            .items(choix)
                            .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                                @Override
                                public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                    updateItem(array.get(array.size()-1), array.get(which));
                                    curentDialog++;
                                    if(dialogs.size()>curentDialog){
                                        dialogs.get(curentDialog).show();
                                    }
                                    return true;
                                }
                            })
                            .positiveText(R.string.choose)
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    showToast(array.get(array.size()-1).getNom(), listeN.getNom());
                                    curentDialog++;
                                    if(dialogs.size()>curentDialog){
                                        dialogs.get(curentDialog).show();
                                    }
                                }
                            })
                            .negativeText(R.string.annuler));
                }
            }
            curentDialog = 0;
            if(dialogs.size()>=1){
                dialogs.get(0).show();
            }
            super.onPostExecute(aVoid);
        }

        private void showToast(String itemName, String listName){
            Toast.makeText(mainActivity, mainActivity.getString(R.string.not_add, itemName, listName),
                    Toast.LENGTH_SHORT).show();
        }

    }

    private class LoadAllListsEmpty extends AsyncTask<Boolean, Void, Void> {
        private boolean reload;

        @Override
        protected Void doInBackground(Boolean... params) {
            reload = params[0];
            mainActivity.setToutesLesListes((LinkedList<GSListe>) mDb.getAllListe());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(reload){
                mainActivity.reloadDrawer();
            }
        }
    }
}
