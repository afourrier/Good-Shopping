package com.fourrier.goodshopping;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.LinkedList;

import database.DatabaseHandler;
import database.GSCategory;
import database.GSItem;
import database.GSListe;

/**
 * Created by Alexis on 19/06/2017.
 */

public class GSFirebase {
    private DatabaseReference database;
    private DatabaseHandler mDb;


    public GSFirebase(DatabaseReference database, DatabaseHandler mDb) {
        this.database = database;
        this.mDb = mDb;
    }

    public boolean isUserConnected(){
        return FirebaseAuth.getInstance().getCurrentUser()!=null;
    }


    public void addListToFireBase(GSListe liste){
        if(isUserConnected()) {
            confirmFirebaseId_List(liste);
            database.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(liste.getTypeListe().toString())
                    .child(String.valueOf(liste.getFirebaseId())).setValue(liste);
        }
    }

    public void addCategoryToFirebase(GSCategory category, MainActivity mainActivity){
        if(isUserConnected()) {
            category = confirmFirebaseId_Category(category);
            GSListe listeActive = mainActivity.getListeActive();
            database.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(listeActive.getTypeListe().toString())
                    .child(String.valueOf(listeActive.getFirebaseId()))
                    .child("categories")
                    .child(String.valueOf(category.getFirebaseId()))
                    .setValue(category);

        }
    }

    public void addItemToFirebase(GSItem item, MainActivity mainActivity){
        if(isUserConnected()) {
            item = confirmFirebaseId_Item(item);
            GSListe listeActive = mainActivity.getListeActive();
            database.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(listeActive.getTypeListe().toString())
                    .child(String.valueOf(listeActive.getFirebaseId()))
                    .child("categories")
                    .child(String.valueOf(item.getIdFirebaseCategorie()))
                    .child("items")
                    .child(String.valueOf(item.getFirebaseId()))
                    .setValue(item);
        }
    }

    public void addItemToFirebase(GSItem item, String typeListe, String listeId, String categoryId){
        if(isUserConnected()) {
            item = confirmFirebaseId_Item(item);
            database.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(typeListe)
                    .child(listeId)
                    .child("categories")
                    .child(categoryId)
                    .child("items")
                    .child(String.valueOf(item.getFirebaseId()))
                    .setValue(item);
        }
    }

    public void changeItemCategoryOnFirebase(GSItem itemDebut, GSItem item, MainActivity mainActivity){
        addItemToFirebase(item, mainActivity);
        removeItemFromFirebase(itemDebut, mainActivity);
    }

    public void changeCategoryOrder(MainActivity mainActivity){
        LinkedList<GSCategory> categorys= mainActivity.getCategorys();
        for (GSCategory category:categorys) {
            alterCategoryFromFirebase(category, mainActivity);
        }
    }

    public void alterListFromFirebase(GSListe liste){
        if(isUserConnected()) {
            HashMap<String, Object> alterMap = new HashMap<>();
            alterMap.put("firebaseId", liste.getFirebaseId());
            alterMap.put("nbPersonnes", liste.getNbPersonnes());
            alterMap.put("nbPersonnesDemande", liste.getNbPersonnesDemande());
            alterMap.put("nom", liste.getNom());
            alterMap.put("position", liste.getPosition());
            alterMap.put("selected", liste.isSelected());
            alterMap.put("typeListe", liste.getTypeListe());
            alterMap.put("device_ID", liste.getDEVICE_ID());

            database.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(liste.getTypeListe().toString())
                    .child(String.valueOf(liste.getFirebaseId()))
                    .updateChildren(alterMap);
        }
    }

    public void alterCategoryFromFirebase(GSCategory category, MainActivity mainActivity){
        if(isUserConnected()) {
            GSListe listeActive = mainActivity.getListeActive();
            HashMap<String, Object> alterMap = new HashMap<>();
            alterMap.put("firebaseId", category.getFirebaseId());
            alterMap.put("couleur", category.getCouleur());
            alterMap.put("emplacement", category.getEmplacement());
            alterMap.put("nom", category.getNom());
            alterMap.put("device_ID", category.getDEVICE_ID());

            database.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(listeActive.getTypeListe().toString())
                    .child(String.valueOf(listeActive.getFirebaseId()))
                    .child("categories")
                    .child(String.valueOf(category.getFirebaseId()))
                    .updateChildren(alterMap);
        }
    }

    public void alterItemFromFirebase(GSItem item, String typeListe, String idListe){
        if(isUserConnected()) {
            HashMap<String, Object> alterMap = new HashMap<>();
            alterMap.put("firebaseId", item.getFirebaseId());
            alterMap.put("hauteur", item.getHauteur());
            alterMap.put("emplacement", item.getEmplacement());
            alterMap.put("image", item.getImage());
            alterMap.put("largeur", item.getLargeur());
            alterMap.put("longueur", item.getLongueur());
            alterMap.put("mHauteurType", item.getHautType());
            alterMap.put("mLargeurType", item.getLargType());
            alterMap.put("mLongueurType", item.getLongType());
            alterMap.put("nbFoisAchete", item.getNbFoisAchete());
            alterMap.put("nom", item.getNom());
            alterMap.put("quantite", item.getQuantite());
            alterMap.put("select", item.isSelect());
            alterMap.put("type", item.getType());
            alterMap.put("device_ID", item.getDEVICE_ID());

            database.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(typeListe)
                    .child(idListe)
                    .child("categories")
                    .child(String.valueOf(item.getIdFirebaseCategorie()))
                    .child("items")
                    .child(String.valueOf(item.getFirebaseId()))
                    .updateChildren(alterMap);
        }
    }

    public void alterItemFromFirebase(GSItem item, MainActivity mainActivity){
        GSListe listeActive = mainActivity.getListeActive();
        alterItemFromFirebase(item, listeActive.getTypeListe().toString(), String.valueOf(listeActive.getFirebaseId()));
    }


        public void removeListFromFirebase(GSListe liste){
        if(isUserConnected()) {
            database.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(liste.getTypeListe().toString())
                    .child(String.valueOf(liste.getFirebaseId())).removeValue();
        }
    }

    public void removeItemFromFirebase(GSCategory category, MainActivity mainActivity){
        if(isUserConnected()) {
            GSListe listeActive = mainActivity.getListeActive();
            database.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(listeActive.getTypeListe().toString())
                    .child(String.valueOf(listeActive.getFirebaseId()))
                    .child("categories")
                    .child(String.valueOf(category.getFirebaseId()))
                    .removeValue();
        }
    }

    public void removeItemFromFirebase(GSItem item, MainActivity mainActivity){
        if(isUserConnected()) {
            GSListe listeActive = mainActivity.getListeActive();
            database.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(listeActive.getTypeListe().toString())
                    .child(String.valueOf(listeActive.getFirebaseId()))
                    .child("categories")
                    .child(String.valueOf(item.getIdFirebaseCategorie()))
                    .child("items")
                    .child(String.valueOf(item.getFirebaseId()))
                    .removeValue();
        }
    }

    /**
     * Verifie si tous les elements de la liste comporte un id firebase
     * @param liste
     * @return la liste avec les id firebase
     */
    public GSListe confirmFirebaseId_List(GSListe liste){
        //On verifie si la liste contien un id firabase
        if(liste.isFirebaseIdEmpty()){
            //Si elle n'en a pas on l'ajoute
            liste.setFirebaseId(getNewFirebaseID());
            mDb.updateListe(liste);
        }
        for (GSCategory cate: liste.getListCategories()) {
            confirmFirebaseId_Category(cate);
        }
        return liste;
    }

    /**
     * Verifie si tous les elements de la category comporte un id firebase
     * @param category
     * @return la liste avec les id firebase
     */
    public GSCategory confirmFirebaseId_Category(GSCategory category){
        //On verifie si la liste contien un id firabase
        if(category.isFirebaseIdEmpty()){
            //Si elle n'en a pas on l'ajoute
            category.setFirebaseId(getNewFirebaseID());
            mDb.updateCategorie(category);

            for (GSItem item: category.getListItems()) {
                if(item.isFirebaseIdEmpty()){
                    //Si in n'en a pas on l'ajoute
                    confirmFirebaseId_Item(item);
                }
            }
        }

        return category;
    }

    /**
     * Verifie si tous les elements de la categorie comporte un id firebase
     * @param item
     * @return la liste avec les id firebase
     */
    public GSItem confirmFirebaseId_Item(GSItem item){
        //On verifie si la liste contien un id firabase
        if(item.isFirebaseIdEmpty()){
            //Si in n'en a pas on l'ajoute
            item.setFirebaseId(getNewFirebaseID());
            mDb.updateItem(item);
        }
        return item;
    }

    public String getNewFirebaseID(){
        if(isUserConnected()) {
            return database.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).push().getKey();
        }
        return "";
    }

}
