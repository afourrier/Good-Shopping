package com.fourrier.goodshopping;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import java.io.File;

import services.ImageFile;

public class PictureActivity extends AppCompatActivity {
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_picture);

        imageView = (ImageView)findViewById(R.id.imageViewPicture);

        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File path = cw.getDir("imageDir", Context.MODE_PRIVATE);

        Intent intent = getIntent();
        String name = intent.getStringExtra("imageName");

        Bitmap b = ImageFile.loadImageFromStorage(path, name);
        imageView.setImageBitmap(b);
    }
}
