package com.fourrier.goodshopping;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.LinkedList;

import adapters.MenuAdapter;
import adapters.Item;
import adapters.AddItemMenu;
import adapters.SeparateurItemMenu;
import database.GSListe;
import database.TypeListe;
import services.TypePopup;

/**
 * Fragment used for managing interactions for and presentation of a navigation
 * drawer. See the <a href=
 * "https://developer.android.com/design/patterns/navigation-drawer.html#Interaction"
 * > design guidelines</a> for a complete explanation of the behaviors
 * implemented here.
 */
public class NavigationDrawerFragment extends Fragment {

	/**
	 * Remember the position of the selected item.
	 */
	private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

	/**
	 * Per the design guidelines, you should show the drawer on launch until the
	 * user manually expands it. This shared preference tracks this.
	 */
	private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

	/**
	 * A pointer to the current callbacks instance (the Activity).
	 */
	private NavigationDrawerCallbacks mCallbacks;

	/**
	 * Helper component that ties the action bar to the navigation drawer.
	 */
	private ActionBarDrawerToggle mDrawerToggle;

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerListView;
	private View mFragmentContainerView;
	private boolean mFirst;
	private int mCurrentSelectedPosition = 0;
	private boolean mFromSavedInstanceState;
	private boolean mUserLearnedDrawer;
	private LinkedList<GSListe> mAllListes;
	private String[] mTabMenu;
	private LayoutInflater inflater;
	private ViewGroup container;
	private MenuAdapter mAdapter;
	private MenuItem share;
	private MenuItem tousCocherDecocher;
	private MenuItem inverse;
	private MenuItem propistions;
	private Menu menu;
	private Toolbar mToolbar;

	public NavigationDrawerFragment(){}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Read in the flag indicating whether or not the user has demonstrated
		// awareness of the
		// drawer. See PREF_USER_LEARNED_DRAWER for details.
		mFirst = true;
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(getActivity());
		mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

		if (savedInstanceState != null) {
			mCurrentSelectedPosition = savedInstanceState
					.getInt(STATE_SELECTED_POSITION);
			mFromSavedInstanceState = true;
		}

		// Select either the default item (0) or the last selected item.
		selectItem(mCurrentSelectedPosition);
		setHasOptionsMenu(true);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Indicate that this fragment would like to influence the set of
		// actions in the action bar.
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.inflater = inflater;
		this.container = container;
		gererListView();
		return mDrawerListView;
	}
	
	public void gererListView(){
		
		mAllListes = ((MainActivity) getActivity()).getAllListes();
		mTabMenu = new String[mAllListes.size()+1];
		for(int i=0;i<mAllListes.size();i++){
			mTabMenu[i] = (String) mAllListes.get(i).getNom();
		}
		mTabMenu[mAllListes.size()] = getActivity().getString(R.string.newList);
		
		mDrawerListView = (ListView) inflater.inflate(
				R.layout.fragment_navigation_drawer, container, false);

		affichageMenu(false);
	}

	public boolean isDrawerOpen() {
		return mDrawerLayout != null
				&& mDrawerLayout.isDrawerOpen(mFragmentContainerView);
	}

	/**
	 * Users of this fragment must call this method to set up the navigation
	 * drawer interactions.
	 * 
	 * @param fragmentId
	 *            The android:id of this fragment in its activity's layout.
	 * @param drawerLayout
	 *            The DrawerLayout containing this fragment's UI.
	 */
	public void setUp(int fragmentId, DrawerLayout drawerLayout) {
		mFragmentContainerView = getActivity().findViewById(fragmentId);
		mDrawerLayout = drawerLayout;

		// set a custom shadow that overlays the main content when the drawer
		// opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		// set up the drawer's list view with items and click listener

		mToolbar = ((MainActivity)getActivity()).getToolbar();
        android.support.v7.app.ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
		//mToolbar.setNavigationIcon(R.drawable.ic_launcher);
		//mToolbar.setDisplayHomeAsUpEnabled(true);
		//mToolbar.setHomeButtonEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the navigation drawer and the action bar app icon.
		mDrawerToggle = new ActionBarDrawerToggle(getActivity(), /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		//R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
		R.string.navigation_drawer_open, /*
										 * "open drawer" description for
										 * accessibility
										 */
		R.string.navigation_drawer_close /*
										 * "close drawer" description for
										 * accessibility
										 */
		) {
			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				if (!isAdded()) {
					return;
				}

				getActivity().invalidateOptionsMenu(); // calls
														// onPrepareOptionsMenu()
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				if (!isAdded()) {
					return;
				}

				if (!mUserLearnedDrawer) {
					// The user manually opened the drawer; store this flag to
					// prevent auto-showing
					// the navigation drawer automatically in the future.
					mUserLearnedDrawer = true;
					SharedPreferences sp = PreferenceManager
							.getDefaultSharedPreferences(getActivity());
					sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true)
							.apply();
				}

				getActivity().invalidateOptionsMenu(); // calls
														// onPrepareOptionsMenu()
			}
		};

		// If the user hasn't 'learned' about the drawer, open it to introduce
		// them to the drawer,
		// per the navigation drawer design guidelines.
		if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
			mDrawerLayout.openDrawer(mFragmentContainerView);
		}

		// Defer code dependent on restoration of previous instance state.
		mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
		//mDrawerToggle.setDrawerIndicatorEnabled(true);
		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}

	private void selectItem(int position) {
		mCurrentSelectedPosition = position;
		if (mDrawerListView != null) {
			mDrawerListView.setItemChecked(position, true);
		}
		if (mDrawerLayout != null) {
			mDrawerLayout.closeDrawer(mFragmentContainerView);
		}
		if (mCallbacks != null) {
			mCallbacks.onNavigationDrawerItemSelected(position);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallbacks = (NavigationDrawerCallbacks) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(
					"Activity must implement NavigationDrawerCallbacks.");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = null;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Forward the new configuration the drawer toggle component.
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// If the drawer is open, show the global app actions in the action bar.
		// See also
		// showGlobalContextActionBar, which controls the top-left area of the
		// action bar.
		if (mDrawerLayout != null && isDrawerOpen()) {
			inflater.inflate(R.menu.global, menu);
			showGlobalContextActionBar();
		}
		this.menu = menu;
		if(mFirst){
	    	tousCocherDecocher = menu.findItem(R.id.touscocherdecocher);
	    	share              = menu.findItem(R.id.menu_share);
			inverse            = menu.findItem(R.id.menu_inverser);
			propistions        = menu.findItem(R.id.menu_suggestions);
			
			mFirst = false;
		}
		onPrepareOptionsMenu();
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
	
	public boolean onPrepareOptionsMenu(){
        share.setVisible(true);
        if(((MainActivity) getActivity()).getPremierCoche()==-1){
        	tousCocherDecocher.setIcon(R.drawable.ic_check_box_outline_blank_black);
        }
        else{
        	tousCocherDecocher.setIcon(R.drawable.ic_check_box_black);
        }
		if(((MainActivity) getActivity()).getInverse())
			inverse.setTitle(getString(R.string.CPA));
		else
			inverse.setTitle(getString(R.string.DPA));
			propistions.setChecked(((MainActivity) getActivity()).getProposition());

        return true;
    }


    /**
     * Per the navigation drawer design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setTitle(R.string.app_name);
    }

    private ActionBar getActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }

	/**
	 * Callbacks interface that all activities using this fragment must
	 * implement.
	 */
	public static interface NavigationDrawerCallbacks {
		/**
		 * Called when an item in the navigation drawer is selected.
		 */
		void onNavigationDrawerItemSelected(int position);
	}
	
	private void gererListe(TypePopup type, int position, int idListe){
		//new GestionListes(type, position, (MainActivity)getActivity(), this, idListe);
	}
	
	public void affichageMenu(boolean reload){
		//Adapteur
		mAdapter = new MenuAdapter((MainActivity)getActivity(), generateBasicsItems(reload), this);
		mDrawerListView.setAdapter(mAdapter);
    }

    public ArrayList<Item> generateBasicsItems(boolean reload){
        ArrayList<Item> items = new ArrayList<Item>();

        items.addAll(chargerListe(TypeListe.LISTE_NORMALE));
        items.addAll(chargerListe(TypeListe.LISTE_RECETTE));
        //items.addAll(chargerListe(TypeListe.LISTE_COMPOSE));

        items.addAll(chargerListe(TypeListe.PARAMETRES));
        SharedPreferences prefs = getActivity().getSharedPreferences(MainActivity.GLOBAL, getActivity().MODE_PRIVATE);

        if(!prefs.getBoolean(getActivity().getString(R.string.rate_key), false)){
            items.addAll(chargerListe(TypeListe.NOTE));
        }
//        items.addAll(chargerListe(TypeListe.REFRESH_FIREBASE));

        if(!reload) {
            items.get(1).setSelected(true);//0 = menu liste
        }
            return items;
    }
	
	public void add(Item liste){
		mAdapter.add(liste);
		mAdapter.notifyDataSetChanged();
	}
	
	public void remove(int idListe){
		mAdapter.remove(idListe);
		mAdapter.notifyDataSetChanged();
	}
	
	public void setSelectedItem(int position){
		mAdapter.setSelected(position);
		mAdapter.notifyDataSetChanged();
	}
	
	public void rename(int position, GSListe liste){
		mAdapter.rename(position, liste);
		mAdapter.notifyDataSetChanged();
	}
	
	public void openMenuLateral(){
		mDrawerLayout.openDrawer(mDrawerListView);
		mDrawerLayout.closeDrawer(mDrawerListView);
		//((MainActivity)getActivity()).onPrepareOptionsMenu();
	}

	private ArrayList<Item> chargerListe(TypeListe typeListe){
		ArrayList<Item> items = new ArrayList<Item>();
		String nom = getString(R.string.listeNormale);
		if(typeListe==TypeListe.LISTE_COMPOSE)
			nom = getString(R.string.listeFusione);
		else if(typeListe==TypeListe.LISTE_RECETTE)
			nom = getString(R.string.listeRecettes);
		else if (typeListe==TypeListe.PARAMETRES)
			nom = getString(((MainActivity)getActivity()).isUserConnected()?
					R.string.paremeters: R.string.connexion);
        else if (typeListe==TypeListe.REFRESH_FIREBASE)
            nom = getString(R.string.firebase_refresh);
        else if (typeListe==TypeListe.NOTE){
            nom = getString(R.string.note);
        }
		//Ajout du separateur
		items.add(new SeparateurItemMenu(nom, false, typeListe));

		if(typeListe!=TypeListe.PARAMETRES && typeListe!=TypeListe.REFRESH_FIREBASE
                && typeListe!=TypeListe.NOTE) {
			//Ajout des listes de courses
			for (int i = 0; i < mAllListes.size(); i++) {
				if (mAllListes.get(i).getTypeListe() == typeListe) {
                    if(mAllListes.get(i).getId() == ((MainActivity) getActivity()).getListeActive().getId()){
                        mAllListes.get(i).setSelected(true);
                    }
                    items.add(mAllListes.get(i));
                }
			}
			//Ajout de l'item nouvelle liste
			items.add(new AddItemMenu(getString(R.string.add), typeListe));
		}
		return items;
	}


	public void refresh(GSListe liste){
		for (int i = 0; i < mAllListes.size(); i++) {
			if(mAllListes.get(i).getId() == liste.getId()){
				mAdapter.rename(liste);
				mAllListes.set(i, liste);
			}
		}
		mAdapter.notifyDataSetChanged();
		//gererListView();
		//affichageMenu();
	}

	public void remove(GSListe liste){
		mAdapter.remove(liste);
	}

	public void refresh(){
		mAdapter.notifyDataSetChanged();
//		gererListView();
	}

	public void reload(MainActivity mainActivity){
        mAllListes = mainActivity.getAllListes();
        mAdapter.reloadList(generateBasicsItems(true));
    }

	public void close(){
		mDrawerLayout.closeDrawers();
	}

	public void unselectAll(){
        mAdapter.unselectAll();
    }
}
