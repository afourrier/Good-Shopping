package com.fourrier.goodshopping;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import database.DatabaseHandler;
import services.Useful;

public class SplashScreen extends Activity implements IActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        DatabaseHandler db = new DatabaseHandler(this);

        SharedPreferences prefs = getSharedPreferences(MainActivity.GLOBAL, MODE_PRIVATE);
        if(prefs.getBoolean(getString(R.string.first_use_key), true)){
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(getString(R.string.first_use_key), false);
            edit.commit();

            //Verification qu'il s'agit bien de la premiere utilisation
            int size = db.getListSize();
            if(size==0){
                new CreateList().execute(db, edit);
            }
            else {
                edit.putBoolean(getString(R.string.inverse_key), Boolean.parseBoolean(db.getDico("inverse")));
                edit.putBoolean(getString(R.string.proposition_key), Boolean.parseBoolean(db.getDico("proposition")));
                edit.commit();
                startMainActivity();
            }

        }
        else {

            new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app main activity
                    startMainActivity();
                }
            }, SPLASH_TIME_OUT);
        }
    }

    private void startMainActivity(){
        Intent i = new Intent(SplashScreen.this, MainActivity.class);
        startActivity(i);

        // close this activity
        finish();
    }

    @Override
    public boolean isUserConnected() {
        return FirebaseAuth.getInstance().getCurrentUser()!=null;
    }

    @Override
    public String getNewFirebaseID() {
        if(isUserConnected()) {
            return FirebaseDatabase.getInstance().getReference()
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).push().getKey();
        }
        return "";
    }

    private class CreateList extends AsyncTask<Object, Void, Void> {

        @Override
        protected Void doInBackground(Object... o) {
            DatabaseHandler db = (DatabaseHandler) o[0];
            SharedPreferences.Editor edit = (SharedPreferences.Editor) o[1];
            db.addListeCategoriesItems(Useful.nouvelleListe(SplashScreen.this, SplashScreen.this.getString(R.string.defaut)));
            edit.putBoolean(getString(R.string.inverse_key), false);
            edit.putBoolean(getString(R.string.proposition_key), true);
            edit.commit();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            startMainActivity();
        }
    }

}